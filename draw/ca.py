# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl

import cairo
from math import atan2, cos, sin
from os import getpid
from os import sep, path

from htlib import totup0
from pict import BBox

A4SIZE = (594, 895)

from htlib import getcolor, P, totup
from htlib import LTSYMBOLFIXED, LTMATH, LTTEXT
from htlib import STYLESUBSCRIPT2, LTSYMBOL, LTSYMBOLMATH, STYLENORMAL

stnsizesdict = {}

scriptsize = 0.6

SCALEFONTS = 1  # simulated scaling with fonts only
SCALESIMPLE = 0  # simulated with no font scalling

MRESOLUTION = 3

fndict = {
    LTTEXT: ("Times",),
    2: ("Sans",),
    3: ("Serif",),
    4: ("Courier",),
    5: ("Helvetica",),
    6: ("Symbol",),

    LTMATH: ("Times", cairo.FONT_SLANT_ITALIC),
    LTSYMBOL: ("Times",),
    LTSYMBOLMATH: ("Times", cairo.FONT_SLANT_ITALIC),
    LTSYMBOLFIXED: ("Symbol",),

    # LTSYMBOL:("Times",),
}

# A4

# temp surfaces for bb

# optimalization
from time import gmtime, strftime
from os import remove

tempfilename = '/tmp/gsevoltmp%d_%s' % (getpid(), strftime("%Y%m%d%H%M%S", gmtime()))
tempfilename2 = tempfilename + "_2"
tempsurface = cairo.PDFSurface(tempfilename, 50, 50)
# tempcontext = cairo.Context (cairo.PDFSurface ('/tmp/tmp%d'%getpid(), 50,50) )

# Clean temp files
import atexit


def cleantempfile():
    try:
        remove(tempfilename)
        remove(tempfilename2)
    except Exception:
        pass  # ignore


atexit.register(cleantempfile)


class WState:
    def __init__(self, picture, ctx, **kwargs):

        self.picture = picture
        self.ctx = ctx
        self.ignoretextbb = kwargs.get('ignoretextbb', 0)
        self.fulltextbb = kwargs.get('fulltextbb', 0)
        self.symbolshift = kwargs.get("symbolshift", 31)

        self.fndict = fndict.copy()
        if "fonttext" in kwargs: self.fndict[LTTEXT] = kwargs["fonttext"]
        # FONTCODES \2 \3 etc.
        for i in xrange(2, 9):
            if "fonttext%d" % i in kwargs: self.fndict[i] = kwargs["fonttext%d" % i]
        if "fontmath" in kwargs: self.fndict[LTMATH] = kwargs["fontmath"]
        if "fontsymbol" in kwargs: self.fndict[LTSYMBOL] = kwargs["fontsymbol"]
        self.extscalling = kwargs.get("extscalling", SCALESIMPLE)

        self.scriptsize = kwargs.get("scriptsize", scriptsize)
        self.extscalling = kwargs.get("extscalling", SCALESIMPLE)

        m = kwargs.get("margin", 5)
        if type(m) == tuple and len(m) == 4:
            m = (P(m[0:2]), P(m[2:]))
        else:
            m = (P(m), P(m))
        self.margin = m  # P(kwargs.get("margin",5))
        # self.stnwidth,self.stnheight = stnsizes(WState(self.fontsize,self.extscalling,self.fndict,self.scriptsize))
        # self.fontmargin=max(self.stnheight,self.stnwidth)*0.7
        self.background = getcolor(kwargs.get("backgroundcolor", 'white'))

        self.outputsize = kwargs.get("outputsize", 0)

        # @TODO: other sizes
        if self.outputsize == 'A4': self.outputsize = A4SIZE
        self.outputsize = totup0(self.outputsize)

        self.alx = 1.0
        self.bex = 10.0
        self.aly = -1.0
        self.bey = 10.0
        self.fontsize = kwargs.get('fontsize', 12)

        self.firstfs = 1
        self.context = 0

    def save(self):
        self.context += 1
        # print "SAVECONTEXT",self.context
        self.ctx.save()

    # print self.ctx.get_matrix()

    def restore(self):
        # print "DELCONTEXT",self.context
        self.ctx.restore()
        self.context -= 1

    # print self.ctx.get_matrix()

    def translate(self, x, y):
        # print "  "*self.context,"TRANSLATE",x,y
        self.ctx.translate(x, y)

    def rotate(self, phi):
        # print "  "*self.context,"ROTATE",phi
        self.ctx.rotate(phi)

    def translaterotate(self, a, b):
        self.translate(a.x, a.y)
        v = b - a
        phi = atan2(v.y, v.x)
        self.rotate(phi)
        return phi

    def marginx(self):
        return self.margin[0].x + self.margin[1].x

    def marginy(self):
        return self.margin[0].y + self.margin[1].y

    def geqs(self, W, H, minx, maxx, miny, maxy, margin):
        w, h = self._prepwh(minx, maxx, miny, maxy)
        sc = min((W - -margin[0].x - margin[1].x) / w, (H - margin[0].y - margin[1].y) / h)
        # print "geqs",w,h,W,H,sc

        self.alx = sc
        self.bex = margin[0].x - minx * self.alx
        self.aly = -sc
        self.bey = margin[0].y - maxy * self.aly
        return w, h

    def geqsbb(self, W, H, bb, margin):
        return self.geqs(W, H, bb.a.x, bb.b.x, bb.a.y, bb.b.y, margin)

    def gsbb(self, S, bb, margin):
        return self.gs(S[0] - margin[0].x - margin[1].x, S[1] - margin[0].y - margin[1].y, bb.a.x, bb.b.x, bb.a.y,
                       bb.b.y, margin)

    def gs(self, Sx, Sy, minx, maxx, miny, maxy, margin):
        w, h = self._prepwh(minx, maxx, miny, maxy)
        scx = Sx / w
        scy = Sy / h
        if not scx: scx = 1.0  # zerodivisionerror
        if not scy: scy = 1.0  # zerodivisionerror
        self.alx = scx
        self.bex = -minx * scx + margin[0].x
        self.aly = -scy
        self.bey = maxy * scy + margin[0].y
        return w * scx + margin[0].x + margin[1].x, h * scy + margin[0].y + margin[1].y

    def _prepwh(self, minx, maxx, miny, maxy):
        w = maxx - minx
        h = maxy - miny
        if not w: w = 0.01
        if not h: h = 0.01
        return w, h

    def rxy(self, x, y):
        return 1.0 * (x - self.bex) / self.alx, (1.0 * (y - self.bey)) / self.aly

    def xy(self, p, store=0, name=None):

        # print "xy",store,name,p,p.__class__

        p = (self.bex + self.alx * p.x, self.bey + self.aly * p.y)
        # import htlib
        # htlib.pstack()
        if store and name:
            if name not in self.N:
                self.S.setdefault((int(p[0]) / MRESOLUTION, int(p[1]) / MRESOLUTION), []).append(name)
                self.N.append(name)
        return p

    def uniscale(self):
        return min(abs(self.alx), abs(self.aly))

    def stnfontsize(self, style):
        if style == STYLENORMAL: return 1.0
        if style == STYLESUBSCRIPT2: return (1.0 + self.scriptsize) / 2
        return self.scriptsize

    def getfontsize(self, style=STYLENORMAL, fontsize=0):
        if not fontsize: fontsize = self.fontsize
        if self.firstfs:
            self._initfs = self.uniscale()
            self.firstfs = 0
        if self.extscalling == SCALEFONTS:
            return self.uniscale() * fontsize * self.stnfontsize(style) / self._initfs
        else:
            return self.stnfontsize(style) * fontsize

    def stnsizes(self):
        sc = self.getfontsize() * self.stnfontsize(STYLENORMAL)
        fn = self.fndict[LTTEXT]
        if (sc, fn) not in stnsizesdict:
            surface = cairo.SVGSurface(tempfilename2, 500, 500)
            ctx = cairo.Context(surface)
            ctx.select_font_face(*fn)
            ctx.set_font_size(sc)
            stnsizesdict[sc, fn] = ctx.text_extents("A")[2:4]

        return stnsizesdict[sc, fn]

    def __repr__(self):
        return "%g %g x %g %g WH%s" % (self.alx, self.bex, self.aly, self.bey, str(self.outputsize))


# A collection of Prim's with drawing capabilities

class Picture:
    def __init__(self):
        self.p = []
        self.points = []
        self._points = {}
        self.bbox = None

    def getwstate(self, ctx, **kwargs):

        wstate = WState(self, ctx, **kwargs)
        bb = self.realbb(wstate)

        if not bb: bb.addpoint(P(0, 0))

        w = abs(bb.b.x - bb.a.x)
        h = abs(bb.b.y - bb.a.y)

        if not wstate.outputsize:  # nativesize

            w += wstate.marginx()
            h += wstate.marginy()

            # print w,h,margin
            w, h = wstate.gsbb((w, h), bb, wstate.margin)


        else:  # size defined
            if wstate.extscalling:
                cnt = 1
            else:
                cnt = 4
            # scalling fonts problem, required
            for i in xrange(cnt):
                w, h = wstate.gsbb(wstate.outputsize, bb, wstate.margin)
                bb = self.realbb(wstate)

            w, h = wstate.outputsize

        return wstate, w, h

    def append(self, o):

        self.p.append(o)

    def draw(self, wstate, **kwargs):
        # print "\n in DRAW",wstate
        caprim = kwargs.get('caprim', 0)
        cai = []
        cap = {}
        # TODO: priorities set outside
        self.p.sort(lambda x, y: cmp(x.priority, y.priority))
        for primpicture in self.p:
            # print primpicture.__class__,primpicture.priority
            # print "----Draw",primpicture
            # print wstate.ctx.get_matrix()
            primpicture.draw(wstate)
            if caprim:
                s = primpicture.gse(cap)
                if s: cai.append(s)
        if caprim:
            f = open("%s.gse" % caprim, "a")
            f.write("\n draw p\n&p (")
            f.write(",\n".join("%s=(%g,%g)" % (cap[x, y], x, y) for x, y in sorted(cap)))
            if cap: f.write(",\n")
            f.write(",\n".join(cai))
            f.write("\n)\n")
            f.close()
            print "%s.gse saved (caprim)" % caprim

    def boundingbox(self, **kwargs):
        return self.realbb(WState(self, None, **kwargs), **kwargs)

    # real bounding box of the picture
    def realbb(self, wstate, **kwargs):

        pctx = wstate.ctx
        wstate.ctx = cairo.Context(tempsurface)

        bb = BBox()

        for primpicture in self.p:
            primpicture.getbb(wstate, bb)

        bb.a = P(wstate.rxy(bb.a.x, bb.a.y))
        bb.b = P(wstate.rxy(bb.b.x, bb.b.y))

        bb.b.y, bb.a.y = bb.a.y, bb.b.y
        wstate.ctx = pctx

        return bb

    def addpoint(self, p):

        self.points.append(p)
        if not self.bbox: self.bbox = BBox()
        self.bbox.addp(p)

    def getpoint(self, p, pict, tr):
        if type(p) != str:  # skip named point
            p = pict.getp(p, tr)
            self.points.append(p)
        return p

    def getvector(self, p, pict, tr):
        if type(p) != str:  # skip named point
            p = pict.getvector(p, tr)
        return p

    def getpointbyname(self, name):
        return self._points.get(name, None)

    def _save(self, w, h, wstate, curdrawing, leadingzeros, format, **kwargs):

        resolutionscale = totup(kwargs.get("resolution", (1.0, 1.0)))
        fn = self.getbasename(**kwargs)

        def cntform(d):
            return "0" * max(0, leadingzeros - len("%d" % d)) + "%d" % d

        dw, dh = int(resolutionscale[0] * w), int(resolutionscale[1] * h)
        if curdrawing:
            destf = fn + cntform(curdrawing) + "." + format
        elif leadingzeros:
            destf = fn + cntform(1) + "." + format
        else:
            destf = fn + "." + format

        if format == "png":
            surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, dw, dh)
        elif format == "svg":
            surface = cairo.SVGSurface(destf, dw, dh)
        elif format == "ps":
            surface = cairo.PSSurface(destf, dw, dh)
            surface.set_eps(False)
        elif format == "eps":
            surface = cairo.PSSurface(destf, dw, dh)
            surface.set_eps(True)
        elif format == "pdf":
            surface = cairo.PDFSurface(destf, dw, dh)
        else:
            print "Format %s is not available. File not saved." % format
            return False

        wstate.ctx = ctx = cairo.Context(surface)
        ctx.scale(1.0 * resolutionscale[0], 1.0 * resolutionscale[1])

        if wstate.background:
            ctx.set_source_rgb(*wstate.background)
            ctx.rectangle(0, 0, w, h)
            ctx.fill()

        self.draw(wstate, **kwargs)

        if format == "png":
            surface.write_to_png(destf)
        else:
            surface.finish()

        print "%s saved" % destf
        return destf

    def getbasename(self, **kwargs):
        fn = kwargs.get("outputfile", "gsevol")
        dir = kwargs.get("outputdir", "")
        if dir: fn = dir + sep + path.basename(fn)
        return fn

    def save(self, curdrawing, **kwargs):

        if "outputformat" in kwargs:
            formats = [kwargs["outputformat"]]
        else:
            formats = kwargs.get("outputformats", ['svg'])

        wstate, w, h = self.getwstate(None, **kwargs)

        return [self._save(w, h, wstate, curdrawing, kwargs.get('filecntleadingzeros', 0), format, **kwargs) for format
                in formats]

    def fill(self, cr, fillstyle, fillcolor):
        if fillstyle == "solid":
            r, g, b = fillcolor
            cr.set_source_rgb(r, g, b)
            cr.fill()
        elif type(fillstyle) == float:
            r, g, b = fillcolor
            cr.set_source_rgba(r, g, b, fillstyle)
            cr.fill()
