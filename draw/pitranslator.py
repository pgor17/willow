# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl

from htlib import drawpict, P, getcolor
from prim import PrimBox, PrimCircle, PrimNLabelMulti, \
    PrimCurve, PrimLine, PrimMark, PrimPolygon, PrimConvex, PrimNamedNode, PrimCurve, PrimBezier

from ca import Picture
from tools import ppsmpdict
from tokenizer import GseError
from pict import PointBase

import sys

defdash = [3.0, 5.0]


class Translator:

    def __init__(self, **kwargs):
        self.alx = 1.0
        self.bex = 10.0
        self.aly = -1.0
        self.bey = 10.0
        self.params = dict(**drawpict)
        self.params.update(kwargs)
        self.picture = None
        self._dash = self.params.get("dash", defdash)
        self._arrowlength = self.params.get("arrowlength", 10)
        self._linetype = self.params.get("linetype", "c-c")
        self._rotate = self.params.get("rotate", 0)
        self._extsyntax = self.params.get("extsyntax", 1)
        self._lw = self.params.get("lw", 1)
        self._color = self.params.get("color", (0, 0, 0))
        self._labelcolor = self.params.get("labelcolor", self._color)
        self._linecolor = self.params.get("linecolor", self._color)
        self._bboxpoints = self.params.get("bboxpoints", 1)
        # self._labelsep=self.params.get("labelsep",0)
        self._labelspacing = P(self.params.get("labelspacing", 0.3))
        self._fillcolor = self.params.get('fillcolor', getcolor('lightgray'))
        self._fillstyle = self.params.get('fillstyle', 'solid')
        self._pollinecolor = self.params.get('pollinecolor', self._linecolor)
        self._convexlinecolor = self.params.get('convexlinecolor', self._linecolor)
        self._boxcolor = self.params.get('boxlinecolor', self._linecolor)
        self._boxlines = self.params.get('boxlines', None)
        self._polfillcolor = self.params.get('polfillcolor', self._fillcolor)
        self._convexfillcolor = self.params.get('convexfillcolor', self._fillcolor)
        self._boxfillcolor = self.params.get('boxfillcolor', self._fillcolor)
        self._polfillstyle = self.params.get('polfillstyle', self._fillstyle)
        self._pollw = self.params.get('pollw', self._lw)
        self._convexfillstyle = self.params.get('convexfillstyle', self._fillstyle)
        self._boxfillstyle = self.params.get('boxfillstyle', self._fillstyle)
        self._convexlw = self.params.get("convexlw", self._lw)
        self._boxlw = self.params.get("boxlw", self._lw)
        self._multimarkalign = self.params.get("multimarkalign", 'l')
        self._marksize = self.params.get("marksize", 1.0)
        self._marklw = self.params.get("marklw", 1.0)
        self._multimarksep = self.params.get("multimarksep", 1.0)
        self._fontsize = self.params.get("fontsize", 12)
        self._radius = self.params.get("radius", 1.0)
        self._circlelw = self.params.get("circlelw", self._lw)
        self._circlefillcolor = self.params.get("circlefillcolor", self._fillcolor)
        self._circlecolor = self.params.get("circlecolor", self._color)
        self._circlefillstyle = self.params.get("circlefillstyle", self._fillstyle)
        self._convexparams = (50, 30, 10, 0.05)
        self.dnamed = {}

    def addptr(self, l, tr, gseaddr):
        if l.__class__ == P: l = PointBase(l, gseaddr=gseaddr)
        pos = l.getp(l, tr)
        self.picture.addpoint(pos)
        return pos

    def getdnamed(self, v):
        pos = self.dnamed.get(v, None)
        self.picture.addpoint(pos)
        return pos

    def append(self, p, priority):

        p.setpriority(priority)
        self.picture.append(p)

    def namednode(self, l, tr, **kwargs):

        try:
            pos = l.getp(l, tr)
        except Exception as e:
            print "Problem with variable/point %s. Is it defined?" % l
            sys.exit(-1)
        pos.src = l
        if self._bboxpoints == 1: self.picture.addpoint(pos)
        self.dnamed[l.name] = pos
        self.append(PrimNamedNode(pos, l.name),
                    kwargs.get("priority", kwargs.get("namednodepriority", 0)))

    def metanode(self, l, tr, **kwargs):
        return self.namednode(l, tr, **kwargs)

    def draw(self, pict, **kwargs):
        old = self.picture
        self.picture = Picture()  # aggregate in o

        pict.draw(self, [], **kwargs)

        o = self.picture
        self.picture = old
        return o

    def boundingbox(self, pict, **kwargs):
        picture = self.draw(pict, **kwargs)
        return picture.boundingbox(**kwargs)

    def labelgen(self, l, tr, nlab, **kwargs):

        lb = kwargs.get("labelcolor", self._labelcolor)
        rot = kwargs.get("rotate", self._rotate)
        gseaddr = kwargs["gseaddr"]
        if nlab:
            self.namednode(l.p, tr, **kwargs)
            pos = self.dnamed[l.p.name]
        else:
            pos = self.addptr(l, tr, gseaddr)

        if not l.s: return None, 0

        return (PrimNLabelMulti(pos, l.s, l.side,
                                lb, rot + l.getrotations(tr), kwargs.get('extsyntax', self._extsyntax),
                                P(kwargs.get('labelspacing', self._labelspacing)), kwargs.get('fontsize', 0)),
                kwargs.get("priority", kwargs.get("labelpriority", 0))
                )

    def nlabel(self, l, tr, **kwargs):
        res = self.labelgen(l, tr, True, **kwargs)
        if res[0]: self.append(*res)

    def ncurve(self, l, label, tr, **kwargs):
        self.append(
            PrimCurve(*(self.line2(l.s, l.e, label, tr, **kwargs)))
            ,
            kwargs.get("priority", kwargs.get("curvepriority", 0))
        )

    def label(self, l, tr, **kwargs):
        res = self.labelgen(l, tr, False, **kwargs)
        if res[0]: self.append(*res)

    def node(self, l, tr, **kwargs):
        pass

    def nline(self, __l, __label, __tr, **kwargs):
        self.append(PrimLine(*(self.line2(__l.s, __l.e, __label, __tr, **kwargs)[:-5])),
                    kwargs.get("priority", kwargs.get("linepriority", 0))
                    )

    def nbox(self, __l, __tr, **kwargs):
        a = self.getpoint(__l.s, __tr, kwargs['gseaddr'])
        b = self.getpoint(__l.e, __tr, kwargs['gseaddr'])
        self.append(PrimBox(a, b,
                            kwargs.get('lw', kwargs.get('boxlw', self._boxlw)),
                            kwargs.get('color', kwargs.get('boxcolor', self._boxcolor)),
                            kwargs.get('fillcolor', kwargs.get('boxfillcolor', self._boxfillcolor)),
                            kwargs.get('fillstyle', kwargs.get('boxfillstyle', self._boxfillstyle)),
                            kwargs.get('boxlines', self._boxlines))
                    ,
                    kwargs.get("priority", kwargs.get("boxpriority", 0)))

    def line2(self, __a, __b, __label, __tr, **kwargs):
        up = kwargs.get('up', 1)
        lt = kwargs.get("type", kwargs.get("linetype", self._linetype))
        if not lt: lt = "c-c"
        if len(lt) == 1: lt = "c" + lt + "c"
        if len(lt) != 3:
            print "Warning: linetype len should be 1 or 3"
            lt = 'c-c'
        if lt[1] == ".":
            dash = kwargs.get("dash", self._dash)
        else:
            dash = None
        al = kwargs.get("arrowlength", self._arrowlength)
        ex = kwargs.get("extsyntax", self._extsyntax)

        a = self.getpoint(__a, __tr, kwargs['gseaddr'])
        b = self.getpoint(__b, __tr, kwargs['gseaddr'])
        labelalign = kwargs.get('labelalign', 0.5)
        # print "FLIP",'autofliplabels' in kwargs
        repl = 0
        if __label and kwargs.get('autofliplabels', 1):
            if a.x >= b.x:
                a, b = b, a
                repl = 1
                labelalign = 1.0 - labelalign
                if lt == "c->":
                    lt = "<-c"
                elif lt == "<-c":
                    lt = "c->"
                up = not up

        crval = kwargs.get('crval', 0)
        crvbe = kwargs.get('crvbe', 0)
        endat = kwargs.get('endat', 0)
        curvetype = kwargs.get('curvetype', 0)

        if curvetype:
            crval, crvbe = P(crval), P(crvbe)
            alx, bex = crval.ln(), crvbe.ln()

        if curvetype == 1:
            # point
            crval = self.getpoint(crval, __tr, kwargs['gseaddr'])
            crvbe = self.getpoint(crvbe, __tr, kwargs['gseaddr'])
        elif curvetype == 2:
            # vector
            crval = self.getvector(crval, __tr, kwargs['gseaddr'])
            crvbe = self.getvector(crvbe, __tr, kwargs['gseaddr'])
        elif curvetype == 3:
            # vector
            crval = self.getvector(crval, __tr, kwargs['gseaddr'])
            crvbe = self.getvector(crvbe, __tr, kwargs['gseaddr'])
            crval = (crval / crval.ln()) * alx
            crvbe = (crvbe / crvbe.ln()) * bex
            curvetype = 2
        if repl:
            crval, crvbe = crvbe, crval
            if endat: endat = 1.0 - endat

        lc = kwargs.get("color", kwargs.get("linecolor", self._linecolor))

        # print lc,self._linecolor,kwargs.get("color","--"),kwargs.get("linecolor","--")
        # print "LT",lt,kwargs.get('linetype','--')

        return (a, b,
                kwargs.get("lw", self._lw),
                lc,
                lt,
                __label,
                kwargs.get("labelcolor", self._labelcolor),
                dash, ex,
                al,
                kwargs.get("labelspacing", self._labelspacing),
                labelalign,
                kwargs.get("fontsize", 0),
                up,
                crval, crvbe,
                curvetype,
                endat
                )

    def line(self, l, label, tr, **kwargs):
        gseaddr = kwargs['gseaddr']
        self.append(PrimLine(
            *(self.line2(self.getpoint(l.a, tr, gseaddr), self.getpoint(l.b, tr, gseaddr), label, tr, **kwargs)[:-3])))

    def _getsmpmark(self, _ap, _n, _rot, **kwargs):
        df = kwargs
        # ppdiff(kwargs,self.params)

        if _n < 0:
            markdict = df.get('dltmarks', self.params['dltmarks'])[_n]  # from env
        else:
            markdict = df.get('marks', self.params['marks'])[_n]
        d = dict(marksize=df.get("defmarksize", self._marksize),
                 marklw=df.get("defmarklw", self._marklw),
                 markfillstyle='solid',  # TODO: rest of def
                 markyratio=1.0,
                 markcolor='black',
                 markfillcolor='red')
        d.update(markdict)
        d.update(df)  # highest

        return PrimMark(_ap, d['marktype'], d['marksize'], d['marklw'], d['markcolor'],
                        d['markfillcolor'], d['markfillstyle'], d['markyratio'], _rot)

    def nmark(self, __l, __tr, **kwargs):
        a = self.getpoint(__l.p, __tr, kwargs['gseaddr'])
        # if type(ap)==str: ap=self.o.getpointbyname(ap)
        phi = __l.getrotations(__tr)
        self.append(
            self._getsmpmark(a, kwargs['mark'], phi, **kwargs),
            kwargs.get("priority", kwargs.get("markpriority", 0))
        )

    def multimark(self, __l, __ml, __tr, **kwargs):
        df = self.params.copy()
        df.update(kwargs)
        p = self.getpoint(__l.p, __tr, kwargs['gseaddr'])
        if type(p) == str: p = self.o.getpointbyname(p)
        # res=[ self._getsmpmark(df,ap,n) for n in ml ]

        # kwargs.get("linetype",self._linetype)
        multimarkalign = kwargs.get('multimarkalign', self._multimarkalign)
        marksize = kwargs.get('marksize', self._marksize)
        size = marksize * 0.45 * kwargs.get('fontsize', self._fontsize)

        multimarksep = kwargs.get('multimarksep', self._multimarksep) * 2.0 * size
        phi = __l.getrotations(__tr)

        # TODO - individual mark sizes!
        if multimarkalign == 'r':
            p -= P((len(__ml) - 1) * multimarksep, 0).rotatedeg(phi)
        elif multimarkalign == 'c':
            p -= P(0.5 * (len(__ml) - 1) * multimarksep, 0).rotatedeg(phi)

        # TODO: rotate individual marks
        for i, gp in enumerate(__ml):
            self.append(self._getsmpmark(p + P(i * multimarksep, 0).rotatedeg(phi), gp, phi, **df),
                        kwargs.get("priority", kwargs.get("markpriority", 0))
                        )

    def getpoint(self, a, tr, gseaddr):
        # print "GPT",a,type(a),a.__class__
        if a is None: return a

        res = self.getdnamed(a) if type(a) == str else self.addptr(a, tr, gseaddr)
        if not res:
            raise GseError("Undefined node label <%s>" % a, gseaddr)
        return res

    def getvector(self, a, tr, gseaddr):
        # print "GPV",a,type(a),a.__class__
        if type(a) == str:
            res = self.getdnamed(a)
        else:
            if a.__class__ == P: a = PointBase(a, gseaddr=gseaddr)
            res = a.getvector(a, tr)

        if not res:
            raise GseError("Undefined node/vector label <%s>" % a, gseaddr)
        return res

    def polygon(self, p, tr, **kwargs):
        pts = [self.getpoint(a, tr, kwargs['gseaddr']) for a in p.p]
        self.append(PrimPolygon(pts,
                                kwargs.get("lw", kwargs.get("pollw", self._pollw)),
                                kwargs.get('color',
                                           kwargs.get('polcolor', kwargs.get('pollinecolor', self._pollinecolor))),
                                kwargs.get('fillcolor', kwargs.get('polfillcolor', self._polfillcolor)),
                                kwargs.get('fillstyle', kwargs.get('polfillstyle', self._polfillstyle))),
                    kwargs.get("priority", kwargs.get("polygonpriority", 0)))

    def convex(self, p, tr, **kwargs):
        pts = [self.getpoint(a, tr, kwargs['gseaddr']) for a in p.p]
        self.append(PrimConvex(pts,
                               kwargs.get("convexlw", self._convexlw),
                               kwargs.get('convexcolor', kwargs.get('convexlinecolor', self._convexlinecolor)),
                               kwargs.get('convexfillcolor', self._convexfillcolor),
                               kwargs.get('convexfillstyle', self._convexfillstyle),
                               kwargs.get('convexparams', self._convexparams)
                               ),
                    kwargs.get("priority", kwargs.get("convexpriority", 0)))

    def bezier(self, p, tr, **kwargs):
        pts = [self.getpoint(a, tr, kwargs['gseaddr']) for a in p.p]
        self.append(PrimBezier(pts,
                               kwargs.get("lw", kwargs.get("pollw", self._pollw)),
                               kwargs.get('color',
                                          kwargs.get('polcolor', kwargs.get('pollinecolor', self._pollinecolor))),
                               kwargs.get('fillcolor', kwargs.get('polfillcolor', self._polfillcolor)),
                               kwargs.get('fillstyle', kwargs.get('polfillstyle', self._polfillstyle))),
                    kwargs.get("priority", kwargs.get("bezierpriority", 0)))

    def ncircle(self, l, tr, **kwargs):
        ap = self.getpoint(l.p, tr, kwargs['gseaddr'])

        return self.append(PrimCircle(ap,
                                      kwargs.get('radius', kwargs.get('circleradius', self._radius)),
                                      kwargs.get('lw', kwargs.get('circlelw', self._circlelw)),
                                      kwargs.get('color', kwargs.get('circlecolor',
                                                                     kwargs.get('circlelinecolor', self._circlecolor))),
                                      kwargs.get('fillcolor', kwargs.get('circlefillcolor', self._circlefillcolor)),
                                      kwargs.get('fillstyle', kwargs.get('circlefillstyle', self._circlefillstyle))
                                      ),
                           kwargs.get("priority", kwargs.get("circlepriority", 0))
                           )

    def box(self, b, tr, **kwargs):

        # print b.a,type(b),b.a.__class__
        # print b.b
        pos1 = self.getpoint(b.a, tr, kwargs['gseaddr'])
        pos2 = self.getpoint(b.b, tr, kwargs['gseaddr'])
        # pos1=self.o.getpoint(b.a,b,tr)
        # pos2=self.o.getpoint(b.b,b,tr)
        self.append(PrimBox(pos1, pos2,
                            kwargs.get('lw', kwargs.get('boxlw', self._boxlw)),
                            kwargs.get('color', kwargs.get('boxcolor', self._boxcolor)),
                            kwargs.get('fillcolor', kwargs.get('boxfillcolor', self._boxfillcolor)),
                            kwargs.get('fillstyle', kwargs.get('boxfillstyle', self._boxfillstyle)),
                            kwargs.get('boxlines', self._boxlines)),
                    kwargs.get("priority", kwargs.get("boxpriority", 0))
                    )
