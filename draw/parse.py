# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl


from htlib import GseError
from optparse import OptionParser

import textwrap

from tokenizer import collectinput, toktokenize, tokenizegroups, pptokens
from frontenddefs import *
from random import randint
import sys

from evallib import InstrPythAssignment, InstrBlock, InstrDrawGen, InstrEnd, InstrEval
from evallib import InstrType, InstrObj, InstrNop
from evallib import getevalsrc
from evallib import getrest


def getobj(tokens, src, obj):
    while tokens and tokens[0].typ != GSESEP:  # till next separator on the highest level
        t = topt(tokens, src, "")
        obj.append(nextt(tokens, src, ""))
    if not obj: raise GseError("Empty obj?", src)
    return obj


# grouping by instructions on top level
def gparse(tokens, variables):
    def getblock(tokens, src, instrlist):
        blsrc = src

        if not blsrc and tokens: blsrc = tokens[0]
        while tokens:

            instrlist.append(getinstr(tokens, src))
            while True:
                src = topt(tokens, None, "")
                if not src: break
                if src.typ == GSESEP or src.typ == GSESPACE:
                    nextt(tokens, src, "")  # eat whitespaces
                else:
                    break

        return InstrBlock(blsrc, instrlist)

    def getinstr(tokens, src):

        src = nextt(tokens, src, whitespaces)  # prev no whitespaces

        if src.typ == GSEGROUP and src.token == "{":
            return getblock(src.grp, src, [])
        if src.typ == GSEGROUP and src.token == "(": return InstrObj(src, OBJTYPEANY, getobj(tokens, src, [src]))

        if src.token == "&":  # objects
            t = topt(tokens, src, '')  # check if type is given; not spacing allowed
            objtype = OBJTYPEANY
            tp = None
            if t.typ == GSENAME:
                objtype = t.token  # valid type; important: no spaces inbetween
                tp = t

                nextt(tokens, src, whitespaces)
                topt(tokens, src, whitespaces)  # this time eat leading whitespaces after &TYP

            t = topt(tokens, src, whitespaces)
            # grouping when &[TYPE] { .... }
            if t.token == "{":
                il = []
                if tp: il.append(InstrType(src, [tp]))
                bl = getblock(t.grp, t, il)
                if objtype != OBJTYPEANY: il.append(InstrEnd(src))
                return bl
            else:
                return InstrObj(src, objtype, getobj(tokens, src, []))

        if src.typ == GSENAME:

            if src.token == 'draw':

                tsep = topt(tokens, src, '')  # to avoid removing space in objects
                t = topt(tokens, src, " \t")  # not eoln here
                # check if assignment if not then proceed to draw
                if t and t.token != "=":

                    col = ''
                    # draw is present                    
                    drawspec = nextt(tokens, src, whitespaces).token

                    if tokens:
                        # print drawspec,params,tokens
                        t = topt(tokens, src, '\t ', 1)  # prev EOLN
                        if t:
                            if t.token == ':':
                                col = ":"
                                nextt(tokens, src, whitespaces)

                    return InstrDrawGen(src, drawspec, None, {}, None)

            if src.token == "type": return InstrType(src, getrest(tokens, src))

            if src.token == "eval": return InstrEval(src, getobj(tokens, src, []))
            if src.token == KTYPEEND: return InstrEnd(src)
            if src.token == KTYPEENDX: return InstrEnd(src, 1)

            tsep = topt(tokens, src, '')  # to avoid removing space in objects
            t = topt(tokens, src, " \t")  # not eoln here
            if t and t.token == "=":
                # Assignment          
                nextt(tokens, src, whitespaces)  # eat=
                t = topt(tokens, src, whitespaces)  # eat whitespaces again
                return InstrPythAssignment(src, src.token, getobj(tokens, src, []))

            if tsep == t:
                go = getobj(tokens, src, [src])
            else:
                go = getobj(tokens, src,
                            [src, tsep])  # this sep is required; otherwise object: a b -> ab
            return InstrObj(src, OBJTYPEANY, go)  # possible many objects

        if src.typ == GSENUMBER or src.typ == GSESTRING:
            return InstrObj(src, OBJTYPEANY, getobj(tokens, src, [src]))

        if src.typ == GSEEOF:
            if tokens:
                raise GseError("Symbols left after GSEEOF: %s" % getsrc(tokens), src)
            return InstrNop(src)

        if src.typ == GSECNF:
            gr = nextt(tokens, src, whitespaces)
            bl = getblock(gr.grp, src, [])
            for i in bl.instrlist: i.cnf = 1
            bl.cnf = 1
            return bl

        raise GseError("Unknown token <%s>" % src.token, src)

    if not tokens or tokens[-1].typ != GSEEOF:
        tokens.append(tokEOF)

    firsttoken = tokens[0]
    mainblock = getblock(tokens, None, [])
    mainblock.firsttoken = firsttoken

    return mainblock


import optparse
from optparse import HelpFormatter


class PlainHelpFormatter(HelpFormatter):
    def __init__(self):
        kargs = {'indent_increment': 1, 'short_first': 1,
                 'max_help_position': 10, 'width': None}
        HelpFormatter.__init__(self, **kargs)

    def format_usage(self, usage):
        return optparse._("%s") % usage.lstrip()

    def format_heading(self, heading):
        return "%*s%s:\n" % (self.current_indent, "", heading)

    def format_option(self, option):
        result = []
        opts = self.option_strings[option]
        opt_width = self.help_position - self.current_indent - 2
        if len(opts) > opt_width:
            opts = "%*s%s\n" % (self.current_indent, "", opts)
            indent_first = self.help_position
        else:  # start help on same line as opts
            opts = "%*s%-*s  " % (self.current_indent, "", opt_width, opts)
            indent_first = 0
        result.append(opts)
        if option.help:
            help_text = self.expand_default(option)
            print "!!!", help_text, "????"
            help_lines = help_text.split("\n") + [""]
            # textwrap.wrap(help_text, self.help_width)
            result.append("%*s%s\n" % (indent_first, "", help_lines[0]))
            result.extend(["%*s%s\n" % (self.help_position, "", line)
                           for line in help_lines[1:]])
        elif opts[-1] != "\n":
            result.append("\n")

        return "".join(result)


def getparser():
    usage = """drwillow.py [options] [inputfile1 inputfile2 ...]
    """

    # class MyParser(OptionParser):
    #     def format_epilog(self, formatter):
    #         return self.epilog

    parser = OptionParser(usage, formatter=PlainHelpFormatter())

    parser.add_option(
        "-o", "--outputdir", action="store",
        help="output directory; default is .")

    parser.add_option(
        "-p", "--forcepdf", action="store_true",
        help="force creation of pdf(s)")

    parser.add_option(
        "-P", "--forcepng", action="store_true",
        help="force creation of png(s)")

    parser.add_option(
        "-d", "--draw", action="store",
        help="""draw based on the following letters:
  S - svg output (default),
  P - png output,
  E - encapsulated postscript output (eps),
  O - postscript output (ps),
  D - pdf output
  G - graphs (&G)""")

    parser.add_option(
        "-C", "--config", action="store",
        help="""config definition a file or a string; def. source is .htree; 
          format: 'key=value,key=value,...'""")    

    return parser


def parsefromopts(argv):
    debugparse = 0

    # we don't like processing *.py
    argv = [a for a in argv if a[-3:] != ".py"]

    # Parse options
    parser = getparser()
    res, options = collectinput(parser, argv)

    sourcescript = res
    res = tokenizegroups(toktokenize(res))

    if debugparse:
        print >> sys.stderr, "=" * 80
        print >> sys.stderr, pptokens(res)

    def stripeof(p):
        if p and p[-1].typ == GSEEOF: del p[-1]

    stripeof(res)
    res = res + [tokEOF]

    block = gparse(res, {})
    block.setlev(0)

    return options, block
