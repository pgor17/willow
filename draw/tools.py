# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl

def ppsmpdict(d, x=''):
    def cut(s):
        return s if len(s) < 10 else s[:9] + "..."

    r = ''
    for k in d:
        if x in k:
            if type(d[k]) == dict or type(d[k]) == list:
                r += " %s={%s}" % (k, cut(str(d[k])))
            else:
                r += "%s=%s" % (k, d[k])
            r += " "
    return r


def getsubd(d, n, all=0):
    dres = {}
    ln = len(n)
    for k in d:
        if len(k) > ln and k[ln] == '.' and k[:ln] == n:
            dres[k[ln + 1:]] = d[k]
        elif all and k not in dres:
            dres[k] = d[k]
    return dres


def convex_hull(points):
    def turn(p, q, r):
        return cmp((q[0] - p[0]) * (r[1] - p[1]) - (r[0] - p[0]) * (q[1] - p[1]), 0)

    def _keep_left(hull, r):
        while len(hull) > 1 and turn(hull[-2], hull[-1], r) != 1:
            hull.pop()
        if not len(hull) or hull[-1] != r:
            hull.append(r)
        return hull

    points = sorted(points)
    l = reduce(_keep_left, points, [])
    u = reduce(_keep_left, reversed(points), [])
    return l.extend(u[i] for i in xrange(1, len(u) - 1)) or l
