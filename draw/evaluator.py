# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl

import sys

from frontenddefs import *
from marks import genmarks
from pict import Group
from tools import getsubd

OBJECTFLAGS = ['G']
doperations = ['scale', 'scaleto', 'rotate']

from pict import Scale, ScaleTo, Rotate

doperations2 = (Scale, ScaleTo, Rotate)
debugcontext = 0


def ppgseobject(t, eo):
    if type(t) == list: return _ppgseobjects(t, eo)

    s = None
    res = ''

    if isinstance(t, Graph):
        if eo.potypeflag: res = "&G "
        if s: return res + "%s" % s
        return res + "%s" % t

    print "Unknown type to print?"


def _ppgseobjects(t, eo):
    if not t: return ''

    return "\n".join(ppgseobject(i, eo) for i in t)


class VariableInfo:
    def __init__(self, label, val, time, addr, oldvi):
        self.label = label
        self.val = val
        self.time = time
        self.addr = addr
        self.oldvi = oldvi

    def __str__(self): return self.label + "=" + str(self.val)

    def __repr__(self): return self.__str__()


class EoPict:
    def __init__(self, gr, vs, varset, src):
        self.empty = False
        if not gr:
            gr = Group(gseaddr=src)
            self.empty = True
        else:
            gr = Group(gr, gseaddr=src)
        self.gr = gr
        self.vs = vs
        self.varset = varset
        self.src = src

    def apply(self, opers):
        gr = self.gr
        for op, vi in opers:
            gr = op(gr, vi.val, gseaddr=vi.addr, **self.vs)
        return EoPict(gr, self.vs, self.varset, self.src)


class DrawContext:
    def __init__(self, eo, spec, drawing):
        self.startkeep = len(eo.keepstack)
        self.spec = spec  # what to draw
        # self.typestrstacklen=len(eo.typestrstack)
        self.qtime = eo.qtime
        self.contextobj = {}
        for t in eo.objvariables: self.contextobj[t] = len(eo.objvariables[t])
        self.opers = []
        self.rc = []
        self.picts = []
        self.level = len(eo.contextstack)
        self.drawing = drawing
        if debugcontext:
            self.spc = "  " * self.level
            print self.spc + "New Draw Context <%s> lev=%d draw=%d qtime=%d" % (
            self.spec, self.level, self.drawing, self.qtime)

    def finalize(self, eo):
        for i in xrange(self.startkeep, len(eo.keepstack)): eo.restore()
        if self.drawing:
            # remove
            for t in self.contextobj: eo.objvariables[t] = eo.objvariables[t][:self.contextobj[t]]
        # eo.typestrstack=eo.typestrstack[:self.typestrstacklen]

        return self.picts

    def newobjtype(self, typ):
        self.contextobj[typ] = 0

    def addpicts(self, plist):
        for eopict in plist: self.addpict(eopict)

    def insertdraw(self, gr, vs, varset, src):
        self.addpict(EoPict(gr.pict(**vs), vs, varset, src))

    def getpicts(self, n):
        t = self.picts[:n]
        del self.picts[:len(t)]
        return t

    def addpict(self, eopict):
        # gr,vs,varset,src):
        opers = self.getopers(eopict.varset, eopict.vs['envqtime'])
        gr = eopict.gr
        for vi in opers:
            # sprint "OPER%d"%self.level,varset,vi.label,vi.val,src
            gr = vi.op(gr, vi.val, gseaddr=vi.addr, **eopict.vs)

        # print "LEV%d"%self.level,"inserting"
        if debugcontext:
            print self.spc + "new-pict #%d" % len(self.picts), varset, src, vs['envqtime']
        self.picts.append(EoPict(gr, eopict.vs, eopict.varset, eopict.src))

    # convert PObject (trees, mappings) into translated form
    def genpictures(self, d):
        res = []
        if debugcontext:
            print self.spc + "draw! pictslen=%d" % len(self.picts)
        # for i,(gr,vs,varset,src) in enumerate(self.picts):
        for i, eopict in enumerate(self.picts):

            # TODO: CHECK
            fn = eopict.varset
            if 'filename' in eopict.vs:
                if i >= 0:
                    fn = "%s%d" % (kw['filename'], i)
                else:
                    fn = kw['filename']
            eopict.vs['filename'] = fn

            if hasattr(eopict.gr, 'treename') and 'filenamebytreename' in eopict.vs:
                eopict.vs['filename'] = eopict.gr.treename
            # print "==="*80,"\n",env.printenvext(vs)

            res.append((eopict.vs, eopict.varset, eopict.src, d.draw(eopict.gr.pict(**eopict.vs), **eopict.vs)))
        return res

    def getopers(self, varset, qtime):
        r = {}
        for op in self.opers:
            if op.label in doperations:
                if op.time < qtime:
                    r[-op.time] = op
                continue
            for k in doperations:
                if varset + "." + k == op.label:
                    if op.time < qtime:
                        r[-op.time] = op

        return [r[i] for i in sorted(r)]


class GseEvaluator:
    def __init__(self, addr, options, extvariables=None):

        self.variables = {}  # mapping variables -> values
        self.cnfvariables = set()  # defined in config (constants)
        self.variableinfo = {}
        self.objvariables = {}  # mapping variables -> values
        for t in OBJECTFLAGS: self.objvariables[t] = []
        self.typestrstack = []
        self.typestr = ['G']
        self.typestrpos = 0
        self.keepstack = []
        self.contextstack = []
        self.addr = addr
        self.qtime = 0

        d = {}

        self.setvariable("flatten", 0.001, addr)
        self.setvariable("marks", genmarks("rgb myc BWG"), addr)
        self.setvariable("defmarksize", 1.0, addr)
        self.setvariable("defmarklw", 0.25, addr)
        self.setvariable("extsyntax", 1, addr)  # default)
        self.setvariable("crval", 0.4, addr)
        self.setvariable("crvbe", 0.15, addr)

        if extvariables: self.variables.update(extvariables)

        self.getoptions(options)

        self.context = DrawContext(self, self.drawopt, 1)

    def getoptions(self, options):
        from tokenizer import GseToken, GSEOPT
        gseaddr = GseToken(GSEOPT, 'CommandLineOption')

        self.ignorelostprefix = 0
        self.dltcostformat = "!n !c"
        self.potypeflag = 1
        self.vhtreeprint = 0

        if options.outputdir: self.setvariable('outputdir', options.outputdir, gseaddr)

        self.printenv = 0

        self.drawopt = options.draw

        if self.drawopt:
            if options.outputdir:
                self.setvariable('outputdir', options.outputdir, gseaddr)
            else:
                self.setvariable('outputdir', ".", gseaddr)
        else:
            self.drawopt = ''

        of = []

        if options.forcepdf:
            of = ['pdf']
        elif options.forcepng:
            of = ['png']
        elif self.drawopt:
            if "P" in self.drawopt: of.append("png")
            if "E" in self.drawopt: of.append("eps")
            if "O" in self.drawopt: of.append("ps")
            if "D" in self.drawopt: of.append("pdf")
            if "S" in self.drawopt or not of: of.append("svg")

        # print of
        self.setvariable('outputformats', set(of), gseaddr)

    def extrenv(self, l):

        def _exx(o):

            if o.qtime < self.context.qtime:  # object is defined before
                env = self.variables.copy()
                envqtime = self.qtime
                objqtime = o.qtime
            else:
                env = o.env.copy()  # environment from the definition location
                objqtime = envqtime = o.qtime
            env['envqtime'] = envqtime
            env['objqtime'] = objqtime
            # env['opers']=self.getopers
            if debugcontext:
                print "CHECKING%d" % self.qtime, o.qtime, self.context.qtime, "res=", envqtime
            return o, env

        return [_exx(o) for o in l]

    def getGl(self):
        return self.extrenv(self.objvariables['G'])

    def get(self, n):
        return getsubd(self.variables, n, 1)

    # def gets(self,n):
    # 	return getsubd(self.variables,n,0)

    def newdrawcontext(self, spec, drawing):
        self.contextstack.append(self.context)
        self.context = DrawContext(self, spec, drawing)

    def save(self, d):
        pictures = self.context.genpictures(d)
        dnums = {}
        res2 = []
        for i, (vs, varset, src, picture) in enumerate(pictures):
            b = picture.getbasename(**vs)
            if b not in dnums:
                dnums[b] = 0
            else:
                dnums[b] += 1
            picture._b = b
            picture._dnumsb = dnums[b]

        saved = []
        for i, (vs, _, _, picture) in enumerate(pictures):
            c = picture._dnumsb
            b = picture._b

            if c == 0 and dnums[b] == 0:
                sf = picture.save(0, **vs)
            else:
                sf = picture.save(c + 1, **vs)
            saved.extend(sf)
        return saved

    def deldrawcontext(self):
        # restore values of variables created in this context
        picts = self.context.finalize(self)
        if debugcontext:
            print self.context.spc + "ContextFinalized lev=%d" % self.context.level, " pictures", len(picts)
        self.context = self.contextstack.pop(-1)

        # insert pictures into higher level
        self.context.addpicts(picts)

    # apply operations from the current context

    def getopers2(self, varset):
        res = []
        for k in doperations:
            vss = varset + "." + k
            if vss not in self.variables: continue
            # print i,varset,vs['envqtime'],vs['objqtime'],vs.get(varset+".scale",-1),
            vi = self.variableinfo.get(vss, 0)
            while vi and vi.time > self.context.qtime: vi = vi.oldvi
            # if vi: print ">>",vi.time,vi.val
            if vi: res.append((doperations2[doperations.index(k)], vi))
        res = sorted(res, key=lambda o: o[1].time)

        return res

    def getpicts(self, n):
        t = self.context.getpicts(n)
        # print "QT",self.qtime,self.context.qtime
        res = []
        for i, eopict in enumerate(t):
            opers = self.getopers2(eopict.varset)
            res.append(eopict.apply(opers))

        return res

    def evaluateinstr(self, b):
        if self.drawopt:
            from evallib import InstrDrawGen
            i = InstrDrawGen(self.addr, [GseToken(GSESTRING, "'" + self.drawopt + "'")], None, None, None)
            i.body = b
            b = i
        b.eval(self)

    def addobjects(self, typ, src, token):
        for i in src:
            for j in i:
                self._addobject(typ, j, token)

    def _addobject(self, typ, src, token):
        # print "addobj",typ,type(src),len(src),src
        self.qtime += 1
        # debugcontext=1 #!!!!
        # if debugcontext:
        #	print >>sys.stderr,"OBJ: &%s %s qtime=%d"%(typ,src,self.qtime)
        from evallib import GseObj

        if len(src) > 2 and src[0] == '&':
            if typ != "." and typ != src[1]:
                print >> sys.stderr, "Warning: conficting object types: required &%s found &%s" % (typ, src[1])
            typ = src[1]
            src = src[2:].lstrip()

        o = GseObj(typ, src, token, self.qtime, self.variables.copy())
        if typ == ".":
            # use typestr
            typ = self.typestr[self.typestrpos]
            self.typestrpos = (self.typestrpos + 1) % len(self.typestr)
        if typ not in self.objvariables:
            self.objvariables[typ] = []
            self.context.newobjtype(typ)
        self.objvariables[typ].append(o)
        self.qtime += 1

    def keep(self, label):
        pres = label in self.variables
        self.keepstack.append((label, self.variableinfo.get(label, None)))
        return self.keepstack[-1][1]

    def setvariable(self, label, val, addr, cnf=0):
        self.qtime += 1
        if debugcontext:
            if hasattr(self, 'context'): print self.context.spc[:-1],
            print "%s=%s" % (label, val)
        if label == 'dltcostformat': self.dltcostformat = val
        if label in self.cnfvariables:
            # Ignored
            return
        if cnf: self.cnfvariables.add(label)
        oldvi = self.keep(label)
        self.variables[label] = val

        vi = self.variableinfo[label] = VariableInfo(label, val, self.qtime, addr, oldvi)
        last = label.split(".")[-1]
        if last in doperations:
            vi.op = doperations2[doperations.index(last)]
            self.context.opers.append(vi)
        self.qtime += 1

    def getopers(self, varset):
        return self.context.getopers(varset)

    def restore(self):
        label, vi = self.keepstack.pop(-1)
        if not vi:
            del self.variables[label]
            del self.variableinfo[label]
        else:
            self.variableinfo[label] = vi
            self.variables[label] = vi.val

    def settype(self, newtypestr):
        self.typestrstack.append(self.typestr)
        self.typestrstack.append(self.typestrpos)
        self.typestrpos = 0
        self.typestr = newtypestr

    def endtype(self):
        self.typestrpos = self.typestrstack.pop(-1)
        self.typestr = self.typestrstack.pop(-1)

    def draw(self, gr, vs, varset, src):

        self.qtime += 1
        gr.qtime = vs['envqtime']
        self.context.insertdraw(gr, vs, varset, src)
