#!/usr/bin/python

import sys

from evaluator import GseEvaluator
from parse import parsefromopts
from htlib import GseError
from pitranslator import Translator

# SUBOPTION PROCESSING -> evallib.py

if __name__ == "__main__":
  
  
    try:
        options, mainblock = parsefromopts(sys.argv)
       
        env = GseEvaluator(mainblock.src, options)
        env.evaluateinstr(mainblock)
        if env.printenv: env.pp()
        saved = env.save(Translator(**env.variables))

    except GseError as e:
        print "Error in section"
        print e
        sys.exit(-1)  


    # Happy to end here :)
