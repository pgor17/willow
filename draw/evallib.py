# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl


from frontenddefs import *
from tokenizer import tokenizegroups

from subprocess import check_output, CalledProcessError, STDOUT
from htlib import GseError
from marks import genmark, _setmark, genmarks, _addmark
import sys
from pict import Group
from math import sqrt
from tools import getsubd
from evaluator import EoPict
from pict import NCircle
import traceback

debuginterpreter = 0


def getrest(tokens, src, grptok="{"):
    t = nextt(tokens, src, "")
    if t.typ == GSEGROUP and t.token == grptok: return t.grp
    rest = [t]
    while tokens and tokens[0].typ != GSESEP: rest.append(nextt(tokens, src, ""))
    return rest


def setmark(i, **kwargs):
    return _setmark(env, i, **kwargs)


def addmark(**kwargs):
    return _addmark(env, **kwargs)


import string


def evalpythonic(s, src, eo):
    return _evalpythonic(s, src, eo.variables, eo.objvariables)


def _evalpythonic(s, src, vars, objvars):
    try:
        from htlib import P
        import itertools, math, random

        d = vars.copy()
        d.update(objvars)
        g = globals()
        g['env'] = vars
        g['P'] = P
        d['gseaddr'] = src
        g['itertools'] = itertools
        g['math'] = math
        g['random'] = random
        s = s.strip()
        # TODO: config variables?
        if debugpythonic:
            print >> sys.stderr, "PythonicExpr<%s>" % s
            # print "EVALPYTH:===%s==="%(s)
        z = eval(s, g, d)
        if debugpythonic: print >> sys.stderr, "PythonicExpr-RES<%s>" % [i for i in str(z)]
        return z
    except Exception as e:

        if debugpythonic:
            import traceback
            print >> sys.stderr, "PythonicExpr-Exception<%s>" % e
            traceback.print_exc()

        # import traceback
        # print >>sys.stderr,"PythonicExpr-Exception<%s>"%e        
        # traceback.print_exc()
        raise GseError("[1] Cannot evaluate pythonic expr <%s>: %s" % (s, str(e)), src)


def sngcompr(l):
    c = ''
    res = []
    for i in l:
        if type(i) == str:
            c += i
        elif type(i) == list:
            print "Warning: ll not expanded?"
            if c: res.append(c)
            c = ''
            res.append(i)
        else:
            # print "C1=<%s>"%c,type(i),i.__class__
            c += str(i)
            # print "C=<%s>"%c
    if c: res.append(c)
    return res


def evalobj(eo, obj, instr=0):
    def resproduct(r1, r2):
        return [i + j for i in r1 for j in r2]

    def resappend(r, elt):
        for i in r:
            if i and type(i[-1]) == str and type(elt) == str:
                i[-1] += elt
            else:
                i.append(elt)

    res = [[]]
    # debugeval=1

    if debugeval:
        print >> sys.stderr, "EVALOBJ", pptokens(obj, '', 1)

    srcobj = obj
    obj = obj[:]
    while obj:
        src = topt(obj, None, "")
        if not src: break
        nextt(obj, src, "")
        if src.typ == GSEGROUP:
            if instr and src.token == '{': resappend(res, src.token)
            # print "GSE",src.token,instr,res,src.grp
            res = resproduct(res, evalobj(eo, src.grp[:], instr))
            if instr and src.token == '{': resappend(res, closegroup[src.token])

        elif src.typ == GSENAME:
            if src.token in eo.variables:

                t = topt(obj, src, '', 1)  # eat whitespaces
                if t and t.token == '=':
                    # skip this one 
                    resappend(res, src.token)
                    continue
                v = eo.variables[src.token]
                if type(v) == list:
                    res = resproduct(res, [[i] for i in v])
                else:
                    resappend(res, str(v))

            elif src.token in eo.objvariables:
                potarg = topt(obj, None, "")
                if potarg and potarg.token == '[':
                    args = getrest(obj, src, "[")

                    v = evalpythonic(src.token + getevalsrc(args, ''), src, eo)

                    if type(v) == list:
                        res = resproduct(res, [[i] for i in v])
                    else:
                        resappend(res, str(v))


                else:
                    # igoring (avoid expanding -s, -g in gsevol/urec/bash)
                    resappend(res, src.token)
            else:
                resappend(res, src.token)
        elif src.typ == GSESTRING and src.token[0] == "'":
            stokens = tokenizegroups(tokenize(src.token[1:-1]))

            if len(stokens):  # avoid empty strings
                ll = evalobj(eo, stokens, 1)
                res = resproduct(res, [["'" + i[0] + "'"] for i in ll])
            else:
                res = resproduct(res, [[src.token]])  # check!
        else:
            resappend(res, src.token)

    if debugeval:
        print >> sys.stderr, "EVALOBJ-RES[%s]" % getevalsrc(srcobj, ''), res

    return map(sngcompr, res)  # compress strings


def getevalsrc(toklist, delim=' '):
    if type(toklist) == str: return toklist
    return delim.join(t.getevalsrc() for t in toklist)


import imp


class GseObj:
    def __init__(self, typ, src, token, qtime, env):
        self.typ = typ
        self.src = src
        self.env = env
        self.qtime = qtime
        self.gseaddr = token  # the first token of object

    def __str__(self):
        return str(self.src)  # mergel(self.src)

    def __repr__(self): return self.__str__()


class Instr:
    """docstring Instr ClassName"""

    def __init__(self, src):
        self.src = src
        self.lev = 0

    def __str__(self):
        return self.name() + "[%s:%d]" % (self.src.file.split("/")[-1][:-4], self.src.line) + " " + self.strcontents()

    def __repr__(self):
        return self.__str__()

    def strcontents(self):
        return ''

    def getevalsrc(self, lev):
        return self.spc(lev) + self.name() + "\n"

    def spc(self, lev):
        return "  " * lev

    def eval(self, eo):
        pass

    def hasdrawcontext(self):
        return False

    def groupable(self):
        return True

    def setlev(self, lev):
        self.lev = lev

    def debug(self):
        s = self.getevalsrc(self.lev)
        s = s.split("\n")
        k = len(s)
        print "Evaluating: ", self.name()
        print "\n".join(s[:3]),
        if k > 3:
            print "..."
        else:
            print


class InstrDrawGen(Instr):

    def __init__(self, src, gstspec, tp, params, body):
        Instr.__init__(self, src)
        self.gstspec = gstspec
        self.body = body
        self.params = params
        self.typ = tp

    def hasdrawcontext(self):
        return True

    def name(self):
        return 'draw'

    def eval(self, eo):
        if debuginterpreter: self.debug()
        eo.qtime += 1
        tp = self.gstspec
        eo.newdrawcontext(self.gstspec, 1)
        eo.settype('G')
        if self.body and self.body.name() == 'block':
            for i in self.body.instrlist: i.eval(eo)
        self.draw(eo, tp)
        if tp: eo.endtype()
        eo.deldrawcontext()

    def draw(self, eo, spec):
        from tree import Tree

        def smpldraw(lst, varset, dfun):
            for ind, (t, env) in enumerate(lst):
                vs = getsubd(env, varset, 1)  # take varset
                eo.draw(dfun(t, vs), vs, varset, ind)

        for c in spec:
            if c == "G": smpldraw(eo.getGl(), 'graph', lambda g, e: Tree(g, **e).dotgraph(**e))

    def setlev(self, lev):
        self.lev = lev
        if self.body: self.body.setlev(lev + 1)


class InstrNop(Instr):
    def name(self): return ";#nop"


class InstrSmp(Instr):

    def __init__(self, src, tokens):
        Instr.__init__(self, src)
        self.tokens = tokens

    def strcontents(self): return pptokens(self.tokens)

    def getevalsrc(self, lev): return self.spc(lev) + self.name() + " " + getevalsrc(self.tokens)


class InstrType(InstrSmp):
    def name(self): return "type"

    def eval(self, eo):
        if debuginterpreter: self.debug()
        eo.settype(getevalsrc(self.tokens, '').strip().split(" "))


class InstrEnd(Instr):
    def __init__(self, src, nongrp=False):
        Instr.__init__(self, src)
        self.isgroupable = not nongrp

    def groupable(self): return self.isgroupable

    def name(self): return KTYPEEND

    def eval(self, eo):
        if debuginterpreter: self.debug()
        eo.endtype()


class InstrWithObj(Instr):

    def __init__(self, src, objs):
        Instr.__init__(self, src)
        self.objs = objs

    def strobjs(self): return pptokens(self.objs)

    def getevalsrc(self, lev): return self.spc(lev) + self.name() + " " + getevalsrc(self.objs)


class InstrEval(InstrWithObj):
    def name(self): return "eval"

    def eval(self, eo):
        if debuginterpreter: self.debug()
        e = getevalsrc(self.objs)
        evalpythonic(e, self.src, eo)


class InstrPythAssignment(InstrWithObj):
    def __init__(self, src, label, objs):
        InstrWithObj.__init__(self, src, objs)
        self.label = label

    def strcontents(self):
        return self.label + "=" + self.strobjs()

    def name(self):
        return "PythA"

    def getevalsrc(self, lev):
        return self.spc(lev) + self.label + "=" + getevalsrc(self.objs)

    def eval(self, eo):
        if debuginterpreter: self.debug()
        e = getevalsrc(self.objs)
        # print "EVAL:##%s=%s##%s##"%(self.label,self.objs,e)
        v = None
        if self.objs[0].token in ('&', 'bash', 'gsevol', 'readlines'):
            v = [v[0] for v in evalobj(eo, self.objs)]  # flattened
        else:
            v = evalpythonic(e, self.src, eo)
        cnf = hasattr(self, 'cnf') and self.cnf == 1
        eo.setvariable(self.label, v, self.objs[0], cnf=cnf)
        if self.label == "markscheme": eo.setvariable("marks", genmarks(v), self.src)


class InstrObj(InstrWithObj):

    def __init__(self, src, typ, objs):
        InstrWithObj.__init__(self, src, objs)
        self.typ = typ

    def strcontents(self): return "&" + self.typ + " " + self.strobjs()

    def name(self): return "Objs"

    def getevalsrc(self, lev):
        return self.spc(lev) + "&" + self.typ + " " + getevalsrc(self.objs)

    def eval(self, eo):
        if debuginterpreter: self.debug()
        res = evalobj(eo, self.objs)
        # print >>sys.stderr,"RESULTEVAL",res
        eo.addobjects(self.typ, res, self.src)


class InstrBlock(Instr):
    def __init__(self, src, instrlist):
        Instr.__init__(self, src)
        # self.instrlist=instrlist
        self.group(instrlist)
        if not hasattr(self, 'cnf'): self.cnf = 0

    def group(self, instrlist):
        # group draw & table  
        # print "GROUPING"
        self.instrlist = []
        while instrlist:
            i = instrlist.pop(0)
            self.instrlist.append(i)
            if not i.hasdrawcontext(): continue  # preamble

            if instrlist and instrlist[0].name() == "block":
                i.body = instrlist.pop(0)
                continue

            il = []
            while instrlist and not instrlist[0].hasdrawcontext() and instrlist[0].groupable():
                il.append(instrlist.pop(0))

            i.body = InstrBlock(i.src, il)
        return

    def strcontents(self):

        def _cont(il, lev=0):
            s = ''
            for i in il:
                if i.name() == "block":
                    s += "\n" + "{{" + _cont(i.instrlist, lev + 1) + "}}"
                else:
                    s += "\n" + str(lev) + " " + str(i)
            return s

        return _cont(self.instrlist, 0)

    def name(self):
        return "block"

    def getevalsrc(self, lev):
        if not self.instrlist: return "{}"
        if len(self.instrlist) == 1:
            x = self.instrlist[0].getevalsrc(0)
            if len(x.split("\n")) == 1:
                return self.spc(lev) + "{ " + x.strip() + " }"
        return self.spc(lev) + "{\n" + "\n".join(i.getevalsrc(lev + 1) for i in self.instrlist) + "\n" + self.spc(
            lev) + "}"

    def setlev(self, lev):
        self.lev = lev
        for i in self.instrlist: i.setlev(lev + 1)

    def eval(self, eo):
        if debuginterpreter: self.debug()
        if not self.cnf: eo.newdrawcontext(eo.context.spec, 0)
        for i in self.instrlist:
            i.eval(eo)
        if not self.cnf: eo.deldrawcontext()
