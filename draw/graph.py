# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl


import random
from math import sqrt
from pict import P, NLine, Label, Group, Polygon, Convex, NamedNode, NLabel, BBox, NCurve, NMark
from pict import nextnamednode
import math
from pict import NCircle, MultiMark
from tools import convex_hull


def signnum(x):
    if x < 0: return -1
    if x > 0: return 1
    return 0


def flatval(l):
    res = []
    for i in l:
        if type(i) in (list, tuple):
            res.extend(i)
        else:
            res.append(i)
    return res


class GraphObject:
    def __init__(self, **kwargs):
        self.adddef("linecolor", None)
        self.adddef("linelabel", None)
        self.decor(**kwargs)

    def decor(self, **kwargs):
        for k, v in kwargs.items():
            if v: setattr(self, k, v)

    def adddef(self, k, v):
        if not hasattr(self, k): setattr(self, k, v)


class GNode(GraphObject):
    def __init__(self, **kwargs):
        GraphObject.__init__(self, **kwargs)


class Edge(GraphObject):
    def __init__(self, a, b, **kwargs):
        self.adddef("lw", None)
        self.adddef("linetype", None)
        GraphObject.__init__(self, **kwargs)
        if 'curve' in kwargs:
            self.curve = 1
        else:
            self.curve = 0
        self.a = a
        self.b = b

    def setmmlen(self, minlen, maxlen):
        self.minlen = minlen
        self.maxlen = maxlen


class Graph:
    def __init__(self, gseaddr, edges=None):

        self.V = []
        self.Ed = {}
        self.E = []
        self.MV = []  # metanodes
        self.ME = []  # metaedges
        self.treelike = 0
        self.gseaddr = gseaddr

        if edges:
            for i in edges: self.eset(i[0], i[1])

    def getmm(self):
        return (min(v.pos.x for v in self.V), max(v.pos.x for v in self.V),
                min(v.pos.y for v in self.V), max(v.pos.y for v in self.V))

    def __add__(self, v):
        if v not in self.V:

            if v.getvalues('metagraph'):
                self.MV.append(v)
            else:
                self.V.append(v)

            self.Ed[v] = []
        return self

    def eset(self, a, b, **kwargs):

        self.__add__(a)
        self.__add__(b)
        if 'multigraph' not in kwargs:
            for ed in self.E:
                if ed.a == a and ed.b == b or ed.a == b and ed.b == a:
                    # present
                    return None
        e = Edge(a, b, **kwargs)
        if 'metagraph' in kwargs:
            self.ME.append(e)
        else:
            self.E.append(e)
        self.Ed[a].append(b)
        self.Ed[b].append(a)
        return e

    def addtriangles(self):
        for v in self.V:
            if len(self.Ed[v]) == 1:
                a = GNode()
                b = GNode()
                self.eset(v, a)
                self.eset(v, b)
                self.eset(a, b)

    def _finddist(self, n, p=None):
        cdist = -1
        n.vis = 1
        for c in self.Ed[n]:
            if c == p: continue  # parent
            if c.vis:
                continue
            else:
                cd = self._finddist(c, n)
            if cd > cdist: cdist = cd
        return cdist + 1

    def findcenter(self):
        mindist = len(self.V) + 10
        self.centernodes = []
        for v in self.V: v.vis = 0
        for v in self.V:
            v.mdist = self._finddist(v)
            if v.mdist == mindist:
                self.centernodes.append(v)
            if v.mdist < mindist:
                mindist = v.mdist
                self.centernodes = [v]
        # print len(self.centernodes)

    def findcentertree(self):

        for v in self.V: v._deg = 0
        for e in self.E:
            e.a._deg += 1
            e.b._deg += 1

        edges = list(self.E)
        vertices = list(self.V)
        while len(edges) > 1 and len(vertices) > 1:
            lf = [v for v in vertices if v._deg == 1]
            for l in lf:
                l._deg = 0
                for e in edges:
                    if e.a == l:
                        e.b._deg -= 1
                        edges.remove(e)
                    if e.b == l:
                        e.a._deg -= 1
                        edges.remove(e)
                vertices.remove(l)

        # print "ALG"
        # if len(edges)==1: print "EE",edges[0].a.num,edges[0].b.num
        # if len(vertices)==1: print "XX",vertices[0]

        if len(edges) == 1: self.centernodes = [edges[0].a]
        if len(vertices) == 1: self.centernodes = [vertices[0]]

    def setlevels(self, n, lev, p=None):
        n.clevel = lev
        n.vis = 1
        n.cc = [self.setlevels(c, lev + 1, n) for c in self.Ed[n] if c != p and not c.vis]
        n.cp = p
        # print n,n.cc,n.cp
        return n

    def getleaves(self, n):
        # print "GL",n,n.cc
        if n.cc:
            n.ccset = sum((self.getleaves(c) for c in n.cc), [])
        else:
            n.ccset = [n]
        return n.ccset

    def assignpos(self, typ=1, _first=1, **kwargs):
        # self.linetree()
        self.radius = kwargs['graphradius']
        if typ == 1:
            if _first:
                self.radius = self.radius
                self.centertree(0, 0)
            else:
                # self.improvecenterbytotallen(0,0,2000)
                self.radimprove()

        if typ == 2:
            if _first:
                self.centerprep()
                self.centerdyn(kwargs['parseparator'], kwargs['minradians'])
            else:
                self.improvecenterbytotallen2(self, **kwargs)

        if typ == 4:
            self.radius = self.radius
            self.centerprep()
            self.centertree(0, 1)

        # self.linetree()

    def centerprep(self):
        self.findcenter()
        self.center = self.centernodes[0]
        self.center.rad = 0
        for v in self.V: v.vis = 0
        self.setlevels(self.center, 0)
        self.maxclevel = max(v.clevel for v in self.V)

    def improvecenterbytotallen2(self, comp, full=0, smp=1, cnts=1000, **kwargs):
        print "improving by total len"
        ctot = sum(((e.a.pos - e.b.pos).ln() for e in self.E), 0.0)
        cnt = cnts
        # print "INIT",ctot
        while cnt:
            cv = self.V[random.randint(0, len(self.V) - 1)]
            if not cv.cc: continue
            cnt -= 1

            oldcc = cv.cc[:]
            lcc = len(cv.cc)
            # print lcc
            for i in xrange(lcc): cv.cc.append(cv.cc.pop(random.randint(0, lcc - 1)))

            self.centerdyn(kwargs['parseparator'], kwargs['minradians'])
            # compute total len
            tot = sum(((e.a.pos - e.b.pos).ln() for e in self.E), 0.0)
            # print "CUR",tot,ctot,
            if tot >= ctot:
                cv.cc = oldcc  # no change
            else:
                # print "IMpr!",ctot,"<--",tot,
                ctot = tot

            self.centerdyn(kwargs['parseparator'], kwargs['minradians'], kwargs['startangle'])
            # print sum( ((e.a.pos-e.b.pos).ln() for e in self.E ),0.0)

        for v in self.V:
            v.info = "%d" % v.clevel

    def centerdyn(self, parseparator=3, minradians=math.pi / 4, startangle=0.0):
        # aggregate levels        

        # assign distance from the center                
        levels = [[] for i in xrange(self.maxclevel + 1)]
        levrads = []
        for v in self.V: levels[v.clevel].append(v)
        for l in xrange(self.maxclevel + 2):  levrads.append(0)

        self.leveldist = [0.0]
        for l in xrange(1, self.maxclevel + 1):
            nlev = self.leveldist[l - 1] + 1.0
            if len(levels[l]) * minradians > 2 * math.pi * nlev:
                nlev = len(levels[l]) * minradians / (2 * math.pi)
            self.leveldist.append(nlev)

        # print "LEVELS",self.leveldist
        cnt = 0

        def drawdyn(n, la, cnt):
            cdelta = minradians / self.leveldist[n.clevel] if n.clevel else 0
            # print "CAND0=%d" % rd(la),
            if la < cdelta + levrads[n.clevel]: la = levrads[n.clevel] + cdelta
            if parseparator:
                for i in xrange(n.clevel - parseparator, n.clevel + parseparator + 1):
                    if 0 < i <= self.maxclevel:
                        if la < levrads[i]: la = levrads[i] + cdelta * 0.3

                if parseparator and la < levrads[n.clevel - 1]: la = levrads[n.clevel - 1]
            # print "CAND=%d" % rd(la)
            if n.cc:
                ccdelta = minradians / (self.leveldist[n.clevel] + 1.0)
                cpos = -0.5 * (len(n.cc) - 1) * ccdelta + la
                # print "cpos=%d" % rd(cpos)
                for c in n.cc: cnt = drawdyn(c, cpos, cnt)
                n.rad = sum((c.rad for c in n.cc), 0.0) / (1.0 * len(n.cc))
            else:
                n.rad = la

            levrads[n.clevel] = n.rad
            cnt += 1
            # n.info="%d-%d"%(n.clevel,rd(n.rad))
            # print cnt,n.clevel,rd(n.rad),rd(cdelta),[ rd(f) for f in levrads ],n
            return cnt

        self.center.rad = 0

        # checking levels

        incr = 1
        while incr:
            incr = 0
            drawdyn(self.center, 0.0, 0)
            S = {}
            for v in self.V: S.setdefault(v.clevel, []).append(v)
            for l in sorted(S):
                if l == 0: continue
                rads = [v.rad % (2 * math.pi) for v in S[l]]
                rads.sort()
                df = []
                for i, r in enumerate(rads):
                    if i:
                        df.append(r - rads[i - 1])
                    else:
                        df.append(2 * math.pi - rads[-1] + r)
                    # if min(df)+0.001<minradians/l:
                #    print "!!!",l,minradians/l,df

                # print [ v.rad  for v in S[l] ]
                # minr=min(v.rad for v in S[l])
                # if minr+2*math.pi-maxr<minradians:
                #    incr=1
                #    print "!!!"
                # for v in S[l]: v.clevel+=incr
            # incr=1
            # if incr: print "========================!!!"
            break

        self._rads2pos(0, startangle)

        # for v in self.V:
        #    v.info="%d"%v.clevel

    def _rads2pos(self, full, startangle=0.0):
        for v in self.V:
            if v.cc or not full:
                l = v.clevel
            else:
                l = self.maxclevel
            v.pos.x = math.cos(v.rad + startangle) * self.radius * self.leveldist[l]
            v.pos.y = math.sin(v.rad + startangle) * self.radius * self.leveldist[l]

    def centertree(self, full=0, smp=1):
        print "center tree"

        Lf = self.getleaves(self.center)
        for i, lf in enumerate(Lf):
            lf.rad = (1.0 * i / len(Lf)) * 2 * math.pi

        def smprads(n):
            if n.cc:
                n.rad = sum((smprads(c) for c in n.cc), 0.0) / (1.0 * len(n.cc))
            return n.rad

        if smp:
            smprads(self.center)
        else:
            for v in self.V:
                if v.cc:
                    v.rad = sum((l.rad for l in v.ccset), 0.0) / (1.0 * len(v.ccset))

        self._rads2pos(full)

        for v in self.V:  print v.rad, v.clevel, v.pos.x, v.pos.y, v

    def evalforce(self, first=1, minx=0, miny=0, maxx=100.0, maxy=100.0):

        def eqSet(p1, p2):
            # rint p1.x,p1.y,p2.x,p2.y
            x = -(p1.y - p2.y) / (p1.x - p2.x)
            return P(x, p1.y + p1.x * x)

        def evLin(p1, p2):
            x = (p1.y - p2.y) / (p1.x - p2.x)
            return P(x, p1.y - p1.x * x)

        def evVal(p1, p2, x):
            a = (p1.y - p2.y) / (p1.x - p2.x)
            return P(x, a * (x - p1.x) + p1.y)

        def crossing(a, b, c, d):
            if a.x == b.x and c.x == d.x:
                if a.x <> d.x: return 0
                if max(a.y, b.y) < min(c.y, d.y): return 0
                if max(c.y, d.y) < min(a.y, b.y): return 0
                return 1

            if a.x == b.x:
                cp = evVal(c, d, a.x)
                if cp.y < min(a.y, b.y) or cp.y > max(a.y, b.y): return 0
                return 1
            if c.x == d.x:
                cp = evVal(a, b, c.x)
                if cp.y < min(c.y, d.y) or cp.y > max(c.y, d.y): return 0
                return 1

            try:
                cp = eqSet(evLin(a, b), evLin(c, d))
            except ZeroDivisionError:
                return 0
            if min(a.x, b.x) < cp.x < max(a.x, b.x) and min(c.x, d.x) < cp.x < max(c.x, d.x): return 1
            return 0

        first = first or not hasattr(self, "area")

        if first:
            # print "Insert triangles"
            # self.addtriangles()
            pass

        for v in self.V:
            if not hasattr(v, "pos"):
                v.x = 100 + random.randint(minx, maxx)
                v.y = 100 + random.randint(miny, maxy)
                v.pos = P(0, 0)
            else:
                v.x = v.pos.x
                v.y = v.pos.y
            if not hasattr(v, "fixed"): v.fixed = False

        if first:
            # print "INIT"

            minx, maxx, miny, maxy = self.getmm()
            self.area = (maxx - minx) * (maxy - miny)
            self.k = math.sqrt(self.area / len(self.V))
            self.temp = 60.0
            self.iter = 1
            self.mincross = 1000000
            self.minitercross = 0

        def fa(z):
            return (z ** 2) / self.k

        def fr(z):
            return (self.k ** 2) / z

        cnt = 1

        # self.V[0].fixed=True

        def leafrot(temp=0.25):
            print "Leaf rotation"
            while temp > 0.01:
                for v in self.V:
                    if len(self.Ed[v]) <= 1: continue
                    lst = sorted([(s, math.pi + math.atan2(s.y - v.y, s.x - v.x)) for s in self.Ed[v]],
                                 key=lambda n: n[1])

                    for i, (s, t) in enumerate(lst):
                        s.dpr = lst[i - 1][1] - t
                        s.dpn = lst[(i + 1) % len(lst)][1] - t
                    lst[0][0].dpr = -2 * math.pi - lst[0][1] + lst[-1][1]
                    lst[-1][0].dpn = -lst[0][0].dpr

                    for (s, t) in lst:
                        if len(self.Ed[s]) == 1:
                            sign = 0
                            if abs(s.dpr) > s.dpn: sign = -1.0
                            if abs(s.dpr) < s.dpn: sign = 1.0

                            sx = s.x - v.x
                            sy = s.y - v.y
                            t = temp

                            if sign:
                                s.x = v.x + sx * math.cos(t * sign) - sy * math.sin(t * sign)
                                s.y = v.y + sx * math.sin(t * sign) + sy * math.cos(t * sign)

                temp = temp * 0.98

        while True:
            self.iter += 1

            print "%d. temp=%g micross=%d minitercross=%d" % (self.iter, self.temp, self.mincross, self.minitercross)
            for v in self.V:
                v.dispx = 0.0
                v.dispy = 0.0

                for u in self.V:
                    if v <> u:
                        deltax = v.x - u.x
                        deltay = v.y - u.y
                        lndelta = sqrt(deltax ** 2 + deltay ** 2)
                        if lndelta:
                            v.dispx = v.dispx + (deltax / lndelta) * fr(lndelta)
                            v.dispy = v.dispy + (deltay / lndelta) * fr(lndelta)

            for e in self.E:
                deltax = e.a.x - e.b.x
                deltay = e.a.y - e.b.y
                lndelta = sqrt(deltax ** 2 + deltay ** 2)
                if lndelta > 0:
                    e.a.dispx = e.a.dispx - (deltax / lndelta) * fa(lndelta)
                    e.a.dispy = e.a.dispy - (deltay / lndelta) * fa(lndelta)
                    e.b.dispx = e.b.dispx + (deltax / lndelta) * fa(lndelta)
                    e.b.dispy = e.b.dispy + (deltay / lndelta) * fa(lndelta)

            for v in self.V:
                # print v.disp,
                if not v.fixed:
                    displn = sqrt(v.dispx ** 2 + v.dispy ** 2)
                    v.x = v.x + (v.dispx / displn) * min(displn, self.temp)
                    v.y = v.y + (v.dispy / displn) * min(displn, self.temp)

                    # if self.temp<1.0

            self.temp = self.temp * 0.97

            # check crossings
            def checkcrossings(rnd=0):
                cross = 0
                for i, e in enumerate(self.E):
                    for j in xrange(i + 1, len(self.E)):
                        ep = self.E[j]
                        if e.a == ep.a or e.a == ep.b or e.b == ep.a or e.b == ep.b:       continue
                        if crossing(e.a, e.b, ep.a, ep.b):
                            if rnd:
                                e.a.x += (random.random() - 0.5) * rnd
                                e.a.y += (random.random() - 0.5) * rnd
                                e.b.x += (random.random() - 0.5) * rnd
                                e.b.y += (random.random() - 0.5) * rnd
                                ep.a.x += (random.random() - 0.5) * rnd
                                ep.a.y += (random.random() - 0.5) * rnd
                                ep.b.x += (random.random() - 0.5) * rnd
                                ep.b.y += (random.random() - 0.5) * rnd
                            cross += 1
                return cross

            cross = checkcrossings()
            if cross < self.mincross:
                self.mincross = cross
                self.minitercross = self.iter
                # self.temp=max(0.1,min(60,0.2*self.mincross))
            else:
                if cross > 0 and self.iter - self.minitercross > 100:  # increase temperature
                    print "Temp!"
                    checkcrossings(2.0)
                    self.temp = self.temp + 10
                    self.mincross = 1000000

            print "1-Cross", cross

            if cross == 0:
                leafrot()
                self.shrink()
                cross = checkcrossings()
                print "LR-CROSSINGS", cross
                import sys
                if cross == 0:
                    # sys.stdin.read()
                    break

            print "current crossings", cross

            if cross == 0:
                break

            cnt += 1
            if cnt == 2:  break

        for v in self.V:
            v.pos.x = v.x
            v.pos.y = v.y

        if self.temp < 0.001:
            pass
            # self.shrink()
            # self.temp=self.temp*3

    def shrink(self):  # trees only
        # find leaf node
        # print "SHRINK"
        if not hasattr(self, "start"):
            def visit(n):
                if n.visited: return
                n.visited = True
                n.sc = [v for v in self.Ed[n] if not v.visited]
                for c in n.sc: visit(c)

            self.start = self.V[0]
            for v in self.V: v.visited = False
            visit(self.start)

        for v in self.V:
            v.adjx = 0.0
            v.adjy = 0.0

        destln = 10.0

        def expand(n):
            for c in n.sc:
                vecx = c.x - n.x
                vecy = c.y - n.y
                vecln = sqrt(vecx ** 2 + vecy ** 2)
                if not c.c:
                    dd = 2 * destln
                else:
                    dd = destln
                c.adjx = n.adjx + vecx * (dd / vecln - 1.0)
                c.adjy = n.adjy + vecy * (dd / vecln - 1.0)
                expand(c)
                c.x += c.adjx
                c.y += c.adjy

        self.start.adjx = 0.0
        self.start.adjy = 0.0
        expand(self.start)

    def radialtree2(self, start):
        # reconstruct tree structure
        for e in self.E:
            e.branchlen = e.a.branchlen
        e = list(self.E)

        def findkids(v, edges):
            v.kids = []
            while True:
                k = None
                for e in edges:
                    if e.a == v: k = e.b
                    if e.b == v: k = e.a
                    if k: break
                if k:
                    edges.remove(e)
                    v.kids.append(k)
                    k.edge = e
                else:
                    break
            for k in v.kids: findkids(k, edges)

        def printkids(v):
            if v.leaf():
                s = "%s" % v.smplabel()
            else:
                s = "(" + ",".join(printkids(x) for x in v.kids) + ")"
            if v.edge: return s + ":%g" % v.edge.branchlen
            return s

        def assignradialpos(v, rdelta):
            sc = 40.0
            if len(v.kids) == 1:
                v.kids[0].rad = v.rad
            else:
                if v == start:
                    incr = 1.0 * rdelta / len(v.kids)
                else:
                    incr = 1.0 * rdelta / (len(v.kids) - 1)
                for i, k in enumerate(v.kids):
                    k.rad = v.rad - 0.5 * incr * (len(v.kids) - 1) + 1.0 * incr * i
                    k.pos = P(math.cos(k.rad) * (v.pos.ln() + sc), math.sin(k.rad) * (v.pos.ln() + sc))
                    lv = k.pos - v.pos
                    lv = lv / lv.ln()
                    k.pos = v.pos + lv * k.branchlen * sc

            for k in v.kids:
                assignradialpos(k, rdelta / 6)

        def computerdist(n, rdist):
            if n.leaf():
                n.rdist = rdist
            else:
                n.rdist = max(computerdist(k, rdist + k.edge.branchlen) for k in n.c)
            return n.rdist

        start.edge = None
        findkids(start, e)
        start.pos = P(0, 0)
        start.rad = 0

        # find longest paths
        for c in start.c:
            print computerdist(c, 0)

        assignradialpos(start, 2 * math.pi)

        for v in self.V:
            v.pos = NamedNode(v.pos, gseaddr=v.gseaddr, name="rad%d" % v.num)

    def pict(self, **kwargs):

        df = dict(
            drawtype=1,  ## prev. =3 dla starlike
            graphradius=30,  ##
            minradians=math.pi / 4,
            parseparator=3,
            graphname=None,
            showgraphname=True,
            startangle=0,
            lw=1.0,
            linecolor='black',
            nodelabels=[],
            leaflabels=[],
            forceiter=20
        )
        df.update(kwargs)
        drawtype = df["drawtype"]
        forceiter = df["forceiter"]

        graphname = None
        if df['showgraphname']:
            if hasattr(self, "graphname"):
                graphname = self.graphname
            else:
                graphname = df['graphname']

        p = Group(source=self, gseaddr=self.gseaddr)
        # d=100

        for n in self.V:
            if hasattr(n, 'pos'):
                n.pos.eval(**kwargs)

        if not hasattr(self, "nds"):
            self.nds = {}
            for i, v in enumerate(self.V):
                # print "T",i,v
                if not hasattr(v, 'pos'):
                    if v.getvalues('pos'):

                        v.pos = P(v.getvalues('pos')[0])
                    else:
                        v.pos = NamedNode(P(random.randint(0, 100), random.randint(0, 100)),
                                          name=nextnamednode(i), gseaddr=v.gseaddr)

                if not hasattr(v.pos, 'name'):
                    v.pos = NamedNode(v.pos,
                                      name=nextnamednode(i), gseaddr=v.gseaddr)
                self.nds[v.pos.name] = v.pos
                v.ins = 0

            if drawtype == 2:
                self.assignpos(2, 1, **df)

            if drawtype == 3:
                self.assignpos(2, 1, **df)
                self.assignpos(2, 0, **df)

            if drawtype == 4:
                self.assignpos(2, 1, **df)
                self.assignpos(2, 0, **df)
                self.evalforce()
                for i in xrange(forceiter):
                    self.evalforce(0)

            if drawtype == 5:
                # self.radius=self.radius

                self.findcentertree()
                # for v in self.V: print v.num,v
                # print "CE",self.centernodes[0]
                self.radialtree2(self.centernodes[0])
                # self.evalforce()
                # for i in xrange(50):
                #    self.evalforce(0)

        def drawedge(i, e):
            if not e.linecolor:
                lc = df['linecolor']
            else:
                lc = e.linecolor
            if not e.lw:
                lw = df['lw']
            else:
                lw = e.lw

            el = None

            if not e.linelabel:
                if hasattr(e, 'src'):
                    src = e.src
                    el = src.getedgelabel(kwargs.get('edgelabels', []), kwargs.get('labeldelim', ' '))
            else:
                el = e.linelabel

            if hasattr(e, 'curve') and e.curve:
                if not hasattr(e, 'up'): e.up = 1
                p.add(NCurve(e.a.pos.name, e.b.pos.name,
                             linecolor=lc,
                             linetype=e.linetype,
                             linelabel=el,
                             lw=lw,
                             up=e.up,
                             gseaddr=self.gseaddr,
                             crval=kwargs.get('crval'),
                             crvbe=kwargs.get('crvbe'),

                             ))
            else:
                if 'doublecolors' in df and hasattr(e, df['doublecolors'][0]):
                    d = df['doublecolors']
                    keylab = d[0]  # keylabel
                    d = d[1]  # dict

                    mpos = NamedNode((e.a.pos + e.b.pos) * 0.5, gseaddr=self.gseaddr,
                                     name=e.a.pos.name + e.b.pos.name + ".mid")
                    p.add(mpos)
                    p.add(NLine(e.a.pos.name, mpos.name,
                                gseaddr=self.gseaddr,
                                linecolor=d[getattr(e, keylab)[0]],
                                linetype=e.linetype,
                                linelabel='',
                                lw=lw
                                ))
                    p.add(NLine(mpos.name, e.b.pos.name,
                                gseaddr=self.gseaddr,
                                linecolor=d[getattr(e, keylab)[1]],
                                linetype=e.linetype,
                                linelabel='',
                                lw=lw
                                ))

                    lw = 0

                labelalign = 0.5
                if hasattr(e, 'labelalign'):
                    labelalign = e.labelalign
                elif 'labelalign' in e.a.getdictlenv():
                    labelalign = e.a.getdictlenv()['labelalign']
                else:
                    labelalign = df.get('labelalign', 0.5)

                p.add(NLine(e.a.pos.name, e.b.pos.name,
                            gseaddr=self.gseaddr,
                            linecolor=lc,
                            linetype=e.linetype,
                            linelabel=el,
                            lw=lw,
                            labelalign=labelalign
                            ))

        metagraph = None
        if 'metagraph' in df:

            metagraph = df['metagraph']
            mv = {}
            for v in self.MV:
                v.members = []
                v.metagraph = v.getfirstsinglevalue()
                mv[v.metagraph] = v

                # getcomponents
            for n in self.V:
                if n.getvalues(metagraph):
                    x = n.getvalues(metagraph)[0]
                    if x not in mv:
                        print "Graph warning: metanode %s is undefined" % x
                    else:
                        mv[x].members.append(n)

            cvx = []
            emptycomponents = []
            for d in mv:
                cv = [i.pos for i in mv[d].members]
                if not cv:
                    print "Graph warning: empty metagraph node %s." % d
                    emptycomponents.append(d)
                    continue
                for i in cv: p.add(i)
                c = Convex([i.name for i in cv], gseaddr=self.gseaddr, **mv[d].getdictlenv())
                mv[d].pos = NamedNode(sum([i for i in cv], P(0, 0)) / len(cv), name="CVX%d" % mv[d].nnum,
                                      gseaddr=self.gseaddr)
                p.add(mv[d].pos)
                cvx.append(c)

            # addedges
            # print emptycomponents
            # print self.ME
            for i, e in enumerate(self.ME):
                if hasattr(e, 'metagraph'):
                    if e.a.metagraph in emptycomponents or e.b.metagraph in emptycomponents:
                        print "Warning: cannot draw meta-edge incident to an empty metanode. (%s,%s)." % (
                            e.a.metagraph, e.b.metagraph)
                    else:
                        drawedge(i + len(self.E), e)

            for i in cvx: p.add(i)

        # self.evalforce()
        def addlflabel(a, b, label):
            if label:
                if drawtype == 1:
                    side = "Tc"
                else:
                    at = math.atan2(b.y - a.y, b.x - a.x)

                    at = at / math.pi
                    if -0.75 < at <= -0.25:
                        side = "Bc"
                    elif -0.25 < at < 0.25:
                        side = "rxm"
                    elif 0.25 < at < 0.75:
                        side = "Tc"
                    else:
                        side = "lxm"
                p.add(NLabel(a, label, side=side, gseaddr=n.gseaddr))
                return 1

        for i, e in enumerate(self.E):
            def addlb(a, b, p):
                vi = ""

                if hasattr(a, "leaflabel"):
                    return addlflabel(a.pos, b.pos, a.leaflabel + vi)
                    # print "addlb",a,a.pos,b.pos
                return addlflabel(a.pos, b.pos, vi)

            e.a.ins = addlb(e.a, e.b, 1) or e.a.ins
            e.b.ins = addlb(e.b, e.a, 0) or e.b.ins

        for i, v in enumerate(self.V):
            if not v.ins: p.add(v.pos)
            side = "lxb"
            rot = 0
            # print v,v.leaf()
            if self.treelike:
                n = v
                if n.leaf() and kwargs.get('leaflabels', True) and not kwargs.get('leafline', 0.0):
                    # leafline automatic      

                    sl = n.nodelabel(**kwargs)
                    # print "AA%sAA"%n.nodelabel(**kwargs)
                    # print "HERE",n.pos
                    if not sl: sl = ' '  # @TODO: to avoid bug with empty strings
                    # p.add(NLabel(n.pos,sl,side=side,rotate=rot))
                elif n.leaf() and kwargs.get('leaflabels', True):
                    # leafline separated
                    p.add(n.pos)
                    # print "DD",n.nodelabel(**kwargs)
                    if not hasattr(n, 'lfpos'):
                        self._setleafline(n, kwargs.get('leafline'))
                    p.add(NLabel(n.lfpos, n.nodelabel(**kwargs), side=side, rotate=rot))
                elif not n.leaf() and kwargs.get('nodelabels', []):
                    p.add(NLabel(n.pos, n.nodelabel(**kwargs), side=side, rotate=rot))
                else:
                    p.add(n.pos)

            else:

                if v.nodelabel(**kwargs):
                    if kwargs.get('nodelabels', []):
                        p.add(NLabel(v.pos, v.nodelabel(**kwargs), side=side, rotate=rot, gseaddr=v.gseaddr))
                    else:
                        p.add(NLabel(v.pos, '', side=side, rotate=rot, gseaddr=v.gseaddr))

                sh = v.getvalues("show")
                if sh: p.add(Label(v.pos, sh[0], gseaddr=v.gseaddr))

            if hasattr(v, "mark"):
                if type(v) == float:  # old style
                    p.add(NCircle(v.pos.name, radius=0.5, color=kwargs.get("markcolor", "green"), gseaddr=v.gseaddr))
                else:
                    p.add(NMark(v.pos.name, v.mark, gseaddr=v.gseaddr, **v.getdictlenv()))

            con = v.getvalues("connect")
            if con:
                h = kwargs["edgelabelsconnect"]
                z = v.getedgelabel(h, kwargs.get('labeldelim', ' '))
                for k in self.V:
                    if k.hasvalue(con[0]):

                        # def _se(n):
                        #     return set([e for e in self.E if e.a == n or e.b == n])

                        def _se1(n):
                            return set([e.a for e in self.E if e.b == n])

                        def _se2(n):
                            return set([e.b for e in self.E if e.a == n])

                        x = _se1(v).union(_se2(v)).intersection(_se1(k).union(_se2(k)))

                        fc = v.getvalues("fillcolor")
                        if not fc:
                            fc = kwargs.get('fillcolor', 'lightgray')
                        else:
                            fc = fc[0]

                        if len(x) == 1:
                            for w in x:
                                # TODO other colors
                                p.add(Polygon([v.pos, k.pos, w.pos], fillcolor=fc, gseaddr=k.gseaddr))

                        p.add(NLine(v.pos.name, k.pos.name, gseaddr=self.gseaddr, **v.prop.get(z)))

        if 'decoration' in df:

            decors = df['decoration']
            for v in self.V:

                d = df.copy()
                d.update(v.getdictlenv())
                d['gseaddr'] = self.gseaddr
                for f in decors:
                    try:
                        dec = f(v.pos.name, d)
                        p.add(dec)
                    except Exception as e:
                        print "Node decoration warning", v, e

        for i, e in enumerate(self.E):
            if not metagraph or not hasattr(e, metagraph): drawedge(i, e)

        for v in self.V:
            if v.getvalues("mark"):
                mark = v.getvalues("mark")
                if type(mark[0]) == float:
                    p.add(NCircle(v.pos.name,
                                  gseaddr=self.gseaddr,
                                  radius=v.getvalues("mark")[0],
                                  color=kwargs.get("markcolor", "green")))


                else:
                    mark = flatval(v.getvalues("mark"))
                    if mark: p.add(MultiMark(v.pos.name, mark, gseaddr=self.gseaddr, **v.getdictlenv()))

        if graphname:
            bb = BBox()
            for v in self.V: bb.addp(v.pos)
            p.add(NLabel(NamedNode((bb.a.x, bb.b.y), gseaddr=self.gseaddr, name="treepos%d" % self.V[0].nnum),
                         graphname, gseaddr=self.gseaddr, side=kwargs.get('graphnamepos', "lxb")))

        return p

    def save(self):
        s = "%d %d\n" % (len(self.V), len(self.E))
        for i, v in enumerate(self.V):
            v._grnum = i
            s += "%d " % v._grnum
            if hasattr(v, "pos"):
                s += "%f %f" % (v.pos.x, v.pos.y)
            s += "\n"
        return s + "\n".join("%d %d" % (e.a._grnum, e.b._grnum) for e in self.E)
