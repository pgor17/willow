# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl


import re

from math import cos, sin, pi
from traceback import print_stack
from symbols import symbols, symboltokens

drawpict = dict(
    polfillcolor='lightgray',
    pollw=0.1,
    polfillstyle='solid',
    leafline=(0, -0.25),
    rootpos=(0, 0.3),
    reversible=True,
    linecolor='black',
    linetype="c-c",
    lw=1,
    linestyle="solid",
    margin=(1, 1),
)

# TODO: expand
colors = dict(
    brown=(65, .16, .16),
    lightblue=(.68, .85, .9),
    palegreen=(.2, .98, .2),
    pink=(1, .75, .8),
    wheat=(.96, .87, .7),
    verylightgray=(.9, .9, .9),
    lightgray=(.7, .7, .7),
    red=(1, 0, 0),
    green=(0, 1, 0),
    blue=(0, 0, 1),
    cyan=(0, 1, 1),
    magenta=(1, 0, 1),
    yellow=(1, 1, 0),
    black=(0, 0, 0),
    white=(1, 1, 1),
    gray=(.5, .5, .5))


def getcolor(color):
    if color in colors: return colors[color]
    if type(color) == tuple: return color
    if type(color) == str: return eval(color)
    return color


def smartints(v):
    if int(v) == v: return "%d.0" % v
    return "%f" % v


def _tostr(s, mainlevel=1):
    def _tostrx(s):
        if type(s) == str: return "\'" + s + "\'" if not re.search("^[0-9a-zA-Z]+$", s) else s
        if type(s) == tuple: return "(" + ",".join(map(_tostrx, s)) + ")"
        return s.__repr__()

    if type(s) == str: return "\'" + s + "\'" if not re.search("^[0-9a-zA-Z]+$", s) else s
    if type(s) == tuple:
        return s[0] + "=" + _tostrx(s[1:] if len(s) > 2 else s[1])

    return s.__repr__()


class P:
    def __init__(self, *args, **kwargs):
        if not args:
            self.x = 0.0
            self.y = 0.0
        elif len(args) == 2:
            self.x = args[0]
            self.y = args[1]
        elif type(args[0]) == tuple:
            self.x = args[0][0]
            self.y = args[0][1]
        elif len(args) == 1 and type(args[0]) in (float, int):
            self.y = self.x = args[0]
        else:
            self.x = args[0].x
            self.y = args[0].y
        if type(self.x) == str:
            self.xvar = self.x
            self.x = 0
        else:
            self.xvar = 0
        if type(self.y) == str:
            self.yvar = self.y
            self.y = 0
        else:
            self.yvar = 0
        self.post = kwargs.get('post', None)
        for i in kwargs: setattr(self, i, kwargs[i])
        # print self.post

    def metanode(self):
        return 0

    def rotations(self):
        return 0.0

    def rotatedeg(self, phi):
        phi = pi * phi / 180.0
        self.x, self.y = self.x * cos(phi) - self.y * sin(phi), self.x * sin(phi) + self.y * cos(phi)
        return self

    def eval(self, **kwargs):
        if self.xvar:
            self.x = kwargs[self.xvar]
        if self.yvar:
            # print "!",self.yvar
            self.y = kwargs[self.yvar]

    def __str__(self):
        return "(%s,%s)" % (smartints(self.x), smartints(self.y))

    def __repr__(self):
        return self.__str__()

    def __add__(self, other):
        if type(other) == type(self): return P(self.x + other.x, self.y + other.y)
        if type(other) == tuple and len(other) == 2: return P(self.x + other[0], self.y + other[1])
        return P(self.x + other, self.y + other)

    def __sub__(self, other):
        if type(other) == type(self): return P(self.x - other.x, self.y - other.y)
        if type(other) == tuple and len(other) == 2: return P(self.x - other[0], self.y - other[1])
        return P(self.x - other, self.y - other)

    def __mul__(self, other):
        if type(other) == type(self): return P(self.x * other.x, self.y * other.y)
        return P(self.x * other, self.y * other)

    def __div__(self, other):
        if type(other) == type(self): return P(self.x / other.x, self.y / other.y)
        if type(other) == tuple and len(other) == 2: return P(self.x / other[0], self.y / other[1])
        return P(self.x / other, self.y / other)

    def __neg__(self):
        return P(-self.x, -self.y)

    def ln(self):
        import math
        return math.sqrt(self.x ** 2 + self.y ** 2)

    def evalpost(self, q):
        if self.post: return self.post(q)
        return q

    def applyvect(self, p):
        return self.apply(p)


class D:
    def __init__(self, a, b):
        self.a = P(a)
        self.b = P(b)

    def __str__(self):
        return "%s%s" % (self.a, self.b)


LTTEXT = 1
LTSYMBOL = 6
LTMATH = 10
LTSYMBOLMATH = 12

# Fixed fonts to be used in symbols.dat
LTSYMBOLFIXED = 13

# style
STYLENORMAL = 1
STYLESUBSCRIPT = 2
STYLESUBSCRIPT2 = 4
STYLESUPSCRIPT = 8

FONTCODES = "23456789"  # todo sth better


def mathtokenizer(st, extsyntax):
    for t, style, s, tp, color, backgroundcolor, fontsize in mathtokenizernonsp(st, extsyntax):
        fontsize = float(fontsize)
        if type(s) == str:
            s = s.split(' ')
        else:
            s = [s]

        yield t, style, s[0], tp, color, backgroundcolor, fontsize
        s.pop(0)
        while s:
            x = s.pop(0)
            yield t, style, ' ', tp, color, backgroundcolor, fontsize
            if x: yield t, style, x, tp, color, backgroundcolor, fontsize


def mathtokenizernonsp(st, extsyntax):
    color = "black"
    bcolor = "white"
    fontsize = 0
    if not extsyntax:
        tb = st.split("\n")
        if tb[0]:
            yield (LTTEXT, STYLENORMAL, tb[0], 0, color, bcolor, fontsize)

        for i in xrange(1, len(tb)):
            yield (LTTEXT, STYLENORMAL, "\n", 0, color, bcolor, fontsize)
            if tb[i]: yield (LTTEXT, STYLENORMAL, tb[i], 0, color, bcolor, fontsize)


    else:
        style = STYLENORMAL
        stn = LTTEXT
        fontsize = 0

        cur = ""
        # print "<<"+st+">>"
        block = 0
        while st:
            if len(cur) == 1 and not style & STYLENORMAL and not block:
                yield (stn, style, cur, 0, color, bcolor, fontsize)
                cur = ""
                style = STYLENORMAL
                continue

            c = st[0]
            st = st[1:]
            if c == "@" or c == '\\' and st:
                if not st:
                    nc = c  # repeat last
                else:
                    nc = st[0]
                st = st[1:]
                if nc in "'$@|\\{}":
                    cur += nc
                else:
                    if cur: yield (stn, style, cur, 0, color, bcolor, fontsize)
                    cur = ""

                    if nc in FONTCODES:  # font codes
                        stn = int(nc)
                        continue

                    commands = ['color', 'bcolor', 'fontsize']
                    fullc = nc + st
                    command = None
                    for d in commands:
                        if fullc[0:len(d)] == d:
                            command = d
                            # print "OK",st
                            break

                    if command:
                        if st[len(command) - 1] == '{' and "}" in st[len(command):]:
                            st = st[len(d):]
                            args, st = st.split("}", 1)
                            if command == "color":
                                color = args
                            elif command == "fontsize":
                                fontsize = args
                            nc = ''
                            continue

                    # print " #%s#"%st,nc

                    if st and st[0].isdigit():
                        num = nc

                        while st and st[0].isdigit():
                            num += st[0]
                            st = st[1:]
                        cur = ""
                        if nc == '0':
                            yield (
                            LTSYMBOLMATH if stn == LTMATH else LTSYMBOL, style, int(num), 1, color, bcolor, fontsize)
                        else:
                            yield (STYLENORMAL, style, int(num), 1, color, bcolor, fontsize)
                        continue

                    # print " #%s#"%st,nc
                    fnd = 0
                    if nc in symboltokens:
                        for rest, code, font in symboltokens[nc]:

                            if font:
                                font = int(font[1:])
                            elif stn == LTMATH:
                                font = LTSYMBOLMATH
                            else:
                                font = LTSYMBOL

                            # print font,code

                            if rest == st[:len(rest)]:
                                # print font,code
                                yield (font, style, code, 1, color, bcolor, fontsize)
                                st = st[len(rest):]
                                fnd = 1
                                break

                    if not fnd:
                        if nc not in FONTCODES:
                            print "Warning unknown symbol <%s> found in %s" % (nc, nc + st)
                        # yield (LTSYMBOL,style,nc,0,color,bcolor)
                    if style != STYLENORMAL and not block:
                        style = STYLENORMAL
                    cur = ""
                continue

            if c == "$":
                if stn == LTTEXT:
                    if cur: yield (LTTEXT, style, cur, 0, color, bcolor, fontsize)
                    stn = LTMATH
                    style = STYLENORMAL
                else:
                    if cur: yield (LTMATH, style, cur, 0, color, bcolor, fontsize)
                    stn = LTTEXT
                    style = STYLENORMAL
                cur = ""
                continue

            if c == "^":
                if cur: yield (stn, style, cur, 0, color, bcolor, fontsize)
                cur = ""
                style = STYLESUPSCRIPT
                continue

            if c == "_" and st and (st[0] == "_"):
                st = st[1:]
                if cur: yield (stn, style, cur, 0, color, bcolor, fontsize)
                cur = ""
                style = STYLESUBSCRIPT2
                continue

            if c == "_":
                if cur: yield (stn, style, cur, 0, color, bcolor, fontsize)
                cur = ""
                style = STYLESUBSCRIPT
                continue

            if c == "|":
                if cur: yield (stn, style, cur, 0, color, bcolor, fontsize)
                cur = ""
                yield (stn, style, '\n', 0, color, bcolor, fontsize)
                continue

            if c == "{":
                block = 1
                continue

            if c == "}":
                block = 0
                if cur: yield (stn, style, cur, 0, color, bcolor, fontsize)
                style = STYLENORMAL
                cur = ""
                continue

            cur = cur + c

        if cur: yield (stn, style, cur, 0, color, bcolor, fontsize)


def totup(v):
    if type(v) != tuple: return v, v
    return v


def totup0(v):
    if v and type(v) != tuple:
        return v, v
    return v


def quotes(s):
    if '"' in s: return "'" + s + "'"
    if "'" in s: return "\"" + s + "\""
    return s  # @todo: better processing of " '


def sstr(s):
    s = s.replace("\n", "\\n")
    if len(s) < 80: return s
    return s[:77] + "..."


import traceback
import sys


class GseError(Exception):
    def __init__(self, msg, token=None, token2=None):
        self.msg = msg
        self.token = token
        self.token2 = token2

    def __str__(self):
        s = ""
        if self.token:
            s = self.token.reportsrc() + "\n" + sstr(self.token.insrc()) + "\n"
        if self.token2:
            s += self.token2.reportsrcsmp("Parse error")

        return s + repr(self.msg)
