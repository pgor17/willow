# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl


from pict import marknames
from htlib import colors


def genmark(env, **kwargs):
    if 'type' in kwargs and type(kwargs['type']) == int:
        kwargs['type'] = env['marks'][kwargs['type'] % len(env['marks'])]['marktype']
    return dict(("mark" + k, v) for k, v in kwargs.items())


def _addmark(env, **kwargs):
    env['marks'].append(genmark(env, **kwargs))
    return len(env['marks']) - 1


def _setmark(env, i, **kwargs):
    l = genmark(env, **kwargs)
    if i < 0:
        env['dltmarks'][i].update(l)
    else:
        env['marks'][i].update(l)
    return i


def genmarks(colorscheme="rgb myc BWG"):
    if colorscheme == "bw": colorscheme = "BWG"
    if colorscheme == "color": colorscheme = "rgbmyc"

    dcol = dict(G="gray", r="red", g="green", b="blue",
                y="yellow", m="magenta", c="cyan", B="black", W="white")

    # add remaining 
    for i in colors:
        if i in dcol.values(): continue
        for c in i:
            if c not in dcol:
                dcol[c] = i
                break

    if colorscheme == '*': colorscheme = " ".join(dcol.keys())

    from itertools import product
    marks = []
    for c in colorscheme.split():
        for num in xrange(len(c) * len(marknames)):
            ccol = num % len(c)
            cmark = num % len(marknames)
            marks.append(dict(marktype=marknames[cmark], markfillcolor=dcol[c[ccol]], dot=0))
    return marks
