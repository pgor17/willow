# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl


from htlib import GseError

import sys
import os
import string
from copy import copy, deepcopy
from random import randint
from itertools import product

from tools import ppsmpdict, getsubd

from pict import marknames
from htlib import colors

import sys

from optparse import OptionParser

from frontenddefs import *

from ast import literal_eval

from marks import marknames
from htlib import colors


def readfs(s, typestr, cnf=0, **kwargs):
    if not s: return []

    fi = 1
    r = []
    if s == "-":
        sr = sys.stdin.read()
        file = 'stdin' + "/" + kwargs['opt']
        if sr.strip():
            r += [tokSRC(file, sr, cnf)]
    elif s[-4:] == ".gse" or os.path.exists(s):
        if os.path.exists(s):
            file = s
            if len(s) > 4 and s[-4] == ".":
                fn = s[:-4]
            else:
                fn = s
            r += [tokSRC(file + "-" + "preamble", '\noutputfile="%s";' % fn, cnf),
                  tokSRC(s, open(s).read(), cnf)]  # file
        else:
            raise GseError("Gse file not found %s" % s)
    elif s.strip():
        file = kwargs['opt']
        r += [tokSRC(file, s, cnf)]

    if r and typestr:
        r = [tokSRC(file + "-" + "type-preamble", '\n%s %s\n' % (KTYPE, typestr), cnf)] + r + \
            [tokSRC(file + "-" + "type-preamble", '\n%s\n' % KTYPEENDX, cnf)]

    return r


def collectinput(parser, argv):
    options, args = parser.parse_args(argv)

    r = []
    if options.config:
        cnf = readfs(options.config, '', 1, opt='C_OPTION')
    else:
        cnf = []

    s = cnf

    if args:
        for i, a in enumerate(args):
            r += readfs(a, '', opt="ARGV%d" % i)
    else:
        r += readfs("-", '', opt="STDIN")

    if r: s += [tokSRC("default-type-preamble", '\n%s %s\n' % (KTYPE, "G"), 0)] + r + \
               [tokSRC("default-type-preamble", '\n%s\n' % KTYPEENDX, 0)]

    return s, options


def toktokenize(t):
    tokens = []

    while t:
        token = t.pop(0)

        if token.typ == GSEEOF:
            tokens.append(token)
            break

        if token.typ == GSESRC:
            src = tokenize(token.token, token.file)
            if not src: continue
            fst = src[0]
            if token.cnf:
                tokens.append(
                    GseToken(GSECNF, 'config', (fst.p, fst.q), line=fst.line, file=fst.file, column=fst.column))
                tokens.append(tokBlockOpen(fst))

            tokens.extend(src)

            if token.cnf:
                tokens.append(tokBlockClose(fst))

            tokens.append(GseToken(GSESEP, "\n", line=src[-1].line, column=src[-1].column, file=fst.file))

    return tokens


# ppptokens

def getsrc(toklist):
    s = " ".join(t.insrc() for t in toklist)
    return s.replace("\n", "\\n")
