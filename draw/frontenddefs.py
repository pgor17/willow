# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl


from re import match, sub
from tools import ppsmpdict
from htlib import GseError

from copy import copy
from os import environ

GSEOTHER = 900
GSEGROUP = 999
GSESTRING = 902
GSENUMBER = 903
GSENAME = 904
GSEEOF = 905
GSEKTYPE = 906
GSEKTYPEEND = 907
GSESRC = 908
GSESEP = 909
GSECNF = 910
GSEINSTR = 911  # generic GSE instruction, no src
GSESPACE = 912
GSEOPT = 913

KTYPE = "type"
KTYPEEND = "endtype"
KTYPEENDX = "endtype2"

OBJTYPEANY = '.'

debugeval = 0
debugpythonic = 0
debugtreeparse = 0
debugparse = 0
debuggseoutput = 0
debugbash = 0
debugforloop = 0

opengroup = "[({"
closegroup = dict(zip(opengroup, "])}"))

whitespaces = "\n\t "


def nextt(tokens, src, skip):
    t = topt(tokens, src, skip)
    # print "   <NEXTT>",t.token,t.typ,tokens[0]
    if t: tokens.pop(0)
    return t


def tname(typ):
    if typ == GSEOTHER:
        return "oth "
    elif typ == GSEGROUP:
        return "grp "
    elif typ == GSESTRING:
        return "str "
    elif typ == GSENUMBER:
        return "num "
    elif typ == GSENAME:
        return "nam "
    elif typ == GSEEOF:
        return "eof "
    elif typ == GSEKTYPE:
        return "typ "
    elif typ == GSEKTYPEEND:
        return "end "
    elif typ == GSESRC:
        return "src "
    elif typ == GSEINSTR:
        return "ins "
    elif typ == GSECNF:
        return "cnf "
    elif typ == GSESEP:
        return "--- "
    elif typ == GSESPACE:
        return "SPC "

    return "%03d%s" % (ord(typ), str(typ))


def gsecopy(tokens):
    return [t.gsecopy() for t in tokens]


def topt(tokens, src, skip, eofOK=0):
    while True:
        if not tokens:
            if not src: return None
            if eofOK: return None

            raise GseError("Unexpected end of input when parsing <%s:%s>" % (str(src.typ), src.token), src)

            # print "   ",len(skip),"TOPT<%s>"%tokens[0].token.replace("\n","\\n")
        if skip and tokens[0].token:
            if (tokens[0].typ == GSESPACE or tokens[0].typ == GSESEP) and tokens[0].token in skip:
                tokens.pop(0)
                continue
        # print "ret",tokens[0].typ,tokens[0].token
        return tokens[0]


class GseToken:

    def __init__(self, typ, token, q=0, src='', **kwargs):
        self.typ = typ
        if type(q) == tuple:
            self.p, self.q = q
        else:
            self.q = q
        self.token = token
        self.src = src  # whole initial string
        self.line = -1
        self.column = -1
        self.file = ''
        for k in kwargs: setattr(self, k, kwargs[k])
        if 'line' in kwargs: del kwargs['line']
        if 'column' in kwargs: del kwargs['column']
        self.kwargs = kwargs

    def insrc(self):
        if not self.src: return self.token
        return self.src[self.p:self.q]

    def reportsrc(self):
        return "  File \"%s\", line %d, %d" % (self.file, self.line, self.column)

    def reportsrcsmp(self, info):
        s = "  Line %d, %d in processed input:\n" % (self.line, self.column)
        r = self.src.split("\n")
        for i, l in enumerate(r):
            if i == self.line - 1:
                s += l[:self.column] + "\n" + "-" * self.column + "^" + info + "\n" + " " * self.column + l[
                                                                                                          self.column:] + "\n"
            else:
                s += l + "\n"
        return s

    # def reporterr(self):
    #    report "Err: \"%s\" line %d:%d"%(self.file,self.line,self.column)

    def __repr__(self):
        return self.__str__()

    def __str__(self):
        if self.typ == GSEGROUP: return self.getevalsrc()
        return self.token

    def getevalsrc(self):
        if self.typ == GSESEP: return self.token
        if self.typ == GSEGROUP:
            s, e = '', ''
            if self.token == "{": s, e = "{}"
            return s + " ".join(t.getevalsrc() for t in self.grp) + e
        return self.token

    def gsecopy(self):
        cp = copy(self)
        if self.typ == GSEGROUP:
            cp.grp = [i.gsecopy() for i in cp.grp]
        return cp


tokEOF = GseToken(GSEEOF, '')


def tokSRC(file, src, cnf): return GseToken(GSESRC, src, file=file, cnf=cnf)


def tokBlockOpen(token): return GseToken(GSEOTHER, '{', (token.p, token.q), line=token.line, file=token.file,
                                         column=token.column)


def tokBlockClose(token): return GseToken(GSEOTHER, '}', (token.p, token.q), line=token.line, file=token.file,
                                          column=token.column)


def tokenize(s, file=''):
    tokens = []
    pos = 0
    cur = ""
    ln = 1
    col = 0
    lastpos = 0
    inp = 0

    def addc(lastpos, pos, ln, col):
        if cur:
            tokens.append(GseToken(GSENAME, cur, (lastpos, pos), line=ln, column=col))
        return ""

    while pos < len(s):

        # print "YY",lastpos,pos,s[pos]

        c = s[pos]

        if c == "#":
            # eat comment
            while pos < len(s) and s[pos] != "\n": pos += 1


        elif c in ";\n":
            if c == "\n":
                ln += 1
                col = 0
            if c == "\n" and cur and cur[-1] == '\\':
                cur = cur[:-1]
                cur = addc(lastpos, pos - 1, ln, col)
                # cut last+new token)
                pos += 1
                lastpos = pos
                continue

            cur = addc(lastpos, pos, ln, col)
            pos += 1
            tokens.append(GseToken(GSESEP, c, (pos - 1, pos), line=ln, column=col))
            lastpos = pos

        elif c.isspace():
            cur = addc(lastpos, pos, ln, col)
            pos += 1
            lastpos = pos
            col += 1
            tokens.append(GseToken(GSESPACE, c, (lastpos, pos), line=ln, column=col))

        elif c in "{}[](),^+-~:=" or (c in "*&%!"):
            cur = addc(lastpos, pos, ln, col)
            lastpos = pos
            pos += 1
            col += 1
            tokens.append(GseToken(c, c, (lastpos, pos), line=ln, column=col))
            lastpos = pos

        elif c in '"\'':
            cur = addc(lastpos, pos, ln, col)
            lastpos = pos
            res = c
            pos += 1
            initcol = col
            col += 1

            while pos < len(s):
                if s[pos] == "\\" and pos + 1 < len(s) and s[pos + 1] == c:
                    res += "\\" + s[pos:pos + 2]
                    col += 2
                    pos += 2
                    continue
                res += s[pos]
                pos += 1
                col += 1
                if s[pos - 1] == c: break
                if s[pos - 1] == "\n": col = 0

                # print "#%s#"%res,len(res)
            # print "#%s#"%literal_eval(res)

            if res[-1] != res[0]:
                raise GseError(
                    'Unterminated string',
                    GseToken(GSESTRING, res, (lastpos, pos), file=file, line=ln, column=initcol))

            # print literal_eval(res)
            tokens.append(GseToken(GSESTRING, res, (lastpos, pos), line=ln, column=col))

            ln += res.count("\n")
            lastpos = pos

            # ???
        elif c == "\\" and s[pos + 1] == ' ':
            cur += " "
            pos += 2
            col += 2

            # @TODO: better
        # num = "0123456789."
        elif not cur and c in "+-0123456789.":
            m = match("[-+]?(\d+(\.\d*)?|\.\d+)([eE][-+]?\d+)?", s[pos:])
            if m:
                # cur = addc(lastpos,pos,ln, col)
                lastpos = pos
                m = m.group(0)
                pos += len(m)
                col += len(m)
                tokens.append(GseToken(GSENUMBER, m, (lastpos, pos), line=ln, column=col))

            else:
                cur = cur + c
                pos += 1
                col += 1
        else:
            cur = cur + c
            pos += 1
            col += 1

    addc(lastpos, pos, ln, col)
    lastpos = pos

    for t in tokens: t.src = s

    res = []
    while tokens:
        token = tokens.pop(0)
        token.file = file
        if token.typ == GSENAME and token.token == "input":
            fn = nextt(tokens, token, whitespaces)
            if not fn:
                raise GseError("Expected file name after input directive.", token)
            if fn.typ not in (GSENAME, GSESTRING):
                raise GseError("Expected file name after input directive. Found <%s>" % fn.token, token)
                # fn=tokens.pop(0)
            res.append(tokBlockOpen(token))
            try:
                filename = fn.token
                if filename[0] == filename[-1] and filename[0] in '"\'': filename = filename[1:-1]
                if filename[0] == '$':
                    filename = environ[filename[1:]]
                f = open(filename, "r")
                s = f.read()
                f.close()

            except KeyError:
                raise GseError("Variable %s is undefined" % filename, token)
            except Exception:
                raise GseError("Cannot open input file %s" % filename, token)
            res.extend(tokenize(s, fn.token))
            res.append(tokBlockClose(token))
        else:
            res.append(token)

    return res


def tokenizegroups(tokens):
    def trgroup(res, term):
        # print "Group%s"%term

        while tokens:
            token = tokens.pop(0)

            # print "  in[%s]"%term, token.typ,token.src.replace("\n","\\n")
            if token.typ == GSEEOF:
                if term:  # end of input inside a group
                    raise GseError("End of file while scanning for %s" % term, res[0])
                res.append(token)
                return res  # end OK
            if token.token in opengroup:
                grp = trgroup([token], closegroup[token.token])

                grptoken = GseToken(GSEGROUP, token.token, grp=grp, file=token.file, line=token.line,
                                    column=token.column)
                res.append(grptoken)
                if token.src == grp[-1].src:
                    grptoken.p = token.p
                    grptoken.q = grp[-1].q
                    grptoken.file = token.file
                    grptoken.src = token.src
                if token.token == '{': grptoken.grp = grp[1:-1]

            else:
                res.append(token)
                if token.token == term:
                    # print "Group-compl%s"%term
                    return res
        return res  # not term?

    return trgroup([], '')


def pptokens(t, srcname=0, lim=1000):
    def _pst(x):
        x = x.replace("\n", "\\n")
        if len(x) < 60: return x
        return x[:27] + "..." + x[-30:]

    def _pptokens(t, lim, lev):
        # print "Lev",lim,lev
        s = ''
        if lev > lim: return s
        last = '\n'
        index = 0
        while index < len(t):
            cnt = 0
            token = t[index]
            spindex = index
            while token.typ == GSESPACE:
                index += 1
                if index == len(t):  break
                token = t[index]
                cnt += 1
            index += 1
            _slev = "     " * lev
            slev = _slev + "%3d" % spindex + "  "
            if cnt: s += slev + "- -  x%d" % cnt + "\n"
            slev = _slev + "%3d" % index + "  "
            s += slev + tname(token.typ)
            s += " %s:%d:%d" % (token.file.split("/")[-1], token.line, token.column)
            # print token.reportsrc()
            if token.typ == GSEGROUP:
                s += "!!SR!!%s!!" % token.insrc().replace("\n", "\\n") + "\n"  # +token.p+token.q
                s += _pptokens(token.grp, lim, lev + 1)
            else:
                # print s,type(s)
                s += " %s" % str(_pst(token.token))
                if token.token != token.insrc():
                    s += "!!SR!!%s!!" % token.insrc().replace("\n", "\\n")
                if token.kwargs: s += "\n" + slev + "   @" + ppsmpdict(token.kwargs, '')
                s += last
        return s

    if srcname:
        return "%s-START len=%d" % (srcname, len(t)) + "\n" + _pptokens(t, lim, 1) + "%s-END" % srcname
    return _pptokens(t, lim, 1)
