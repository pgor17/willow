# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl

from htlib import P, D
import math
from itertools import product

marknames = ["circle", "square", "triangle", "trianglerev", "diamond",
             "hexagon", "cross", "star", "star8", "star4", "crossrot", "star3",
             "star4rot", "star3rev", "pentagon",
             "hourglass", "hourglass2", "1square", "2square", "3square",
             "4square",
             "pacman1", "pacman2", "pacman3", "pacman4",
             "pacman5", "pacman6", "pacman7", "pacman8",
             "V1", "V2", "V3", "V4",
             "X1", "X2",
             "xcross", "xcrossrot",
             "Xcross", "XcrossE",
             "diamond2", "ellipse"]

# Current size: 37
# Next sizes: 41, 43, 47, 53, 59, 61, 67, 71, 73, 79

savedpicturefiles = []
savedpictureindex = {}


def getmark(m):
    if m in marknames: return marknames.index(m)
    return int(m)


_namednodenum = 0


def nextnamednode(i):
    global _namednodenum
    _namednodenum += 1
    return "nnn%d" % _namednodenum


class OpScale(P):
    def apply(self, p):
        # print type(p)
        # print "  scale ",self," on ",p, " result",P(self.x*p.x,self.y*p.y)
        if hasattr(p, 'noscale') and p.noscale == True: return p
        return P(self.x * p.x, self.y * p.y)


class OpRotate(P):
    def __init__(self, phi):
        self.phi = math.pi * phi / 180.0
        self.phideg = phi

    def apply(self, p):
        return P(p.x * math.cos(self.phi) - p.y * math.sin(self.phi),
                 p.x * math.sin(self.phi) + p.y * math.cos(self.phi))

    def rotations(self): return self.phideg

    def __str__(self): return "Rot(%g)" % self.phi


class PObject:
    _objnum = 1

    def __init__(self, **kwargs):

        if 'gseaddr' not in kwargs: raise Exception("GSEADDR expected")
        if 'name' not in kwargs:
            self.name = "p%d" % PObject._objnum
            PObject._objnum += 1
        else:
            self.name = kwargs['name']
        self.params = kwargs
        self.source = None
        self.margin = P(0, 0)

    def smprepr(self):
        return str(self.__class__).split(".")[1] + "(" + self.name + ")" + self._contents_()

    def _contents_(self):
        return ''

    def __repr__(self):
        return self.__str__()

    def getp(self, p, tr):
        q = P(p)
        for i in xrange(len(tr) - 1, -1, -1): q = tr[i].apply(q)
        return p.evalpost(q)

    def getvector(self, p, tr):
        q = P(p)
        for i in xrange(len(tr) - 1, -1, -1): q = tr[i].applyvect(q)
        return q

    def getrotations(self, tr):
        q = 0
        for i in xrange(len(tr) - 1, -1, -1): q += tr[i].rotations()
        return q

    def draw(self, d, tr, **kwargs):
        pass

    def bbox(self):
        raise Exception("BBox not implemented yet")

    # local params: higher priority
    def getparams(self, **kwargs):
        kwargs.update(self.params)
        return kwargs

    def pict(self, **kwargs):
        return self

    def getsource(self):
        return self.source

    def metanode(self):
        return 0


class PointBase(PObject, P):
    def __init__(self, p, **kwargs):
        PObject.__init__(self, **kwargs)
        P.__init__(self, p, **kwargs)

    def bbox(self, tr=None):
        return BBox(self)

    def __str__(self):
        return P.__str__(self)

    def _contents_(self):
        return self.__str__()


class Node(PointBase):
    def draw(self, __d, __tr, **kwargs):
        return __d.node(self, __tr, **self.getparams(**kwargs))

    # def __str__(self):
    #    return PointBase.__str__(self)+" "+self.name


class NamedNode(Node):
    def draw(self, __d, __tr, **kwargs):
        return __d.namednode(self, __tr, **self.getparams(**kwargs))


class MetaNode(Node):
    def __init__(self, p, **kwargs):
        Node.__init__(self, p, **kwargs)
        if 'mtype' in kwargs:
            self.mtype = kwargs['mtype']
        else:
            self.mtype = 0

    def draw(self, __d, __tr, **kwargs):
        return __d.metanode(self, __tr, **self.getparams(**kwargs))

    def metanode(self):
        return 1


# For assigning labels to points (in Group only).
class Variable(Node): pass


# Only named nodes are used
class NLine(PObject):
    def __init__(self, s, e, **kwargs):
        self.s = s
        self.e = e
        if 'lw' in kwargs and kwargs['lw'] is None: raise Exception
        PObject.__init__(self, **kwargs)

    def __str__(self):
        return "%s-%s" % (self.s, self.e)

    def draw(self, __d, __tr, **kwargs):
        df = self.getparams(**kwargs)
        return __d.nline(self, df.get('linelabel', None), __tr, **df)

    def bbox(self, tr):
        return None

    def _contents_(self):
        return str(self.s) + "-" + str(self.e)


class NBox(NLine):
    def draw(self, __d, __tr, **kwargs):
        return __d.nbox(self, __tr, **self.getparams(**kwargs))


class NCurve(NLine):
    def draw(self, __d, __tr, **kwargs):
        df = self.getparams(**kwargs)
        return __d.ncurve(self, df.get('linelabel', None), __tr, **df)


class Line(D, PObject):
    def __init__(self, s, e, **kwargs):
        D.__init__(self, s, e)
        PObject.__init__(self, **kwargs)

    def __str__(self):
        return "%s%s" % (self.a, self.b)

    def draw(self, __d, __tr, **kwargs):
        df = self.getparams(**kwargs)
        return __d.line(self, df.get('linelabel', None), __tr, **self.getparams(**kwargs))

    def bbox(self, tr):
        return BBox(self.getp(self.a, tr), self.getp(self.b, tr))


class Polygon(PObject):
    def __init__(self, p, **kwargs):
        PObject.__init__(self, **kwargs)
        self.p = p

    def draw(self, __d, __tr, **kwargs):
        return __d.polygon(self, __tr, **self.getparams(**kwargs))

    def bbox(self, tr):
        bb = BBox()
        for p in self.p: bb.addp(self.getp(p, tr))
        return bb


class Convex(Polygon):
    def draw(self, __d, __tr, **kwargs):
        return __d.convex(self, __tr, **self.getparams(**kwargs))


class Bezier(Polygon):
    def draw(self, __d, __tr, **kwargs):
        return __d.bezier(self, __tr, **self.getparams(**kwargs))


class Label(Node):
    def __init__(self, p, s, side="cm", **kwargs):
        Node.__init__(self, p, **kwargs)
        self.s = s
        self.side = side

    def label(self, d):
        d.label(self)

    def draw(self, __d, __tr, **kwargs):
        return __d.label(self, __tr, **self.getparams(**kwargs))


class NLabel(PObject):
    def __init__(self, p, s, side="cm", **kwargs):
        PObject.__init__(self, **kwargs)
        self.p = p
        self.s = s
        # v2017.10.28.11.24.15 - framespec - insert also in other types
        if self.s and self.s[:2] == '!f':
            self.framespec = self.s[2:].split(";")[0]
        self.side = side

    def nlabel(self, d):
        d.nlabel(self)

    def draw(self, __d, __tr, **kwargs):
        return __d.nlabel(self, __tr, **self.getparams(**kwargs))

    def __str__(self):
        return "<%s:%s>" % (self.p, self.s)

    def _contents_(self):
        return self.__str__()


class NSquare(PObject):
    def __init__(self, p, **kwargs):
        PObject.__init__(self, **kwargs)
        self.p = p

    def draw(self, __d, __tr, **kwargs):
        return __d.nsquare(self, __tr, **self.getparams(**kwargs))


class NCircle(PObject):
    def __init__(self, p, **kwargs):
        PObject.__init__(self, **kwargs)
        self.p = p

    def draw(self, __d, __tr, **kwargs):
        return __d.ncircle(self, __tr, **self.getparams(**kwargs))


class NMark(PObject):
    def __init__(self, p, n, **kwargs):
        PObject.__init__(self, **kwargs)
        if 'marktype' in kwargs:
            a = kwargs['marktype']
            if a not in marknames:
                self.params['marktype'] = marknames[a % len(marknames)]
        self.params['mark'] = getmark(n)
        self.p = p

    def draw(self, __d, __tr, **kwargs):
        return __d.nmark(self, __tr, **self.getparams(**kwargs))


class MultiMark(PObject):
    def __init__(self, p, ml, **kwargs):
        PObject.__init__(self, **kwargs)
        self.p = p
        self.ml = [getmark(n) for n in ml]

    def draw(self, __d, __tr, **kwargs):
        return __d.multimark(self, self.ml, __tr, **self.getparams(**kwargs))


class Box(Line):
    def draw(self, __d, __tr, **kwargs):
        return __d.box(self, __tr, **self.getparams(**kwargs))


class BBox(D):

    def __init__(self, *args):
        D.__init__(self, P(0, 0), P(0, 0))
        self.set = False
        self.store = 0
        for i in args: self.addp(i)

    def storepoints(self):
        self.store = 1
        self.points = []

    def addp(self, p):
        if not p: return
        # print "bb-insert",p

        if self.set:
            self.a.x = min(self.a.x, p.x)
            self.a.y = min(self.a.y, p.y)
            self.b.x = max(self.b.x, p.x)
            self.b.y = max(self.b.y, p.y)
        else:
            self.a = P(p)
            self.b = P(p)
            self.set = True
        if self.store: self.points.append(p)

    def add(self, cb):
        self.addp(cb.a)
        self.addp(cb.b)

    def getwh(self):
        return abs(self.b.x - self.a.x), abs(self.b.y - self.a.y)


class Group(PObject):

    def __init__(self, *args, **kwargs):
        PObject.__init__(self, **kwargs)
        self.p = []
        self.n = {}
        self.source = kwargs.get("source", None)
        for i in args: self.add(i)
        self.margin = P(0, 0)

    def add(self, o):
        if o.__class__ == Variable: self.n[o.name] = o
        self.p.append(o)
        return self

    def __str__(self):
        return "Group{%s...}" % self._contents_()[:20]

    def _contents_(self):
        return "{" + ",".join("%s" % o.smprepr() for o in self.p) + "}"

    # accessing variables
    def __getitem__(self, item):
        if item in self.n:
            return self.n[item]
        for o in self.p:
            if isinstance(o, Group):
                if item in o.n:
                    return o.n[item]

    # compute bounding box
    def bbox(self, tr):
        bb = BBox()
        for p in self.p: bb.add(p.bbox(tr))
        return bb

    def draw(self, __d, __tr, **kwargs):
        dr = False
        params = self.getparams(**kwargs)
        for p in self.p: dr = p.draw(__d, __tr, **params) or dr
        return dr


class Oper(PObject):
    def __init__(self, o, **kwargs):
        PObject.__init__(self, **kwargs)
        self.o = o
        self.oper = None  # subclasses

    # def draw(self,d,tr,**kwargs):
    def draw(self, __d, __tr, **kwargs):
        __tr = __tr[:]
        __tr.append(self.oper)
        return self.o.draw(__d, __tr, **kwargs)

    def bbox(self, tr):
        tr = tr.copy().append(self.oper)
        return self.o.bbox(tr)

    def getsource(self):
        return self.o.getsource()


class Scale(Oper):
    def __init__(self, o, p, **kwargs):
        Oper.__init__(self, o, **kwargs)
        self.oper = OpScale(p)


class Rotate(Oper):
    def __init__(self, o, phi, **kwargs):
        Oper.__init__(self, o, **kwargs)
        self.oper = OpRotate(phi)


class ScaleTo(Oper):
    def __init__(self, o, p, **kwargs):
        Oper.__init__(self, o, **kwargs)
        self.p = P(p)
        if self.p.x < 0 or self.p.y < 0:
            raise Exception("Cannot scale to negative size")

    delta = 0.0001
    deltaScaleLimit = 0.00000001

    def draw(self, __d, __tr=None, **kwargs):
        # from drawca import Ca
        # from drawwrapper import Wrapper

        k = dict(kwargs)
        if "filename" in k: k.pop("filename")
        k['margin'] = 0

        def getbb(sc):
            # wrapper=Wrapper(**k)
            # wrapper.draw(Scale(self.o,sc,**k))
            # ca=Ca(wrapper,".",**k)
            # return P(ca.getwstate(0,None,None)[1:3])
            from pitranslator import Translator
            t = Translator(**k)
            if 'gseaddr' not in k: k['gseaddr'] = self.params['gseaddr']
            picture = t.draw(Scale(self.o, sc, **k))
            return P(picture.boundingbox().getwh())

        if not self.oper:
            cnt = 100
            sc = P(1.0, 1.0)
            prevdiff = P(0, 0)
            findy = 1
            while cnt:
                newsize = getbb(sc)
                if newsize.x == 0 or newsize.y == 0: break  # cannot scale
                diff = newsize - self.p
                # print cnt,"SCALE=%s"%sc,"CURSIZE=%s"%newsize,"DIFF=%s"%diff
                if (self.p.x and abs(diff.x) <= ScaleTo.deltaScaleLimit or not self.p.x) \
                        and (self.p.y and abs(diff.y) <= ScaleTo.deltaScaleLimit or not self.p.y):
                    break

                if self.p.x: nsx = sc.x * self.p.x / newsize.x
                if self.p.y: nsy = sc.y * self.p.y / newsize.y
                if not self.p.x: nsx = nsy
                if not self.p.y: nsy = nsx

                sc = P(nsx, nsy)
                cnt = cnt - 1

            self.oper = OpScale(sc)

        return Oper.draw(self, __d, __tr, **kwargs)
