# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl

from itertools import chain
from frontenddefs import tokenize, tokenizegroups, pptokens, GSENAME, GSESTRING, GSEEOF, GSENUMBER, debugtreeparse
from htlib import _tostr, GseError
import random

from math import log
from pict import P, NLine, Label, Group, Polygon, Variable, NamedNode, NLabel, MetaNode, NCurve
from pict import MultiMark, NMark
from ast import literal_eval
from prim import endatt
import traceback

# Node Types
TREENODE = 64
ALLTYPES = 127  # by types

# Tree walking constants
LEAVES = 128  # just leaves
INNER = 256  # internal & non-root
ROOT = 512
INTERNAL = INNER | ROOT  # internal inluding root

ALLDEGS = LEAVES | INTERNAL  # all
POSTFIX = 1024
INFIX = 2048
PREFIX = 2048 * 2


class LineProperty:

    def __init__(self, linecolor=None, linetype=None, lw=None, linelabel=None, linelabel2=None, dash=None):
        self.linecolor = linecolor
        self.linetype = linetype
        self.lw = lw
        self.linelabel = linelabel
        self.linelabel2 = linelabel2
        self.dash = dash

    def get(self, e=None, e2=None):
        d = {}
        lb = self.linelabel
        if lb and e:
            lb = lb + e
        else:
            lb = lb or e
        if lb: d['linelabel'] = lb
        lb = self.linelabel2
        if lb and e2:
            lb = lb + e2
        else:
            lb = lb or e2
        if lb: d['linelabel2'] = lb

        if self.linecolor: d['linecolor'] = self.linecolor
        if self.lw: d['lw'] = self.lw

        if self.linetype: d['linetype'] = self.linetype
        if self.dash: d['dash'] = self.dash
        return d

    def __str__(self):
        s = ""
        if self.lw: s += " lw=%f " % self.lw
        if self.linecolor: s += " linecolor=%s " % _tostr(str(self.linecolor))
        if self.linetype: s += " linetype='%s'" % self.linetype
        if self.linelabel: s += " linelabel=%s " % _tostr(self.linelabel)
        if self.linelabel2: s += " linelabel2=%s " % _tostr(self.linelabel2)
        return s


class TreeNode:
    treenodenum = 1

    def __init__(self, *args, **kwargs):
        self.c = args
        for c in self.c: c.parent = self
        self.parent = None
        self.stype = TREENODE
        self.labels = []
        self.nnum = TreeNode.treenodenum
        TreeNode.treenodenum += 1
        self.branchlendef = False
        for k in kwargs: setattr(self, k, kwargs[k])
        if not self.branchlendef: self.branchlen = 1.0

        self.setlineproperty()
        if self.getvalues('swap') and len(self.c) == 2:
            self.c = [self.c[1], self.c[0]]
            self.dellabel('swap')

        if self.getvalues('reverse'):
            self.reverse()
            self.dellabel('reverse')

        if not hasattr(self, 'gseaddr'):
            raise Exception("no gseaddr")

    def setlineproperty(self):
        self.prop = LineProperty(
            self.getdefvalue("linecolor"),
            self.getdefvalue("linetype"),
            self.getdefvalue("lw"),
            self.getdefvalue("linelabel"),
            self.getdefvalue("linelabel2"),
            self.getdefvalue("dash")
        )

    def path2root(self):
        if not self.parent: return self,
        return (self,) + self.parent.path2root()

    def reverse(self):
        self.c = list(self.c)
        self.c.reverse()
        for c in self.c: c.reverse()

    def leaves(self):
        if self.c: return list(chain.from_iterable([l.leaves() for l in self.c]))
        return [self]

    def size(self):
        if self.c: return sum(l.size() for l in self.c)
        return 1

    def leaf(self):
        return not self.c

    def getfirstsinglevalue(self):
        for l in self.labels:
            if type(l) != tuple:
                if type(l) == str and len(l) >= 2 and l[0:2] == "__": continue  # skip hidden labels
                return l
        return ''

    def getfirstsinglelabel(self):
        return str(self.getfirstsinglevalue())

    def smp(self, **kwargs):
        if self.leaf(): return self.getfirstsinglelabel()
        return "(" + ",".join((c.smp(**kwargs) for c in self.c)) + ")"

    def nodes(self, tp=PREFIX | ALLDEGS | ALLTYPES):

        if self.leaf():
            adm = (tp & ROOT and not self.parent) or tp & LEAVES
            if adm and tp & self.stype: yield self
        else:
            adm = (tp & INNER and self.parent) or (tp & ROOT and not self.parent)
            if adm and tp & PREFIX and tp & self.stype: yield self
            if tp & INFIX:
                if len(self.c) > 2: raise Exception("INFIX require <=2 children")
                for i in self.c[0].nodes(tp): yield i
                yield self
                if len(self.c) > 1:
                    for i in self.c[1].nodes(tp): yield i
            else:
                for i in chain.from_iterable((c.nodes(tp) for c in self.c)): yield i
            if adm and tp & POSTFIX and tp & self.stype: yield self

    def lflabels(self):
        if self.c: return set(chain.from_iterable([c.lflabels() for c in self.c]))
        return {self.smplabel()}

    def height(self):
        if self.c: return 1 + max(c.height() for c in self.c)
        return 0

    def smplabel(self, **kwargs):
        return self.smp(**kwargs)

    def nodelabel(self, **kwargs):
        nl = kwargs.get('nodelabels', [])
        s = ""
        if nl:
            s = self.getnodelabel(nl, kwargs.get('labeldelim',
                                                 ' '))  # NEW
            if not s: return ""
            return s
        return self.smp(**kwargs)

    def extractdecoration(self, vallist, labeldelim):
        s = ''
        first = True
        prefix = ''
        for i, v in enumerate(vallist):
            if not v: continue
            if v[0] == '?':
                prefix = v[1:]
                continue
            if prefix:
                empty = 0
                for k in decorfuns:
                    if k == prefix[:len(k)]:
                        if decorfuns[k](self):
                            prefix = prefix[len(k):]
                            break
                        else:
                            prefix = ''
                            empty = 1  # condition not satisfied
                            break
                if empty: continue  # skip

            if v and v[0].isalpha():
                vals = self._getvalues(v)
                if not vals:
                    prefix = ''  # reset, empty label
                    continue
                if not first: s += labeldelim
                first = False
                s += prefix + labeldelim.join(str(val) for val in self._getvalues(v))
                prefix = ''  # reset
            else:
                s += v
        return s

    def getedgelabel(self, vallist, labeldelim):
        if vallist:
            return self.extractdecoration(vallist, labeldelim)
        return ''

    def getnodelabel(self, vallist, labeldelim):
        return self.extractdecoration(vallist, labeldelim)

    def dellabel(self, label):
        self.labels = [k for k in self.labels if not (type(k) == tuple and k[0] == label)]

    def getenv(self, label=None):
        return (l for l in self.labels if type(l) == tuple and len(l) == 2 and l[0] == label)

    def getdictlenv(self):
        return dict(l for l in self.labels if type(l) == tuple and len(l) == 2)

    def _getvalues(self, v):
        if type(v) == tuple:
            for i in v:
                s = self.getvalues(i)
                if s: return s
        else:
            return self.getvalues(v)
        return []

    def getvalues(self, label=None):
        s = [l[1] for l in self.getenv(label)]
        if self.leaf() and label == "leaflabel" and s:  # don't append smp if attr leaflabel is defined
            return s
        if label == 'label' or (label == 'leaflabel' and self.leaf()) or (label == 'intlabel' and not self.leaf()):
            s.append(self.smp())
        if label == 'cluster':
            lf = sorted(self.lflabels())  # sort clusters
            return lf
        if label == 'branchlen':
            s.append(self.branchlen)
        return s

    def getdefvalue(self, label, val=None):
        for l in self.getenv(label): return l[1]
        return val

    def hasvalue(self, val):
        return val in self.labels

    # def find(self,lab, tp=ALLDEGS|ALLTYPES|PREFIX):
    #     return ( n for n in self.nodes(tp) if lab in n.labels )
    def setparent(self):
        for n in self.c: n.parent = self

    def _labels(self, skiplabs=None, fixedlabs=None, flattenlabs=None):
        def _tostr2(l):
            if type(l) == tuple:
                if skiplabs and l[0] in skiplabs: return ''
                if fixedlabs and l[0] not in fixedlabs: return ''
                if flattenlabs and l[0] in flattenlabs: return _tostr(l[1])
                return _tostr(l)
            if fixedlabs: return ''
            if skiplabs: return _tostr(l)
            return ''

        s = " ".join(_tostr2(l) for l in self.labels if _tostr2(l))
        if not self.c:  # leaf
            if not fixedlabs or 'leaflabel' in fixedlabs:
                sl = self.smp()
                if sl:
                    if s and s[-1] != ' ':
                        s = sl + " " + s
                    else:
                        s = sl + s
        return s

    def str2(self, skiplabs=None, fixedlabs=None, flattenlabs=None):
        num = ""
        lb = self._labels(skiplabs, fixedlabs, flattenlabs)
        if skiplabs and 'branchlen' in skiplabs:
            ln = ''
        elif fixedlabs and 'branchlen' not in fixedlabs:
            ln = ''
        else:
            ln = ":%f" % self.branchlen if self.branchlendef else ""
        if self.c:
            return "(" + ",".join((c.str2(skiplabs, fixedlabs, flattenlabs) for c in self.c)) + ")" + ln + (
                " " + lb if lb else "") + num
        return lb + ln + num

    def __str__(self):
        return self.str2()

    def smprepr(self, addlabels=0):
        s = ""
        if addlabels: s = " " + self._labels()
        if not self.c: return self.smp() + s
        return "(" + ",".join((c.smprepr(addlabels) for c in self.c)) + ")" + s

    def __repr__(self):
        return self.__str__()

    def copy(self, addlabels=None):
        if self.leaf():
            c = self.__class__(labels=self.getlabels() + (addlabels if addlabels else []), gseaddr=self.gseaddr)
            if hasattr(self, "genesource"): c.genesource = self.genesource
            return c
        return self.__class__(*[c.copy() for c in self.c], labels=self.getlabels() + (addlabels if addlabels else []),
                              gseaddr=self.gseaddr)

    def getlabels(self):
        return self.labels[:]

    def setlabel(self, l, v):
        self.labels.append((l, v))
        setattr(self, l, v)
        self.setlineproperty()


class Tree:
    num = 1

    def __init__(self, root, **kwargs):
        self.gseaddr = kwargs.get('gseaddr', None)

        from evallib import GseObj
        gseroot = None
        if root.__class__ == GseObj:
            self.gseaddr = root.gseaddr
            gseroot = root
            root = root.src
        if type(root) == str:
            self.source = root
            if not self.gseaddr:
                raise Exception("Warning: no address for" + str(root))

            kwargs['gseaddr'] = self.gseaddr
            root = parse(root, **kwargs)

        self.treename = None
        if root.getvalues("treename"):
            self.treename = root.getvalues("treename")[0]
        else:
            if root.c:  # treename is omitted for single-noded trees; use treename instead
                self.treename = root.getfirstsinglelabel()
        self.root = root

        self.nodes = list(root.nodes())

        self.nodes = list(root.nodes())
        self.leaves = list(root.leaves())
        self.metanodes = {}
        for k in kwargs: setattr(self, k, kwargs[k])
        num = 0
        for n in self.nodes:
            n.num = num
            num += 1
            n.setparent()
        self.num = Tree.num
        Tree.num += 1

    def nodesgen(self, tp=PREFIX | ALLTYPES | ALLDEGS):
        return self.root.nodes(tp)

    def __repr__(self):
        return self.root.__repr__()

    def __str__(self):
        return self.__repr__()

    def height(self):
        return self.root.height()

    def dotgraph(self, **kwargs):
        from graph import Graph
        g = Graph(self.gseaddr)

        def processg(metagraph):
            sl = {}
            for n in self.leaves:
                if n.parent != self.root or n.metagraph != bool(metagraph): continue

                if not n.labels:
                    print "Warning: a node without attributes - omitted"
                    continue

                if n.smplabel() in sl:
                    print "Warning: %s node %s is already defined" % (metagraph, n.smplabel())
                else:
                    if n.smplabel(): sl[n.smplabel()] = n

            for n in self.root.c:
                if n.leaf() or n.metagraph != bool(metagraph): continue

                if len(n.c) > 1:
                    for f in n.c:
                        if f.smplabel():
                            if f.smplabel() not in sl:
                                print "Warning: %s node %s is undefined" % (metagraph, f.smplabel())
                                sl[f.smplabel()] = f

                    f = sl.get(n.c[0].smplabel(), n.c[0])
                    for fn in n.c[1:]:
                        d = n.getdictlenv()
                        for vs in ['curve', 'up']:
                            crv = n.getvalues(vs)
                            if crv: d[vs] = crv[0]
                        fn = sl.get(fn.smplabel(), fn)
                        d.update(
                            n.prop.get(n.getedgelabel(kwargs.get('edgelabels', []), kwargs.get('labeldelim', ' '))))
                        if 'multigraph' in kwargs: d['multigraph'] = 1
                        e = g.eset(f, fn, **d)
                        f = fn
                        d['src'] = n
                        e.decor(**d)

            for n in self.root.c:
                if n.labels and n.leaf() and n not in g.V and n.metagraph == bool(metagraph):
                    g.__add__(n)

        for n in self.nodes: n.metagraph = bool(n.getvalues('metagraph'))
        processg(False)
        processg("metagraph")
        return g

    def add(self, **kwargs):
        for i in kwargs:
            self.root.labels.append((i, kwargs[i]))

    def addmetanode(self, n):
        self.metanodes[n.name] = n

    def _setleafline(self, n, leafline):
        if leafline:
            if type(leafline) == tuple:
                n.lfpos = MetaNode(n.pos - P(leafline), gseaddr=n.gseaddr, name="lflab%d" % n.nnum, side="B")
            else:
                n.lfpos = MetaNode(n.pos - P(0, leafline), gseaddr=n.gseaddr, name="lflab%d" % n.nnum, side="B")
            self.addmetanode(n.lfpos)
        else:
            n.lfpos = n.pos


def parse(s, TN=TreeNode, **kwargs):
    tokens = tokenizegroups(tokenize(s))

    initialstring = s
    gseaddr = kwargs.get('gseaddr', None)
    if not gseaddr:
        traceback.print_stack()
        print "Parse: no address given for", s

    def parseVal(t, index, **kwargs):

        while index < len(t) and t[index].token in ' \n\t':
            index += 1
        if index >= len(t):
            raise GseError("Cannot evaluate expression in object: <%s>" % t, gseaddr, t[index - 1])
        src = t[index]
        ctyp, ctok = src.typ, src.token

        if ctyp == GSESTRING:
            return ctok[1:-1], index + 1
        if ctyp == GSENAME: return kwargs.get(ctok, ctok), index + 1
        if ctok == "-" and t[index + 1].typ == GSENUMBER:
            return -literal_eval(t[index + 1].token), index + 2
        if ctok == "-" and t[index + 1].typ == GSENAME:
            ctok = t[index + 1].token
            if ctok in kwargs:
                ctok = kwargs.get(ctok, ctok)
                if is_number(ctok): return -ctok, index + 2

        if ctyp == GSENUMBER: return literal_eval(ctok), index + 1
        if ctok in "([":
            r = t[index].insrc()

            locs = {}
            locs.update(kwargs)
            while True:
                try:
                    import itertools
                    res = eval(r, globals(), locs)
                    break  # Evaluation OK
                except NameError as e:
                    s = e.args[0].split("'")[1]
                    locs[s] = s



                except Exception as e:
                    raise GseError("Cannot evaluate expression: <%s>" % r, gseaddr, src)
            return res, index + 1

        raise GseError("Value (num,label,str,[] or tuple) expected. Found <%s>." % t[index].src[t[index].p:], gseaddr,
                       src)

    def skipspaces(t, i):
        while i < len(t) and t[i].token in " \n\t": i += 1
        return i

    def parseNode(t, index, **kwargs):
        children = []
        labels = []
        blen = 1.0
        pos = None
        addattr = {}
        blendef = False

        gseaddr = t[index]
        while index < len(t) and t[index].typ != GSEEOF and t[index].token not in ",)":
            src = t[index]
            ctyp, ctok = src.typ, src.token
            if ctok in ' \n\t':
                index += 1
                continue

            if debugtreeparse:
                print ":::", ctyp, str(ctok)[:40],
                print pptokens([t[index]])

            if not ctok:
                print initialstring
                raise GseError("Parse error: unexpected end of tree string", gseaddr, src)

            if ctok == "(":
                cindex = 1
                grp = t[index].grp
                while True:
                    c, cindex = parseNode(grp, cindex, **kwargs)
                    children.append(c)  # new child
                    while cindex < len(grp) and grp[cindex].token in ' \n\t': cindex += 1  # skip spaces
                    if cindex >= len(grp):
                        raise GseError("Parse error: expected )", gseaddr, t[index])
                    if grp[cindex].token == ',':
                        cindex += 1
                    elif grp[cindex].token == ')':
                        break
                index += 1
                continue

            if ctyp == GSENAME:
                srcindex = index
                index = skipspaces(t, index + 1) - 1

                if (index + 1) < len(t) and t[index + 1].token == "=":
                    index = skipspaces(t, index + 1) - 1
                    lab = ctok
                    v, index = parseVal(t, index + 2, **kwargs)  # single pythonic
                    labels.append((lab, v))

                    if lab in ["parentpos", "lfpos", "legendpos", "pos"]: addattr[lab] = P(v)
                    continue
                else:
                    index = srcindex

            if ctok in "+^.~":  # check -NUM
                labels.append(ctok)
                index += 1
                continue
            if ctok in "-":
                if index + 1 == len(t):
                    labels.append(ctok)
                    index += 1
                    continue
                nx = t[index + 1].typ
                nl = t[index + 1].token
                if nx != GSENAME or nl not in kwargs or (nl in kwargs and not is_number(kwargs[nl])):
                    labels.append(ctok)
                    index += 1
                continue

            if ctyp == GSESTRING or ctyp == GSENAME or ctyp == GSENUMBER or ctok in "-":
                v, index = parseVal(t, index, **kwargs)
                labels.append(v)
                continue

            if ctok == ':':
                blen, index = parseVal(t, index + 1, **kwargs)  # literal_eval(t[0][1]) # prev. float
                blendef = True
                continue

            if ctok == ";":  # ????
                index += 1
                continue

            if ctok == '[':
                lab = "pos"
                v, index = parseVal(t, index, **kwargs)
                if len(v) == 1:
                    v = v[0]
                elif len(v) == 2:
                    v = tuple(v)
                else:
                    raise GseError("At most two values excepted for defining pos by list", gseaddr, src)
                kr = (lab, v)
                labels.append(kr)
                addattr[lab] = P(v)
                continue

            raise GseError("Parse error: expected label, string or :number. Found #%s#." % ctok, gseaddr, src)
        return TN(*children, labels=labels, branchlen=blen, branchlendef=blendef, gseaddr=gseaddr, **addattr), index

    if debugtreeparse:
        print pptokens(tokens, 1)
    tr, index = parseNode(tokens, 0, **kwargs)
    if index < len(tokens) and tokens[index].token:
        raise GseError("Parse error, symbol left at the end of input string: %s\n%s" % (tokens[index].token, s),
                       gseaddr, tokens[index])
    return tr
