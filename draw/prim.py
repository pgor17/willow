# Written by P.Gorecki 2003-2018
# gorecki@mimuw.edu.pl

import cairo
from math import atan2, pi, cos, sin

from htlib import getcolor, P, mathtokenizer, STYLESUBSCRIPT, STYLESUBSCRIPT2, STYLESUPSCRIPT, STYLENORMAL
from pict import marknames
from tools import convex_hull

MATHSUPERSCRIPT = -0.5  # math style superscript
MATHSUBSCRIPT = 0.23  # math style subscript
MATHSUBSCRIPT2 = 0.16  # math style subscript
ARROWSCALE = 1

ROTFACTOR = 100000


def pconv(t):
    if t.__class__ == P: return "(%g,%g)" % (t.x, t.y)
    if type(t) == tuple: return str(t)
    if type(t) == str: return '"%s"' % t
    return str(t)


# symbolshift=31

# print "CAIRO",cairo.cairo_version()
# if cairo.cairo_version()<11406: symbolshift=31
# else: symbolshift=11


def inferrtr(xmove, ymove, phi, xt, yt):
    return xmove + xt * cos(1.0 * phi) - yt * sin(1.0 * phi), ymove + xt * sin(1.0 * phi) + yt * cos(1.0 * phi)


def ctrlpt(ctx, x, y, size=13, col=(1, 0, 0, 0.5), lw=1.0, fill=1):
    ctx.save()
    ctx.set_line_width(lw)
    if fill:
        ctx.arc(x, y, size, 0, 2 * pi)
        ctx.set_source_rgba(0, 0, 1, 0.5)
        ctx.fill()
    ctx.arc(x, y, size, 0, 2 * pi)
    ctx.set_source_rgba(*col)
    ctx.stroke()
    ctx.restore()


class PrimObj:
    primobjnum = 1

    def __init__(self):
        self.id = PrimObj.primobjnum
        PrimObj.primobjnum += 1
        self.priority = 0

    def setpriority(self, priority):
        if priority: self.priority = priority

    def stroke(self, ctx, color, lw, dash=None):
        ctx.set_line_width(lw * 2.0)
        if type(color) == float: color = (color, color, color)
        ctx.set_source_rgb(*color)

        if dash:
            ctx.save()
            ctx.set_dash(dash)
        ctx.set_line_cap(cairo.LINE_CAP_ROUND)
        ctx.stroke()
        if dash: ctx.restore()

    def fill(self, ctx, fillstyle, fillcolor=None):
        if fillstyle == "solid":
            if type(fillcolor) != tuple: fillcolor = (fillcolor, fillcolor, fillcolor)

            ctx.set_source_rgb(*fillcolor)
            ctx.fill()

        if type(fillstyle) == float:
            r, g, b = fillcolor
            ctx.set_source_rgba(r, g, b, fillstyle)
            ctx.fill()

    def gse(self, d):
        return "# PrimObj%d %s" % (self.id, self)


class PrimNamedNode(PrimObj):
    def __init__(self, p, name):
        PrimObj.__init__(self)
        self.p = p
        self.name = name

    def draw(self, wstate):
        # print "ASDAD",self.name
        pass

    def getbb(self, wstate, bb):
        pass

    def gse(self, d): return None

    def __str__(self): return "PrimNamedNode " + str(self.p) + " " + self.name


class PrimCircle(PrimObj):
    def __init__(self, p, radius, lw, color, fillcolor, fillstyle):
        PrimObj.__init__(self)
        self.p = p
        self.radius = radius
        self.lw = lw
        self.color = getcolor(color)
        self.fillcolor = getcolor(fillcolor)
        self.fillstyle = fillstyle

    def __str__(self):
        return "Circle" + str(self.p)

    def getbb(self, wstate, bb):
        # a=P(*wstate.xy(self.p))
        # bb.addp(a)
        mx = 4
        for i in xrange(mx):
            rad = self.radius * wstate.getfontsize()
            r = P(*wstate.xy(self.p + P(rad * cos(i * 2 * pi / mx), rad * sin(i * 2 * pi / mx))))
            bb.addp(r)

    def draw(self, wstate):
        x, y = wstate.xy(self.p)
        radius = self.radius * wstate.getfontsize()
        if self.fillstyle != "none":
            wstate.ctx.arc(x, y, radius, 0, 2 * pi)
            self.fill(wstate.ctx, self.fillstyle, self.fillcolor)

        wstate.ctx.arc(x, y, radius, 0, 2 * pi)
        self.stroke(wstate.ctx, self.color, self.lw)
        return 1


class PrimLine(PrimObj):
    def __init__(self, a, b, lw, color, linetype, label, labelcolor, dash, extsyntax, arrowlength, labelspacing,
                 labelalign, fontsize):
        PrimObj.__init__(self)
        self.a = a
        self.b = b
        # print "PrimLine/Crv",self.a,self.b
        self.lw = lw
        if lw is None: raise Exception

        self.color = getcolor(color)
        self.linetype = linetype
        self.label = label
        self.labelcolor = getcolor(labelcolor)
        self.dash = dash
        self.extsyntax = extsyntax
        self.arrowlength = arrowlength
        self.labelspacing = labelspacing
        self.labelalign = labelalign
        self.fontsize = fontsize
        self.args = label, lw, color, pconv(
            linetype), labelcolor, dash, extsyntax, arrowlength, labelspacing, labelalign, fontsize

    def gseaddlab(self, s, l):
        if l: return s + " '%s'" % l
        return s

    def gse(self, d):
        pa = d.setdefault((self.a.x, self.a.y), "pa%d" % self.id)
        pb = d.setdefault((self.b.x, self.b.y), "pb%d" % self.id)
        return self.gseaddlab(
            "line %s %s lw=%g color=%s linetype=%s labelcolor=%s dash=%s extsyntax=%d arrowlength=%g labelspacing=%s labelalign=%s fontsize=%d" % (
                        (pa, pb) + self.args[1:]), self.args[0])

    def bbox(self, wstate, bb):

        a = P(*wstate.xy(self.a))
        b = P(*wstate.xy(self.b))
        bb.addp(a)
        bb.addp(b)

    def _draw(self, wstate, a, b, phi):
        wstate.ctx.line_to((a - b).ln(), 0)

    def _postdraw(self, wstate, a, b, phi):
        pass

    def getarr1points(self, wstate, a, b, ln):
        return ln, 0, 0, 0, wstate.translaterotate(a, b)

    def getarr2points(self, wstate, a, b, ln):
        return ln, 0, 0, 0, wstate.translaterotate(b, a)

    def getlabel(self, ctx, a, b, ln, labelalign):
        sr = a * labelalign + b * (1 - labelalign)
        v = b - a
        phi = -180 * atan2(v.y, v.x) / pi
        return PrimNLabelMulti(sr, self.label, "yc",
                               self.color, phi, self.extsyntax, self.labelspacing, self.fontsize)

    def genbbpoints(self, wstate, a, b, ln):
        pass

    def getarln(self, wstate):
        return ARROWSCALE * self.arrowlength * wstate.getfontsize()

    def getbb(self, wstate, bb):

        if self.lw > 0:

            a = self._a = P(*wstate.xy(self.a))
            b = self._b = P(*wstate.xy(self.b))
            ln = self._ln = (self._a - self._b).ln()

            bb.addp(a)
            bb.addp(b)
            arln = self.getarln(wstate)
            if self.linetype[2] == ">":
                wstate.save()
                x1, y1, x2, y2, phi = self.getarr2points(wstate, a, b, ln)
                x1, y1, x2, y2 = self._drawarrow2(x1, y1, x2, y2, wstate, arln, self.color, self.lw)
                bb.addp(P(inferrtr(b.x, b.y, phi, x1, y1)))
                bb.addp(P(inferrtr(b.x, b.y, phi, x2, y2)))
                wstate.restore()

            if self.linetype[0] == "<":
                wstate.save()
                x1, y1, x2, y2, phi = self.getarr1points(wstate, a, b, ln)
                x1, y1, x2, y2 = self._drawarrow2(x1, y1, x2, y2, wstate, arln, self.color, self.lw)
                bb.addp(P(inferrtr(a.x, a.y, phi, x1, y1)))
                bb.addp(P(inferrtr(a.x, a.y, phi, x2, y2)))
                wstate.restore()

        if self.label:
            wstate.save()
            a = P(*wstate.xy(self.a))
            b = P(*wstate.xy(self.b))
            ln = (a - b).ln()
            pl = self.getlabel(wstate.ctx, a, b, ln, self.labelalign)
            pl.bbp = []
            pl._draw(wstate)
            v = b - a
            phi = atan2(v.y, v.x)
            for (x, y) in pl.bbp:
                bb.addp(P(inferrtr(pl.pos.x, pl.pos.y, phi, x, y)))
            wstate.restore()

    def draw(self, wstate):
        arln = self.getarln(wstate)
        wstate.save()
        a = self._a = P(*wstate.xy(self.a))
        b = P(*wstate.xy(self.b))

        phi = self._phi = wstate.translaterotate(a, b)
        # print "\nDRLNE",a,b,phi
        ln = self._ln = (a - b).ln()

        if self.linetype[0] == "<" or self.linetype[2] == ">":
            sh = 0.3
            lp = -arln * sh
            v = 2 * ln
            if self.linetype[0] == "<": lp = arln * sh
            rp = ln + arln * sh
            if self.linetype[2] == ">": rp = ln - arln * sh
            wstate.ctx.move_to(lp, v)
            wstate.ctx.line_to(rp, v)
            wstate.ctx.line_to(rp, -v)
            wstate.ctx.line_to(lp, -v)
            wstate.ctx.close_path()
            wstate.ctx.clip()

        self.bbpoints = []
        if self.lw > 0:
            wstate.ctx.move_to(0, 0)
            self._draw(wstate, a, b, phi)
            self.stroke(wstate.ctx, self.color, self.lw, self.dash)
            self._postdraw(wstate, a, b, phi)
        wstate.restore()

        if self.label:
            wstate.save()
            pl = self.getlabel(wstate.ctx, a, b, ln, self.labelalign)
            # def getlabel(self,ctx,a,b,ln,labelalign):
            # pl._draw(pl.pos,wstate)
            pl._draw(wstate)
            wstate.restore()

        self.genbbpoints(wstate, a, b, ln)

        if self.linetype[0] == "<":
            wstate.save()
            x1, y1, x2, y2, phi = self.getarr1points(wstate, a, b, ln)
            x1, y1, x2, y2 = self._drawarrow2(x1, y1, x2, y2, wstate, arln, self.color, self.lw)
            wstate.restore()

        if self.linetype[2] == ">":
            wstate.save()
            x1, y1, x2, y2, phi = self.getarr2points(wstate, a, b, ln)
            x1, y1, x2, y2 = self._drawarrow2(x1, y1, x2, y2, wstate, arln, self.color, self.lw)
            wstate.restore()

        return 1

    def _drawarrow2(self, sx, sy, ex, ey, wstate, arrowlength, color, lw, typefilled=1):
        angle = atan2(ey - sy, ex - sx) + pi
        arrowdegrees = pi / 7
        arrmid = 0.43
        x1 = ex + arrowlength * cos(angle - arrowdegrees);
        y1 = ey + arrowlength * sin(angle - arrowdegrees);
        x2 = ex + arrowlength * cos(angle + arrowdegrees);
        y2 = ey + arrowlength * sin(angle + arrowdegrees);
        x3 = ex + arrowlength * cos(angle) * arrmid;
        y3 = ey + arrowlength * sin(angle) * arrmid;
        wstate.ctx.set_line_width(lw * 2.0)
        wstate.ctx.set_source_rgb(*color)
        wstate.ctx.move_to(x1, y1)
        wstate.ctx.line_to(ex, ey)
        wstate.ctx.line_to(x2, y2)

        if typefilled:
            wstate.ctx.line_to(x3, y3)
            wstate.ctx.line_to(x1, y1)
            self.fill(wstate.ctx, "solid", color)
        wstate.ctx.stroke()
        return x1, y1, x2, y2


def endatt(t, p0, p1, p2, p3):
    # shorter line
    # de Casteljau algorithm
    t = t
    t1 = 1.0 - t
    # p0=P(0,0)
    q0, q1, q2 = p0 * t1 + p1 * t, p1 * t1 + p2 * t, p2 * t1 + p3 * t
    r0, r1 = q0 * t1 + q1 * t, q1 * t1 + q2 * t
    s0 = r0 * t1 + r1 * t
    return p0, q0, r0, s0


class PrimCurve(PrimLine):
    def __init__(self, a, b, lw, color, linetype, label, labelcolor, dash, extsyntax, arrowlength, labelsep, labelalign,
                 fontsize, up, crval, crvbe, curvetype, endat):
        # print "AB",a,b,crval,crvbe
        PrimLine.__init__(self, a, b, lw, color, linetype, label, labelcolor, dash, extsyntax, arrowlength, labelsep,
                          labelalign, fontsize)
        self.up = up
        self.crval = crval
        self.crvbe = crvbe
        self.endat = endat

        if curvetype == 0:  # oldstyle with al/be
            if type(self.crval) in (float, int):
                self.al = self.crval
                if self.up == 0 or self.up == 1:
                    self.be = self.crvbe * (-1.0 + 2 * self.up)
                else:
                    self.be = self.up
            else:
                curvetype = 1  # automate change

        if curvetype > 0:
            # absolute==1 - fixed points p1=al, p2=be
            # relative==2 - relative points p1=a+al, p2=a+be
            # relative==3 - relative scalled points: p1=a+al*|a-b|, p2=a+be*|a-b|
            # values of controlpoints
            self.crval = P(self.crval)
            self.crvbe = P(self.crvbe)

        self.curvetype = curvetype
        self.args = label, lw, color, pconv(
            linetype), labelcolor, dash, extsyntax, arrowlength, labelalign, fontsize, up, crval, crvbe, curvetype, endat

    def gse(self, d):
        pa = d.setdefault((self.a.x, self.a.y), "pa%d" % self.id)
        pb = d.setdefault((self.b.x, self.b.y), "pb%d" % self.id)
        return self.gseaddlab(
            "curve %s %s lw=%g color=%s linetype=%s labelcolor=%s dash=%s extsyntax=%d arrowlength=%g labelalign=%s fontsize=%d up=%d crval=%s crvbe=%s curvetype=%d endat=%f" % (
                        (pa, pb) + self.args[1:]), self.args[0])

    def prepcurvetype(self, wstate):

        if self.curvetype == 1:
            self._p1 = P(*wstate.xy(self.crval)) - self._a
            self._p2 = P(*wstate.xy(self.crvbe)) - self._a
        else:
            self._p1 = P(*wstate.xy(P(self.a) + self.crval))
            self._p2 = P(*wstate.xy(P(self.b) + self.crvbe))

    def getmainpoints(self, a, b, phi):
        ln = (a - b).ln()
        # print "GETMP",a,b,self._a,phi,(b-a).ln()
        if self.curvetype == 1:
            p1 = P(inferrtr(0, 0, -phi, self._p1.x, self._p1.y))
            p2 = P(inferrtr(0, 0, -phi, self._p2.x, self._p2.y))
        # print "GTMP1",self._p1,self._p2

        elif self.curvetype == 2:

            p1 = self._p1 - a
            p2 = self._p2 - a
            p1 = P(inferrtr(0, 0, -phi, p1.x, p1.y))
            p2 = P(inferrtr(0, 0, -phi, p2.x, p2.y))
        # print "GTMP2+",p1,p2

        elif self.curvetype == 3:
            p1 = self._p1 - a
            p2 = self._p2 - a
            p1 = P(inferrtr(0, 0, -phi, p1.x, p1.y))
            p2 = P(inferrtr(0, 0, -phi, p2.x, p2.y))
        # return p1,p2,P(ln,0)
        # print "GTMP2+",p1,p2

        else:
            p1, p2 = P(ln * self.al, ln * self.be), P(ln * (1 - self.al), ln * self.be)
        # if self.curvetype==2:
        #	return P(p1),P(p2),P(ln,0)
        p3 = P(ln, 0)
        if self.endat:
            _, p1, p2, p3 = endatt(self.endat, P(0, 0), p1, p2, p3)
        return p1, p2, p3

    def getarr1points(self, wstate, a, b, ln):
        phi = wstate.translaterotate(a, b)
        p1, _, _ = self.getmainpoints(a, b, phi)
        p01 = p1 * 0.5
        return p01.x, p01.y, 0, 0, phi

    def getarr2points(self, wstate, a, b, ln):
        phi = wstate.translaterotate(a, b)
        _, p2, p3 = self.getmainpoints(a, b, phi)
        p23 = (p2 + p3) * 0.5
        return p23.x, p23.y, p3.x, p3.y, phi

    def _draw(self, wstate, a, b, phi):
        # wstate.ctx.line_to((a-b).ln(),0)
        # def _draw(self,wstate,ln):
        self.prepcurvetype(wstate)

        # 	cm=cairo.Matrix(1,0,0,rev,0,0)
        # 	cm.translate(a.x,rev*a.y)
        # 	cm.rotate(rev*phi)

        p1, p2, p3 = self.getmainpoints(a, b, phi)
        wstate.ctx.curve_to(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y)

    def getbb(self, wstate, bb):
        # print "INGETBB"
        a = self._a = P(*wstate.xy(self.a))
        b = P(*wstate.xy(self.b))
        self.prepcurvetype(wstate)
        ln = (a - b).ln()

        # if a.x>b.x:
        # 	rev=1
        # 	v=b-a
        # else:
        # 	rev=-1
        # 	v=a-b
        # phi=atan2(v.y,v.x)
        # print "REV",rev

        v = b - a
        phi = atan2(v.y, v.x)
        p1, p2, p3 = self.getmainpoints(a, b, phi)

        # cm=cairo.Matrix(1,0,0,rev,0,0)
        # cm.translate(a.x,rev*a.y)
        # cm.rotate(rev*phi)

        m = 20
        self.bbpoints = []
        for i in xrange(m + 1):
            i = 1.0 * i / m
            i1 = 1.0 - i
            p = p1 * 3 * i1 * i1 * i + p2 * 3 * i1 * i * i + p3 * i * i * i
            # cubic bezier curve from p0=(0,0) to (0,ln)
            # pz=P(cm.transform_point(rev*p.x,p.y))
            pz = P(inferrtr(a.x, a.y, phi, p.x, p.y))
            bb.addp(pz)

        arln = self.getarln(wstate)

        if self.linetype[2] == ">":
            wstate.save()
            x1, y1, x2, y2, phi = self.getarr2points(wstate, a, b, ln)
            x1, y1, x2, y2 = self._drawarrow2(x1, y1, x2, y2, wstate, arln, (1, 0, 0), self.lw)
            bb.addp(P(inferrtr(a.x, a.y, phi, x1, y1)))
            bb.addp(P(inferrtr(a.x, a.y, phi, x2, y2)))
            wstate.restore()

        if self.linetype[0] == "<":
            wstate.save()
            x1, y1, x2, y2, phi = self.getarr1points(wstate, a, b, ln)
            x1, y1, x2, y2 = self._drawarrow2(x1, y1, x2, y2, wstate, arln, self.color, self.lw)
            bb.addp(P(inferrtr(a.x, a.y, phi, x1, y1)))
            bb.addp(P(inferrtr(a.x, a.y, phi, x2, y2)))
            wstate.restore()

        if self.label:
            wstate.save()
            pl = self.getlabel(wstate.ctx, a, b, ln, self.labelalign)
            pl.bbp = []
            pl._draw(wstate)
            v = b - a
            phi = atan2(v.y, v.x)
            for (x, y) in pl.bbp:
                bb.addp(P(inferrtr(pl.pos.x, pl.pos.y, phi, x, y)))
            wstate.restore()

    def getlabel(self, ctx, a, b, ln, labelalign):
        v = b - a
        phi = atan2(v.y, v.x)

        p1, p2, p3 = self.getmainpoints(a, b, phi)
        p01 = p1 * 0.5
        p12 = (p1 + p2) * 0.5
        p23 = (p2 + p3) * 0.5
        p0123 = (p01 * 0.5 + p12 + p23 * 0.5) * 0.5

        phideg = 180 * phi / pi
        p = P(inferrtr(a.x, a.y, phi, p0123.x, p0123.y))
        return PrimNLabelMulti(p, self.label, "Bc",
                               self.color, -phideg, self.extsyntax, self.labelspacing, self.fontsize)


class PrimNLabelMulti(PrimObj):
    def __init__(self, pos, s, side, color, rotate, extsyntax, spacing, fontsize):
        PrimObj.__init__(self)
        self.args = str(s), side, pconv(color), rotate, extsyntax, spacing, fontsize
        self.pos = pos
        self.priority = 1
        s = str(s)
        if s and s[0] != '!' or not s: s = '!' + side + '!' + s  # !...! as first
        # print s
        sp = []
        i = 0
        while i < len(s):
            if s[i] == '!':
                sp.append('')
            elif s[i] == '\\' and (i + 1) < len(s) and s[i + 1] == '!':
                i += 1
                sp[-1] += '!'
            else:
                sp[-1] += s[i]
            i += 1
        self.labels = [_PrimNLabel(pos, s, side, color, rotate, extsyntax, spacing, fontsize) for side, s in
                       zip(sp[::2], sp[1::2])]

    # for l in self.labels: print "%s||%s||"%(l.side, l.s	)

    def getbb(self, wstate, bb):
        for l in self.labels: l.getbb(wstate, bb)

    def draw(self, wstate):
        for l in self.labels: l.draw(wstate)

    def _draw(self, wstate):
        for l in self.labels: l._draw(l.pos, wstate)

    def gse(self, d):
        p = d.setdefault((self.pos.x, self.pos.y), "p%d" % self.id)
        return "label %s '%s' side='%s' color=%s rotate=%s extsyntax=%d spacing=%s fontsize=%d" % ((p,) + self.args)


class _PrimNLabel(PrimObj):
    def __init__(self, pos, s, side, color, rotate, extsyntax, spacing, fontsize):
        PrimObj.__init__(self)
        self.pos = pos
        self.s = str(s)
        self.args = str(s), side, pconv(color), rotate, extsyntax, spacing, fontsize
        # print "LABEL ##%s##"%s
        self.side = side
        self.color = getcolor(color)
        self.rotate = rotate
        self.extsyntax = extsyntax
        self.spacing = P(spacing)
        self.extractctrl()
        self.tokens = list(mathtokenizer(self.s, self.extsyntax))

        self.fontsize = fontsize

        self.bbp = None
        self.linespacing = 1.5
        self.drawglyph = (extsyntax == 2)

    # if str(s).strip():
    #	lllog("prim.py:%s"%s)

    def gse(self, d):
        p = d.setdefault((self.pos.x, self.pos.y), "p%d" % self.id)
        return "label %s '%s' side='%s' color=%s rotate=%s extsyntax=%d spacing=%s fontsize=%d" % ((p,) + self.args)

    def extractctrl(self):
        if not self.s: return
        # if self.s[0]=="!" and "!" in self.s[1:]:
        #	tside,self.s=self.s[1:].split("!",1)
        # else:
        tside = self.side
        negside = 1
        tside = tside.split(";")
        self.side = ''
        while tside:
            curt = tside.pop(0)
            if not curt: continue
            if 'raw' == curt:
                # RAW STRING
                self.extsyntax = 0
                continue
            if curt[0] == '(':
                # NEW POS
                self.pos = P(eval(curt))
                continue
            if curt[:3] == 'rot':
                if curt[3] == ']':
                    if len(curt) == 5:
                        self.side += 'lxm'
                    else:
                        self.side += 'rxm'
                    self.rotate += 90
                elif curt[3] == '[':
                    if len(curt) == 5:
                        self.side += 'rxm'
                    else:
                        self.side += 'lxm'
                    self.rotate += -90
                else:
                    self.rotate += float(curt[3:])
                continue
            self.side += curt
        side = self.side
        if not side: side = "cy"  # default

        side = side.replace("B", "y")
        side = side.replace("Z", "xxyy").replace("X", "xx")
        side = side.replace("Y", "yy").replace("z", "xy")
        side = side.replace("T", "ty")

        self.sidex = side.count('x') * (1 - 2 * int('-x' in side))
        self.sidey = side.count('y') * (1 - 2 * int('-y' in side))
        side = side.replace("-x", '').replace("-y", '')
        side = side.replace("x", '').replace("y", '')
        self.side = side

    def __str__(self):
        return "_PrimNLabel " + str(self.pos) + " <%s>" % self.s

    def draw(self, wstate):
        x, y = wstate.xy(self.pos)
        self.bbp = None
        self._draw(P(wstate.xy(self.pos)), wstate)

    def getbb(self, wstate, bb):
        a = P(*wstate.xy(self.pos))
        bb.addp(a)  #
        phi = -pi * self.rotate / 180
        # print wstate,self.s,"a=",a,phi #!!!!!!!!!1,phi
        if not wstate.ignoretextbb:
            self.bbp = []
            self._draw(a, wstate)
            for x, y in self.bbp:
                p = P(inferrtr(1.0 * a.x, 1.0 * a.y, phi, 1.0 * x, 1.0 * y))
                bb.addp(p)

    def _getextentsglyph(self, ctx, s, x, y):
        return ctx.glyph_extents([(s, x, y)])

    # if type(s)==int: s=str(s).decode("utf-8")
    # return ctx.text_extents(s)

    def _getextentstext(self, ctx, s, x=0, y=0):
        if type(s) == int:

            if s > ROTFACTOR: s = s - ROTFACTOR
            s = unichr(s)
        # s=("\u%d"%s).decode("utf-8")

        return ctx.text_extents(s)

    def _chrtoglyph(self, ctx, c, tp, symbolshift):
        if tp:
            xb, _, wi, _, _, _ = self._getextentsglyph(ctx, c, 0, 0)
            return [(c, 0, 0)], xb + wi

        cx = 0
        res = []
        wspace = 0
        if ' ' in c: _, _, wspace, _, _, _ = self._getextentsglyph(ctx, ord('_'), 0, 0)
        for s in c:
            if s == ' ':
                cx += wspace
                continue

            cg = (ord(s) - symbolshift, cx, 0)
            res.append(cg)
            xb, _, wi, _, _, _ = self._getextentsglyph(ctx, *cg)
            cx += wi + xb
        return res, cx

    def _chrtotext(self, ctx, c, tp):
        if tp:
            xb, _, wi, _, _, _ = self._getextentstext(ctx, c, 0, 0)
            return [(c, 0, 0)], xb + wi

        cx = 0
        res = []
        wspace = 0
        if ' ' in c: _, _, wspace, _, _, _ = self._getextentstext(ctx, '_', 0, 0)
        for s in c:
            if s == ' ':
                cx += wspace
                continue

            cg = (s, cx, 0)
            # print "HERE",s
            res.append(cg)
            xb, _, wi, _, _, _ = self._getextentstext(ctx, *cg)
            cx += wi + xb
        return res, cx

    def chrdraw(self, ctx, c, tp, symbolshift):
        if self.drawglyph:
            return self._chrtoglyph(ctx, c, tp, symbolshift)
        return self._chrtotext(ctx, c, tp)

    def _draw(self, p, wstate):

        # self.bbp=[]
        if not self.tokens: return []

        side = self.side
        sub = 5

        wstate.save()
        wstate.translate(p.x, p.y)

        if self.rotate:
            phi = -self.rotate / 180.0 * pi
            wstate.rotate(phi)
        else:
            phi = 0
        x, y = 0, 0

        tokens = self.tokens
        # print tokens
        # determine width
        swidth = 0.0
        curswidth = 0.0
        lines = []
        for t, style, s, tp, _, _, fontsize in tokens:
            if s == '\n':
                lines.append(curswidth)
                curswidth = 0.0
            if not fontsize: fontsize = self.fontsize
            wstate.ctx.select_font_face(*wstate.fndict[t])
            wstate.ctx.set_font_size(wstate.getfontsize(style, fontsize))
            # print s,type(s),tp
            if tp and type(s) == int and s > ROTFACTOR: s = s - ROTFACTOR
            gly, w = self.chrdraw(wstate.ctx, s, tp, wstate.symbolshift)
            curswidth += w  # ar[2]+ar[0]
        lines.append(curswidth)
        swidth = max(lines)

        # x,y=self.wstate.xy(pos)
        # self._rawpoint(x,y,"red")

        stnwidth, stnheight = wstate.stnsizes()
        # print "STNSIZES",swidth,stnwidth,stnheight,wstate.ctx.get_font_face()
        spx, spy = self.spacing.x, self.spacing.y

        # if 'x' in self.s:
        #	print "TYPE",self.s,typ

        deltax = self.sidex * stnwidth * spx
        if "r" in side:  # right
            x += -swidth - deltax
        elif "c" in side:  # center
            x -= 0.5 * swidth
        else:  # l default
            x += deltax

        deltay = self.sidey * spy * stnheight

        if "t" in side:
            y += stnheight + deltay
        elif "m" in side:
            if len(lines) == 1:
                y += stnheight * 0.5
            else:
                y += -stnheight * 0.5 - (len(lines) - 2) * stnheight * self.linespacing * 0.5
        else:
            y -= deltay + (len(lines) - 1) * stnheight * self.linespacing  # b - default

        oy = y

        sup = stnheight * MATHSUPERSCRIPT
        sub = stnheight * MATHSUBSCRIPT
        sub2 = stnheight * MATHSUBSCRIPT2
        x0 = x

        first = 1
        startline = 1
        lastsupx = lastsubx = lastnor = x
        linecnt = 0

        for t, style, s, tp, color, backgroundcolor, fontsize in tokens:  # FSX
            # if sidee(s)==int: s=

            # print '#%s#'%s

            if s == "\n":
                x = x0
                lastsupx = lastsubx = lastnor = x
                linecnt += 1
                oy = oy + stnheight * self.linespacing
                startline = 1
                continue

            if style == STYLENORMAL:
                y = oy
                x = max(x, lastnor, lastsubx, lastsupx)
            elif style == STYLESUBSCRIPT:
                y = oy + sub
                x = lastsubx
            elif style == STYLESUBSCRIPT2:
                y = oy + sub2
                x = lastsubx

            elif style == STYLESUPSCRIPT:
                y = oy + sup
                x = lastsupx

            if startline:
                if "R" in side:
                    x += swidth - lines[linecnt]
                elif "C" in side:
                    x += (swidth - lines[linecnt]) * 0.5
                startline = 0

            sp = 0
            if s == ' ':
                s = 'i'
                sp = 1

            wstate.ctx.set_source_rgb(*getcolor(color))
            wstate.ctx.move_to(x, y)
            wstate.ctx.select_font_face(*wstate.fndict[t])
            if not fontsize: fontsize = self.fontsize  # upper level
            wstate.ctx.set_font_size(wstate.getfontsize(style, fontsize))

            gly, _ = self.chrdraw(wstate.ctx, s, tp, wstate.symbolshift)
            # print gly,len(gly)
            if self.drawglyph:
                x_bearing, y_bearing, width, height, _, _ = wstate.ctx.glyph_extents(gly)
            else:
                x_bearing, y_bearing, width, height, _, _ = self._getextentstext(wstate.ctx, s)

            xb = x + x_bearing
            yb = y + y_bearing

            rot = 0
            if not sp:
                if self.bbp is not None:
                    if wstate.fulltextbb:
                        lst = [(xb, yb + height), (xb, yb), (xb + width, yb + height), (xb + width, yb)]
                    else:
                        lst = [(xb, y), (xb + width, y), (xb + width, y - stnheight)]
                    self.bbp.extend(lst)

            if first:
                miny = y + y_bearing
                maxy = y + y_bearing + height
                first = 0
            else:
                miny = min(miny, y + y_bearing)
                maxy = max(maxy, y + y_bearing + height)

            if not sp:
                # ctx.move_to(x,y)
                # if tp: s=chr(s+31) # str(s+31).decode("utf-8")
                # ctx.show_text(s)
                gly = [(i, a + x, b + y) for (i, a, b) in gly]
                if self.drawglyph:
                    wstate.ctx.show_glyphs(gly)
                    wstate.ctx.stroke()
                else:
                    if type(s) == int:
                        if s > ROTFACTOR:
                            rot = 1
                            s = s - ROTFACTOR
                        else:
                            rot = 0
                        s = unichr(s)
                    if not rot:

                        wstate.ctx.show_text(s)
                        wstate.ctx.stroke()

                    else:
                        # print "HERE",x,width,x_bearing,y,y_bearing,height
                        wstate.save()
                        # wstate.ctx.move_to(x+width,y+height)
                        # wstate.translate(x+width+x_bearing,y+height)
                        wstate.translate(x, y)

                        wstate.ctx.move_to(0, 0)
                        wstate.rotate(pi)
                        wstate.ctx.move_to(-width, -y_bearing)
                        # ctrlpt(wstate.ctx,0,0,0.1)
                        # wstate.ctx.move_to(-width,-height)

                        wstate.ctx.show_text(s)
                        wstate.ctx.stroke()
                        wstate.restore()

            x += width + x_bearing

            if style == STYLESUBSCRIPT:
                lastsubx = x
            elif style == STYLESUPSCRIPT:
                lastsupx = x
            else:
                lastnor = lastsubx = lastsupx = max(lastnor, lastsubx, lastsupx, x)

        # self._rawpoint(x,miny,"red",1)
        # self._rawpoint(x-2,maxy,"blue",1)
        # self._rawpoint(x,my,"green",1)

        wstate.restore()


class PrimBox(PrimObj):
    def __init__(self, a, b, lw, linecolor, fillcolor, fillstyle, boxlines):
        PrimObj.__init__(self)
        self.a = a
        self.b = b
        self.lw = lw
        self.linecolor = getcolor(linecolor)
        self.fillcolor = getcolor(fillcolor)
        self.fillstyle = fillstyle
        if not boxlines: boxlines = 'tblr'
        self.boxlines = boxlines

    def getbb(self, wstate, bb):
        a = P(*wstate.xy(self.a))
        b = P(*wstate.xy(self.b))
        bb.addp(a)
        bb.addp(b)

    def _draw(self, p, q, wstate):
        lx = min(p.x, q.x)
        rx = max(p.x, q.x)
        by = max(p.y, q.y)
        ty = min(p.y, q.y)
        l = []

        if not self.boxlines or len(self.boxlines) == 4:
            wstate.save()
            p = [(lx, ty), (rx, ty), (rx, by), (lx, by)]
            if self.fillstyle != "none":
                for i in xrange(-1, len(p)):
                    if i == -1:
                        wstate.ctx.move_to(*p[i])
                    else:
                        wstate.ctx.line_to(*p[i])
                self.fill(wstate.ctx, self.fillstyle, self.fillcolor)

            for i in xrange(-1, len(p)):
                if i == -1:
                    wstate.ctx.move_to(*p[i])
                else:
                    wstate.ctx.line_to(*p[i])
            self.stroke(wstate.ctx, self.linecolor, self.lw)
            wstate.restore()
            return

        if 't' in self.boxlines: l.append((P(lx, ty), P(rx, ty)))
        if 'b' in self.boxlines: l.append((P(rx, by), P(lx, by)))
        if 'l' in self.boxlines: l.append((P(lx, by), P(lx, ty)))
        if 'r' in self.boxlines: l.append((P(rx, ty), P(rx, by)))

        for p, q in l:
            wstate.ctx.move_to(p.x, p.y)
            wstate.ctx.line_to(q.x, q.y)
            self.stroke(wstate.ctx, self.linecolor, self.lw)

    def draw(self, wstate):
        ax, ay = wstate.xy(self.a)
        bx, by = wstate.xy(self.b)
        self._draw(P(ax, ay), P(bx, by), wstate)


class PrimPolygon(PrimObj):
    def __init__(self, p, lw, linecolor, fillcolor, fillstyle):
        PrimObj.__init__(self)
        self.p = p
        self.lw = lw
        self.linecolor = getcolor(linecolor)
        self.fillcolor = getcolor(fillcolor)
        self.fillstyle = fillstyle

    # print "PP",lw,

    def getbb(self, wstate, bb):
        for p in self.p: bb.addp(P(*wstate.xy(p)))

    def draw(self, wstate):
        wstate.save()
        if self.fillstyle != "none":
            for i in xrange(-1, len(self.p)):
                x, y = wstate.xy(self.p[i])
                if i == -1: wstate.ctx.move_to(x, y)
                wstate.ctx.line_to(x, y)
            # self.rbb.addp(P(x,y))
            self.fill(wstate.ctx, self.fillstyle, self.fillcolor)

        for i in xrange(-1, len(self.p)):
            x, y = wstate.xy(self.p[i])
            if i == -1: wstate.ctx.move_to(x, y)
            wstate.ctx.line_to(x, y)
        # self.rbb.addp(P(x,y))
        self.stroke(wstate.ctx, self.linecolor, self.lw)
        wstate.restore()
        return 1


class PrimBezier(PrimPolygon):

    # Points: P0..PN - main points, K,K' - control points
    # P0 K0' K1
    # P1 K1' K2
    # P2 K2' K3
    # ...
    # PN KN' K0 (loop to P0)

    def draw(self, wstate):
        wstate.save()
        if self.fillstyle != "none":
            for i in xrange(0, len(self.p), 3):
                x, y = wstate.xy(self.p[i])
                if i == 0: wstate.ctx.move_to(x, y)
                x1, y1 = wstate.xy(self.p[i + 1])
                x2, y2 = wstate.xy(self.p[i + 2])
                if i + 3 == len(self.p):
                    x3, y3 = wstate.xy(self.p[0])
                else:
                    x3, y3 = wstate.xy(self.p[i + 3])
                wstate.ctx.curve_to(x1, y1, x2, y2, x3, y3)
            # self.rbb.addp(P(x,y))
            self.fill(wstate.ctx, self.fillstyle, self.fillcolor)

        for i in xrange(0, len(self.p), 3):
            x, y = wstate.xy(self.p[i])
            if i == 0: wstate.ctx.move_to(x, y)
            if i + 3 == len(self.p):
                x3, y3 = wstate.xy(self.p[0])
            else:
                x3, y3 = wstate.xy(self.p[i + 3])

            x1, y1 = wstate.xy(self.p[i + 1])
            x2, y2 = wstate.xy(self.p[i + 2])

            wstate.ctx.curve_to(x1, y1, x2, y2, x3, y3)

        self.stroke(wstate.ctx, self.linecolor, self.lw)
        wstate.restore()
        return 1


class PrimConvex(PrimPolygon):
    def __init__(self, p, lw, linecolor, fillcolor, fillstyle, convexparams):
        PrimPolygon.__init__(self, p, lw, linecolor, fillcolor, fillstyle)
        self.params = convexparams

    def draw(self, wstate):

        def getconvex(p):
            return [j[2] for j in convex_hull([(i.x, i.y, i) for i in p])]

        def extendcenter(centerpoint, p, factor):
            pp = p - centerpoint
            pp = pp / pp.ln()
            return p + pp * factor

        def expandconvex(pl, delta, alpha):
            centerpoint = sum(pl, P(0, 0)) / len(pl)

            cpts = []
            for i in xrange(len(pl)):
                cpts.append(extendcenter(centerpoint, pl[i], delta))

            mp = []
            for e in cpts:
                c1 = e - centerpoint
                c1 = P(c1.y, -c1.x)
                c1 = e + c1 / c1.ln() * alpha
                mp.append(c1)
                mp.append(e)
                c1 = e - centerpoint
                c1 = P(-c1.y, c1.x)
                c1 = e + c1 / c1.ln() * alpha
                mp.append(c1)

            return mp

        def convexedge(pl, delta, gamma):
            centerpoint = sum(pl, P(0, 0)) / len(pl)
            mp = [pl[-1]]
            for e in pl:
                s = mp.pop(-1)
                c1 = extendcenter(centerpoint, s * (1 - gamma) + e * gamma, delta)
                c2 = extendcenter(centerpoint, s * gamma + e * (1 - gamma), delta)
                c12 = (c1 + c2) * 0.5
                mp.append(c1)
                mp.append(c12)
                mp.append(c2)
                mp.append(e)
            mp.pop(-1)
            return mp

        wstate.save()
        mp = [P(wstate.xy(i)) for i in self.p]
        mp = getconvex(mp)

        if len(mp) == 1:
            p = mp[0]
            z = 0.66
            mp = [p + P(0, 1), p + P(1, 0), p + P(0, -1), p + P(-1, 0), p + P(z, z), p + P(-z, z), p + P(z, -z),
                  p + P(-z, -z)]
            mp = getconvex(mp)

        a, b, c, d = self.params
        mp = expandconvex(mp, a, b)
        mp = getconvex(mp)
        mp = convexedge(mp, c, d)

        drawx = 1
        if drawx:
            mp.append(mp.pop(0))
            mp.append(mp.pop(0))
            wstate.ctx.move_to(mp[-1].x, mp[-1].y)
            for i in xrange(0, len(mp), 3):
                wstate.ctx.curve_to(mp[i].x, mp[i].y, mp[i + 1].x, mp[i + 1].y, mp[i + 2].x, mp[i + 2].y)
            self.stroke(wstate.ctx, self.linecolor, self.lw)
            wstate.restore()

            if self.fillstyle != "none":
                wstate.save()
                wstate.ctx.move_to(mp[-1].x, mp[-1].y)
                for i in xrange(0, len(mp), 3):
                    wstate.ctx.curve_to(mp[i].x, mp[i].y, mp[i + 1].x, mp[i + 1].y, mp[i + 2].x, mp[i + 2].y)
                self.fill(wstate.ctx, self.fillstyle, self.fillcolor)

        self.stroke(wstate.ctx, self.linecolor, self.lw)
        wstate.restore()
        return 1


class PrimMark(PrimObj):
    def __init__(self, p, marktype, size, lw, color, fillcolor, fillstyle, yratio, rot):
        PrimObj.__init__(self)
        self.p = p
        self.marktype = marktype
        self.size = size
        self.lw = lw
        self.color = getcolor(color)
        self.fillcolor = getcolor(fillcolor)
        self.fillstyle = fillstyle
        self.yratio = yratio
        self.rotate = rot

    def getbb(self, wstate, bb):
        a = P(*wstate.xy(self.p))
        bb.addp(a)
        bbp = self.draw(wstate)
        for p in bbp: bb.addp(P(*wstate.xy(p)))

    def draw(self, wstate):

        def _polygon(p, lw, linecolor, fillcolor, fillstyle):
            wstate.save()

            if fillstyle != "none":
                for i in xrange(-1, len(p)):
                    x, y = wstate.xy(p[i])
                    if i == -1:
                        wstate.ctx.move_to(x, y)
                    else:
                        wstate.ctx.line_to(x, y)
                # self.rbb.addp(P(x,y))
                self.fill(wstate.ctx, fillstyle, fillcolor)

            for i in xrange(-1, len(p)):
                x, y = wstate.xy(p[i])
                if i == -1: wstate.ctx.move_to(x, y)
                wstate.ctx.line_to(x, y)
            # self.rbb.addp(P(x,y))
            self.stroke(wstate.ctx, linecolor, lw)
            wstate.restore()
            return 1

        from math import sin, cos, pi

        def rpsize(pl, rot, rev, mov, yratio=1.0):
            def _rev(p): return P(p.x, -p.y)

            if rev: pl = [_rev(P(v)) for v in pl]
            return [(self.p + _mov(_rot(P(v) * P(1.0, yratio), rot), mov) * size) for v in pl]

        def star(k, d):
            rad = pi / k
            return [P(sin(i * rad), cos(i * rad)) * 0.65 * (2 - d * (i % 2)) for i in xrange(2 * k)]

        def ngon(k, st):
            rad = 2 * pi / k
            return [P(sin(i * rad), cos(i * rad)) * st for i in xrange(k)]

        def _rot(p, rot):
            if rot == -1: return P(p.x, -p.y)
            if not rot: return p
            return P(1.0 * p.x * cos(rot) - 1.0 * p.y * sin(rot), 1.0 * p.x * sin(rot) + 1.0 * p.y * cos(rot))

        def _mov(p, mov):
            if not mov: return p
            return P(p) + mov

        def pacman2(phi, phi2, sc=None):
            def drawhalf(side=1):
                wstate.ctx.arc(0, 0, size, 0, pi)
                wstate.ctx.move_to(-size, 0)
                wstate.ctx.line_to(0, 0)

                if self.fillstyle != "none":
                    self.fill(wstate.ctx, self.fillstyle, self.fillcolor)
                    wstate.ctx.arc(0, 0, size, 0, pi)
                    wstate.ctx.move_to(-size * side, 0)
                    wstate.ctx.line_to(0, 0)
                self.stroke(wstate.ctx, self.color, self.lw)

            wstate.save()

            wstate.translate(x, y)
            wstate.rotate(-self.rotate * pi / 180)
            wstate.rotate(phi * pi)
            wstate.ctx.move_to(0, 0)
            if sc: wstate.ctx.scale(*sc)
            drawhalf()
            wstate.rotate(-phi2 * pi)
            drawhalf(-1)
            wstate.restore()

            mx = 8
            res = []
            for q in xrange(1, mx):
                alf = 2 * pi * q / mx - phi * pi - 3 * pi / 4
                p = P(x, y) + P(size * sin(alf), size * cos(alf)).rotatedeg(-self.rotate)
                res.append(P(wstate.rxy(p.x, p.y)))
            return res

        x, y = wstate.xy(self.p)
        size = self.size * 0.45 * wstate.getfontsize()

        # print marktype
        marktype = self.marktype
        if marktype == "circle":
            wstate.ctx.arc(x, y, size, 0, 2 * pi)
            if self.fillstyle != "none":
                self.fill(wstate.ctx, self.fillstyle, self.fillcolor)
                wstate.ctx.arc(x, y, size, 0, 2 * pi)
            self.stroke(wstate.ctx, self.color, self.lw)
            mx = 8
            res = []
            for q in xrange(0, mx):
                alf = 2 * pi * q / mx
                p = P(x, y) + P(size * sin(alf), size * cos(alf)).rotatedeg(-self.rotate)
                res.append(P(wstate.rxy(p.x, p.y)))
            return res
        elif marktype == "pacman1":
            return pacman2(0, .5)
        elif marktype == "pacman2":
            return pacman2(0.5, .5)
        elif marktype == "pacman3":
            return pacman2(1.0, .5)
        elif marktype == "pacman4":
            return pacman2(1.5, .5)
        elif marktype == "pacman5":
            return pacman2(-0.25 / 2, .75)
        elif marktype == "pacman6":
            return pacman2(1.0 - 0.25 / 2, .75)
        elif marktype == "pacman7":
            return pacman2(1.5 - 0.25 / 2, .75)
        elif marktype == "pacman8":
            return pacman2(2.0 - 0.25 / 2, .75)
        elif marktype == "ellipse":

            wstate.save()
            wstate.translate(x, y)
            wstate.rotate(-pi * self.rotate / 180)
            xf = 1.2
            yf = 0.7
            wstate.ctx.scale(xf, yf * self.yratio)
            wstate.ctx.arc(0, 0, size, 0, 2 * pi)
            if self.fillstyle != "none":
                self.fill(wstate.ctx, self.fillstyle, self.fillcolor)
                wstate.ctx.arc(0, 0, size, 0, 2 * pi)
            self.stroke(wstate.ctx, self.color, self.lw)
            wstate.restore()

            # xr=P(xf*size,0).rotatedeg(-self.rotate)
            # yr=
            a = xf * size
            b = yf * size * self.yratio
            # q=P(x,y)+P(0,size*yf*self.yratio).rotatedeg(-self.rotate)
            res = []
            mx = 10
            for q in xrange(mx):
                alf = 2 * pi * q / mx
                p = P(x, y) + P(a * sin(alf), b * cos(alf)).rotatedeg(-self.rotate)
                res.append(P(wstate.rxy(p.x, p.y)))
            return res

        elif marktype in marknames:

            st = 1.15  # triangle
            sd = 1.15  # diamond
            q = 0.4  # cross
            qs = 1.15  # corss
            sdx = 1.25  # diamond2
            rev = 0
            rot = 0
            if marktype[-3:] == "rot":
                rot = pi / 4
            elif marktype[-3:] == "rev":
                rev = -1
            else:
                rot = 0
            pl = None
            mov = 0
            if marktype == "star":
                pl = star(5, 1.1)
            elif marktype == "star8":
                pl = star(8, 1.0)
            elif marktype[:5] == "star4":
                pl = star(4, 1.2)
            elif marktype == "star3":
                mov = P(0, -0.3); pl = star(3, 1.3);
            elif marktype == "star3rev":
                mov = P(0, 0.3); pl = star(3, 1.3);
            elif marktype[:6] == "square":
                pl = [(-1, -1), (-1, 1), (1, 1), (1, -1)]
            elif marktype[:8] == "triangle":
                pl = [(-1, -1), (1, -1), (0, 1)]  # pl=ngon(3,st)
            elif marktype == "hexagon":
                pl = ngon(6, st)
            elif marktype == "pentagon":
                pl = ngon(5, st)
            elif marktype == "diamond":
                pl = [(-sd, 0), (0, sd), (sd, 0), (0, -sd)]
            elif marktype == "diamond2":
                pl = [(-sdx, 0), (0, sdx * 0.6), (sdx, 0), (0, -sdx * 0.6)]
            elif marktype == "hourglass":
                pl = [(-1, -1), (-0.25, 0), (-1, 1), (1, 1), (0.25, 0), (1, -1)]
            elif marktype == "hourglass2":
                pl = [(-1, -1), (-1, 1), (0, .25), (1, 1), (1, -1), (0, -.25)]
            elif marktype == "1square":
                pl = [(-1, -1), (0, 0), (-1, 1), (1, 1), (1, -1)]
            elif marktype == "2square":
                pl = [(-1, -1), (-1, 1), (1, 1), (1, -1), (0, 0)]
            elif marktype == "3square":
                pl = [(-1, -1), (-1, 1), (0, 0), (1, 1), (1, -1)]
            elif marktype == "4square":
                pl = [(-1, -1), (-1, 1), (1, 1), (1, -1), (0, 0)]
            elif marktype[:5] == "cross":
                pl = [(-qs, q), (-q, q), (-q, qs), (q, qs), (q, q), (qs, q), (qs, -q), (q, -q), (q, -qs), (-q, -qs),
                      (-q, -q), (-qs, -q)]
            elif marktype[0] == "V" and len(marktype) == 2:
                rot = (int(marktype[1]) - 1) * pi / 2
                pl = [(-1, 0), (-1, 1), (-0.5, 1), (0, .5), (.5, 1), (1, 1), (1, 0), (0, -1)]
            elif marktype[0] == "X" and len(marktype) == 2:
                rot = (int(marktype[1]) - 1) * pi / 2
                pl = [(-1, -1), (-1, 1), (-0.5, 1), (0, .3), (.5, 1),
                      (1, 1), (1, -1), (0.5, -1), (0, -.3), (-.5, -1)]
            elif marktype[0:6] == "xcross":
                q = .25
                px = .7
                pl = [(px, 1), (q, q), (1, px), (1, -px), (q, -q), (px, -1),
                      (-px, -1), (-q, -q), (-1, -px), (-1, px), (-q, q), (-px, 1)]
                if marktype == "xcrossrot":
                    size = size * 0.9

            elif marktype[0:6] == "Xcross":
                if len(marktype) == 6:
                    q = .7
                    px = .5
                else:
                    q = .27
                    px = .7
                pl = [(1, 1), (1, q), (px, 0), (1, -q),
                      (1, -1), (q, -1), (0, -px), (-q, -1),
                      (-1, -1), (-1, -q), (-px, 0), (-1, q),
                      (-1, 1), (-q, 1), (0, px), (q, 1)]

            if pl:
                rot = rot + pi * self.rotate / 180.0

                res = rpsize(pl, rot, rev, mov, self.yratio)
                _polygon(res, self.lw, self.color, self.fillcolor, self.fillstyle)
                return res
            else:
                print "Unknown marktype %s" % marktype, rot, marktype[-3:]

        return []
