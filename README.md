
This package contains willow software.

In directories:
draw - the main directory for drawing graphs written in python 2.7; 
        the main script is drwillow.py 
gse - some examples for drawing networks


How to run drwillow?

drwillow.py --help

drwillow.py -p ../gse/A103_A303_LC_merge_sameSize.fasta.gse
